import IController from '@presentation/protocols/IController';

import CreateTransactionController from '@presentation/controllers/transaction/CreateTransactionController';

import createTransactionFactory from '@main/factories/useCases/transaction/CreateTransactionFactory';
import createTransactionPresenterFactory from '@main/factories/presenters/transaction/CreateTransactionPresenterFactory';
import createTransactionValidationFactory from '@main/factories/controllers/transaction/CreateTransactionValidationFactory';

const makeCreateTransactionController = (): IController => {
  const createTransaction = createTransactionFactory.makeCreateTransaction();
  const createTransactionPresenter =
    createTransactionPresenterFactory.makeCreateTransactionPresenter();
  const createTransactionValidation =
    createTransactionValidationFactory.makeCreateTransactionValidation();

  const createTransactionController = new CreateTransactionController(
    createTransaction,
    createTransactionValidation,
    createTransactionPresenter,
  );

  return createTransactionController;
};

export default { makeCreateTransactionController };
