import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import providerDateValidationFactory from '@main/factories/validators/transaction/ProviderDateValidationFactory';
import storeEvaluationValueValidationFactory from '@main/factories/validators/transaction/StoreEvaluationValidationFactory';

const makeCreateTransactionValidation = (): ValidationComposite => {
  const providerDateValidation = providerDateValidationFactory.makeProviderDateValidation();
  const storeEvaluationValueValidation =
    storeEvaluationValueValidationFactory.makeStoreEvaluationValidation();

  const validations = [providerDateValidation, storeEvaluationValueValidation];

  const validationComposite = new ValidationComposite(validations);

  return validationComposite;
};

export default { makeCreateTransactionValidation };
