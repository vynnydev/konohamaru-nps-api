import IController from '@presentation/protocols/IController';

import UpdateTransactionController from '@presentation/controllers/transaction/UpdateTransactionController';

import UpdateTransactionFactory from '@main/factories/useCases/transaction/UpdateTransactionFactory';
import UpdateTransactionPresenterFactory from '@main/factories/presenters/transaction/UpdateTransactionPresenterFactory';
import UpdateTransactionValidationFactory from '@main/factories/controllers/transaction/UpdateTransactionValidationFactory';

const makeUpdateTransactionController = (): IController => {
  const updateTransaction = UpdateTransactionFactory.makeUpdateTransaction();
  const updateTransactionPresenter =
    UpdateTransactionPresenterFactory.makeUpdateTransactionPresenter();
  const updateTransactionValidation =
    UpdateTransactionValidationFactory.makeUpdateTransactionValidation();

  const updateTransactionController = new UpdateTransactionController(
    updateTransaction,
    updateTransactionValidation,
    updateTransactionPresenter,
  );

  return updateTransactionController;
};

export default { makeUpdateTransactionController };
