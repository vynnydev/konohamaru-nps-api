import IController from '@presentation/protocols/IController';

import FindEvaluationsController from '@presentation/controllers/evaluation/FindEvaluationsController';

import findEvaluationsFactory from '@main/factories/useCases/evaluation/FindEvaluationsFactory';
import findEvaluationsPresenterFactory from '@main/factories/presenters/evaluation/FindEvaluationsPresenterFactory';
import findEvaluationsValidationFactory from '@main/factories/controllers/evaluation/FindEvaluationsValidationFactory';

const makeFindEvaluationsController = (): IController => {
  const findEvaluations = findEvaluationsFactory.makeFindEvaluations();
  const findEvaluationsPresenter = findEvaluationsPresenterFactory.makeFindEvaluationsPresenter();
  const findEvaluationsValidation =
    findEvaluationsValidationFactory.makeFindEvaluationsValidation();

  const findEvaluationsController = new FindEvaluationsController(
    findEvaluations,
    findEvaluationsValidation,
    findEvaluationsPresenter,
  );

  return findEvaluationsController;
};

export default { makeFindEvaluationsController };
