import IController from '@presentation/protocols/IController';

import CreateEvaluationController from '@presentation/controllers/evaluation/CreateEvaluationController';

import createEvaluationFactory from '@main/factories/useCases/evaluation/CreateEvaluationFactory';
import createEvaluationPresenterFactory from '@main/factories/presenters/evaluation/CreateEvaluationPresenterFactory';
import createEvaluationValidationFactory from '@main/factories/controllers/evaluation/CreateEvaluationValidationFactory';

const makeCreateEvaluationController = (): IController => {
  const createEvaluation = createEvaluationFactory.makeCreateEvaluation();
  const createEvaluationPresenter =
    createEvaluationPresenterFactory.makeCreateEvaluationPresenter();
  const createEvaluationValidation =
    createEvaluationValidationFactory.makeCreateEvaluationValidation();

  const createEvaluationController = new CreateEvaluationController(
    createEvaluation,
    createEvaluationValidation,
    createEvaluationPresenter,
  );

  return createEvaluationController;
};

export default { makeCreateEvaluationController };
