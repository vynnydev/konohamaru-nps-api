import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import limitValidationFactory from '@main/factories/validators/common/LimitValidationFactory';
import pageValidationFactory from '@main/factories/validators/common/PageValidationFactory';

const makeFindEvaluationsValidation = (): ValidationComposite => {
  const limitValidation = limitValidationFactory.makeLimitValidation();
  const pageValidation = pageValidationFactory.makePageValidation();

  const validations = [limitValidation, pageValidation];

  const validationComposite = new ValidationComposite(validations);

  return validationComposite;
};

export default { makeFindEvaluationsValidation };
