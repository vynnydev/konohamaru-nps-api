import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import descriptionValidationFactory from '@main/factories/validators/common/DescriptionValidationFactory';
import transactionValueValidationFactory from '@main/factories/validators/evaluation/TransactionValueValidationFactory';

const makeCreateEvaluationValidation = (): ValidationComposite => {
  const descriptionValidation = descriptionValidationFactory.makeDescriptionValidation();
  const transactionValueValidation =
    transactionValueValidationFactory.makeTransactionValueValidation();

  const validations = [descriptionValidation, transactionValueValidation];

  const validationComposite = new ValidationComposite(validations);

  return validationComposite;
};

export default { makeCreateEvaluationValidation };
