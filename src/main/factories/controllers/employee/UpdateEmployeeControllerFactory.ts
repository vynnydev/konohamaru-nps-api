import IController from '@presentation/protocols/IController';

import UpdateEmployeeController from '@presentation/controllers/employee/UpdateEmployeeController';

import UpdateEmployeeFactory from '@main/factories/useCases/employee/UpdateEmployeeFactory';
import UpdateEmployeePresenterFactory from '@main/factories/presenters/employee/UpdateEmployeePresenterFactory';
import UpdateEmployeeValidationFactory from '@main/factories/controllers/employee/UpdateEmployeeValidationFactory';

const makeUpdateEmployeeController = (): IController => {
  const updateEmployee = UpdateEmployeeFactory.makeUpdateEmployee();
  const updateEmployeePresenter = UpdateEmployeePresenterFactory.makeUpdateEmployeePresenter();
  const updateEmployeeValidation = UpdateEmployeeValidationFactory.makeUpdateEmployeeValidation();

  const updateEmployeeController = new UpdateEmployeeController(
    updateEmployee,
    updateEmployeeValidation,
    updateEmployeePresenter,
  );

  return updateEmployeeController;
};

export default { makeUpdateEmployeeController };
