import IController from '@presentation/protocols/IController';

import DeleteEmployeeController from '@presentation/controllers/employee/DeleteEmployeeController';

import deleteEmployeeFactory from '@main/factories/useCases/employee/DeleteEmployeeFactory';
import deleteEmployeePresenterFactory from '@main/factories/presenters/employee/DeleteEmployeePresenterFactory';

const makeDeleteEmployeeController = (): IController => {
  const deleteEmployee = deleteEmployeeFactory.makeDeleteEmployee();
  const deleteEmployeePresenter = deleteEmployeePresenterFactory.makeDeleteEmployeePresenter();

  const deleteEmployeeController = new DeleteEmployeeController(
    deleteEmployee,
    deleteEmployeePresenter,
  );

  return deleteEmployeeController;
};

export default { makeDeleteEmployeeController };
