import IController from '@presentation/protocols/IController';

import FindEmployeesController from '@presentation/controllers/employee/FindEmployeesController';

import findEmployeesFactory from '@main/factories/useCases/employee/FindEmployeesFactory';
import findEmployeesPresenterFactory from '@main/factories/presenters/employee/FindEmployeesPresenterFactory';

const makeFindEmployeesController = (): IController => {
  const findEmployees = findEmployeesFactory.makeFindEmployees();
  const findEmployeesPresenter = findEmployeesPresenterFactory.makeFindEmployeesPresenter();

  const findEmployeesController = new FindEmployeesController(
    findEmployees,
    findEmployeesPresenter,
  );

  return findEmployeesController;
};

export default { makeFindEmployeesController };
