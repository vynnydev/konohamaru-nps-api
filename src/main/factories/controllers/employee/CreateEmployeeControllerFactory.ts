import IController from '@presentation/protocols/IController';

import CreateEmployeeController from '@presentation/controllers/employee/CreateEmployeeController';

import createEmployeeFactory from '@main/factories/useCases/employee/CreateEmployeeFactory';
import createEmployeePresenterFactory from '@main/factories/presenters/employee/CreateEmployeePresenterFactory';
import createEmployeeValidationFactory from '@main/factories/controllers/employee/CreateEmployeeValidationFactory';

const makeCreateEmployeeController = (): IController => {
  const createEmployee = createEmployeeFactory.makeCreateEmployee();
  const createEmployeePresenter = createEmployeePresenterFactory.makeCreateEmployeePresenter();
  const createEmployeeValidation = createEmployeeValidationFactory.makeCreateEmployeeValidation();

  const createEmployeeController = new CreateEmployeeController(
    createEmployee,
    createEmployeeValidation,
    createEmployeePresenter,
  );

  return createEmployeeController;
};

export default { makeCreateEmployeeController };
