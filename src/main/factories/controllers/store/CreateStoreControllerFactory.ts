import IController from '@presentation/protocols/IController';

import CreateStoreController from '@presentation/controllers/store/CreateStoreController';

import createStoreFactory from '@main/factories/useCases/store/CreateStoreFactory';
import createStorePresenterFactory from '@main/factories/presenters/store/CreateStorePresenterFactory';
import createStoreValidationFactory from '@main/factories/controllers/store/CreateStoreValidationFactory';

const makeCreateStoreController = (): IController => {
  const createStore = createStoreFactory.makeCreateStore();
  const createStorePresenter = createStorePresenterFactory.makeCreateStorePresenter();
  const createStoreValidation = createStoreValidationFactory.makeCreateStoreValidation();

  const createStoreController = new CreateStoreController(
    createStore,
    createStoreValidation,
    createStorePresenter,
  );

  return createStoreController;
};

export default { makeCreateStoreController };
