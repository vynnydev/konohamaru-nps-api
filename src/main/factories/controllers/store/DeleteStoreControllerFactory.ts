import IController from '@presentation/protocols/IController';

import DeleteStoreController from '@presentation/controllers/store/DeleteStoreController';

import deleteStoreFactory from '@main/factories/useCases/store/DeleteStoreFactory';
import deleteStorePresenterFactory from '@main/factories/presenters/store/DeleteStorePresenterFactory';

const makeDeleteStoreController = (): IController => {
  const deleteStore = deleteStoreFactory.makeDeleteStore();
  const deleteStorePresenter = deleteStorePresenterFactory.makeDeleteStorePresenter();

  const deleteStoreController = new DeleteStoreController(deleteStore, deleteStorePresenter);

  return deleteStoreController;
};

export default { makeDeleteStoreController };
