import IController from '@presentation/protocols/IController';

import UpdateStoreController from '@presentation/controllers/store/UpdateStoreController';

import UpdateStoreFactory from '@main/factories/useCases/store/UpdateStoreFactory';
import UpdateStorePresenterFactory from '@main/factories/presenters/store/UpdateStorePresenterFactory';
import UpdateStoreValidationFactory from '@main/factories/controllers/store/UpdateStoreValidationFactory';

const makeUpdateStoreController = (): IController => {
  const updateStore = UpdateStoreFactory.makeUpdateStore();
  const updateStorePresenter = UpdateStorePresenterFactory.makeUpdateStorePresenter();
  const updateStoreValidation = UpdateStoreValidationFactory.makeUpdateStoreValidation();

  const updateStoreController = new UpdateStoreController(
    updateStore,
    updateStoreValidation,
    updateStorePresenter,
  );

  return updateStoreController;
};

export default { makeUpdateStoreController };
