import IController from '@presentation/protocols/IController';

import FindStoresController from '@presentation/controllers/store/FindStoresController';

import findStoresFactory from '@main/factories/useCases/store/FindStoresFactory';
import findStoresPresenterFactory from '@main/factories/presenters/store/FindStoresPresenterFactory';

const makeFindStoresController = (): IController => {
  const findStores = findStoresFactory.makeFindStores();
  const findStoresPresenter = findStoresPresenterFactory.makeFindStoresPresenter();

  const findStoresController = new FindStoresController(findStores, findStoresPresenter);

  return findStoresController;
};

export default { makeFindStoresController };
