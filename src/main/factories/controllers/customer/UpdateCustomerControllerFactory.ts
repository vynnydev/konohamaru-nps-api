import IController from '@presentation/protocols/IController';

import UpdateCustomerController from '@presentation/controllers/customer/UpdateCustomerController';

import UpdateCustomerFactory from '@main/factories/useCases/customer/UpdateCustomerFactory';
import UpdateCustomerPresenterFactory from '@main/factories/presenters/customer/UpdateCustomerPresenterFactory';
import UpdateCustomerValidationFactory from '@main/factories/controllers/customer/UpdateCustomerValidationFactory';

const makeUpdateCustomerController = (): IController => {
  const updateCustomer = UpdateCustomerFactory.makeUpdateCustomer();
  const updateCustomerPresenter = UpdateCustomerPresenterFactory.makeUpdateCustomerPresenter();
  const updateCustomerValidation = UpdateCustomerValidationFactory.makeUpdateCustomerValidation();

  const updateCustomerController = new UpdateCustomerController(
    updateCustomer,
    updateCustomerValidation,
    updateCustomerPresenter,
  );

  return updateCustomerController;
};

export default { makeUpdateCustomerController };
