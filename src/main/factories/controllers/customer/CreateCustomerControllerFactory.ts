import IController from '@presentation/protocols/IController';

import CreateCustomerController from '@presentation/controllers/customer/CreateCustomerController';

import createCustomerFactory from '@main/factories/useCases/customer/CreateCustomerFactory';
import createCustomerPresenterFactory from '@main/factories/presenters/customer/CreateCustomerPresenterFactory';
import createCustomerValidationFactory from '@main/factories/controllers/customer/CreateCustomerValidationFactory';

const makeCreateCustomerController = (): IController => {
  const createCustomer = createCustomerFactory.makeCreateCustomer();
  const createCustomerPresenter = createCustomerPresenterFactory.makeCreateCustomerPresenter();
  const createCustomerValidation = createCustomerValidationFactory.makeCreateCustomerValidation();

  const createCustomerController = new CreateCustomerController(
    createCustomer,
    createCustomerValidation,
    createCustomerPresenter,
  );

  return createCustomerController;
};

export default { makeCreateCustomerController };
