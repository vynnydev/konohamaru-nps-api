import IController from '@presentation/protocols/IController';

import DeleteCustomerController from '@presentation/controllers/customer/DeleteCustomerController';

import deleteCustomerFactory from '@main/factories/useCases/customer/DeleteCustomerFactory';
import deleteCustomerPresenterFactory from '@main/factories/presenters/customer/DeleteCustomerPresenterFactory';

const makeDeleteCustomerController = (): IController => {
  const deleteCustomer = deleteCustomerFactory.makeDeleteCustomer();
  const deleteCustomerPresenter = deleteCustomerPresenterFactory.makeDeleteCustomerPresenter();

  const deleteCustomerController = new DeleteCustomerController(
    deleteCustomer,
    deleteCustomerPresenter,
  );

  return deleteCustomerController;
};

export default { makeDeleteCustomerController };
