import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import nameValidationFactory from '@main/factories/validators/common/NameValidationFactory';
import cpfValidationFactory from '@main/factories/validators/common/CpfValidationFactory';
import emailValidationFactory from '@main/factories/validators/common/EmailValidationFactory';
import phoneNumberValidationFactory from '@main/factories/validators/common/PhoneNumberValidationFactory';

const makeCreateCustomerValidation = (): ValidationComposite => {
  const nameValidation = nameValidationFactory.makeNameValidation();
  const cpfValidation = cpfValidationFactory.makeCpfValidation();
  const emailValidation = emailValidationFactory.makeEmailValidation();
  const phoneNumberValidation = phoneNumberValidationFactory.makePhoneNumberValidation();

  const validations = [nameValidation, cpfValidation, emailValidation, phoneNumberValidation];

  const validationComposite = new ValidationComposite(validations);

  return validationComposite;
};

export default { makeCreateCustomerValidation };
