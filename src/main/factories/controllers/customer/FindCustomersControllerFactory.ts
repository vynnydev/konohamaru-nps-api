import IController from '@presentation/protocols/IController';

import FindCustomersController from '@presentation/controllers/customer/FindCustomersController';

import findCustomersFactory from '@main/factories/useCases/customer/FindCustomersFactory';
import findCustomersPresenterFactory from '@main/factories/presenters/customer/FindCustomersPresenterFactory';

const makeFindCustomersController = (): IController => {
  const findCustomers = findCustomersFactory.makeFindCustomers();
  const findCustomersPresenter = findCustomersPresenterFactory.makeFindCustomersPresenter();

  const findCustomersController = new FindCustomersController(
    findCustomers,
    findCustomersPresenter,
  );

  return findCustomersController;
};

export default { makeFindCustomersController };
