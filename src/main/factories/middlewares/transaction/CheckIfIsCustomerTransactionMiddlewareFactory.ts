import TypeOrmTransactionRepositoryFactory from '@main/factories/repositories/typeOrm/transaction/TypeOrmTransactionRepositoryFactory';
import TypeOrmCustomerRepositoryFactory from '@main/factories/repositories/typeOrm/customer/TypeOrmCustomerRepositoryFactory';

import IMiddleware from '@presentation/protocols/IMiddleware';
import CheckIfIsCustomerTransactionMiddleware from '@presentation/middlewares/transaction/CheckIfIsCustomerTransactionMiddleware';

const makeCheckIfIsCustomerTransactionMiddleware = (): IMiddleware => {
  const typeOrmCustomerRepository =
    TypeOrmCustomerRepositoryFactory.makeTypeOrmCustomerRepository();
  const typeOrmTransactionRepository =
    TypeOrmTransactionRepositoryFactory.makeTypeOrmTransactionRepository();

  const checkIfIsCustomerTransactionMiddleware = new CheckIfIsCustomerTransactionMiddleware(
    typeOrmCustomerRepository,
    typeOrmTransactionRepository,
  );

  return checkIfIsCustomerTransactionMiddleware;
};

export default { makeCheckIfIsCustomerTransactionMiddleware };
