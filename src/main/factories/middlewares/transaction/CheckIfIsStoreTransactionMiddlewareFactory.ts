import TypeOrmTransactionRepositoryFactory from '@main/factories/repositories/typeOrm/transaction/TypeOrmTransactionRepositoryFactory';
import TypeOrmStoreRepositoryFactory from '@main/factories/repositories/typeOrm/store/TypeOrmStoreRepositoryFactory';

import IMiddleware from '@presentation/protocols/IMiddleware';
import CheckIfIsStoreTransactionMiddleware from '@presentation/middlewares/transaction/CheckIfIsStoreTransactionMiddleware';

const makeCheckIfIsStoreTransactionMiddleware = (): IMiddleware => {
  const typeOrmStoreRepository = TypeOrmStoreRepositoryFactory.makeTypeOrmStoreRepository();
  const typeOrmTransactionRepository =
    TypeOrmTransactionRepositoryFactory.makeTypeOrmTransactionRepository();

  const checkIfIsStoreTransactionMiddleware = new CheckIfIsStoreTransactionMiddleware(
    typeOrmStoreRepository,
    typeOrmTransactionRepository,
  );

  return checkIfIsStoreTransactionMiddleware;
};

export default { makeCheckIfIsStoreTransactionMiddleware };
