import TypeOrmTransactionRepositoryFactory from '@main/factories/repositories/typeOrm/transaction/TypeOrmTransactionRepositoryFactory';
import TypeOrmEmployeeRepositoryFactory from '@main/factories/repositories/typeOrm/employee/TypeOrmEmployeeRepositoryFactory';

import IMiddleware from '@presentation/protocols/IMiddleware';
import CheckIfIsEmployeeTransactionMiddleware from '@presentation/middlewares/transaction/CheckIfIsEmployeeTransactionMiddleware';

const makeCheckIfIsEmployeeTransactionMiddleware = (): IMiddleware => {
  const typeOrmEmployeeRepository =
    TypeOrmEmployeeRepositoryFactory.makeTypeOrmEmployeeRepository();
  const typeOrmTransactionRepository =
    TypeOrmTransactionRepositoryFactory.makeTypeOrmTransactionRepository();

  const checkIfIsEmployeeTransactionMiddleware = new CheckIfIsEmployeeTransactionMiddleware(
    typeOrmEmployeeRepository,
    typeOrmTransactionRepository,
  );

  return checkIfIsEmployeeTransactionMiddleware;
};

export default { makeCheckIfIsEmployeeTransactionMiddleware };
