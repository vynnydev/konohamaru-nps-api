import TypeOrmTransactionRepositoryFactory from '@main/factories/repositories/typeOrm/transaction/TypeOrmTransactionRepositoryFactory';
import TypeOrmEvaluationRepositoryFactory from '@main/factories/repositories/typeOrm/evaluation/TypeOrmEvaluationRepositoryFactory';

import IMiddleware from '@presentation/protocols/IMiddleware';
import CheckIfExistsTransactionEvaluationMiddleware from '@presentation/middlewares/evaluation/CheckIfExistsTransactionEvaluationMiddleware';

const makeCheckIfExistsTransactionEvaluationMiddleware = (): IMiddleware => {
  const typeOrmEvaluationRepository =
    TypeOrmEvaluationRepositoryFactory.makeTypeOrmEvaluationRepository();
  const typeOrmTransactionRepository =
    TypeOrmTransactionRepositoryFactory.makeTypeOrmTransactionRepository();

  const checkIfExistsTransactionEvaluationMiddleware =
    new CheckIfExistsTransactionEvaluationMiddleware(
      typeOrmTransactionRepository,
      typeOrmEvaluationRepository,
    );

  return checkIfExistsTransactionEvaluationMiddleware;
};

export default { makeCheckIfExistsTransactionEvaluationMiddleware };
