import ITransactionValueValidator from '@utils/validation/protocols/evaluation/ITransactionValueValidator';
import JoiTransactionValueValidatorAdapter from '@infra/adapters/validators/evaluation/joi/TransactionValueValidatorAdapter';

const makeJoiTransactionValueValidatorAdapter = (): ITransactionValueValidator => {
  const joiTransactionValueValidatorAdapter = new JoiTransactionValueValidatorAdapter();

  return joiTransactionValueValidatorAdapter;
};

export default { makeJoiTransactionValueValidatorAdapter };
