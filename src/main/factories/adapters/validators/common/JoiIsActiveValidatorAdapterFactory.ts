import IIsActiveValidator from '@utils/validation/protocols/common/IIsActiveValidator';

import JoiIsActiveValidatorAdapter from '@infra/adapters/validators/common/joi/IsActiveValidatorAdapter';

const makeJoiIsActiveValidatorAdapter = (): IIsActiveValidator => {
  const joiIsActiveValidatorAdapter = new JoiIsActiveValidatorAdapter();

  return joiIsActiveValidatorAdapter;
};

export default { makeJoiIsActiveValidatorAdapter };
