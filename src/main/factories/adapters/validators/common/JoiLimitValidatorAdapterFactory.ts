import ILimitValidator from '@utils/validation/protocols/common/ILimitValidator';
import JoiLimitValidatorAdapter from '@infra/adapters/validators/common/joi/LimitValidatorAdapter';

const makeJoiLimitValidatorAdapter = (): ILimitValidator => {
  const joiLimitValidatorAdapter = new JoiLimitValidatorAdapter();

  return joiLimitValidatorAdapter;
};

export default { makeJoiLimitValidatorAdapter };
