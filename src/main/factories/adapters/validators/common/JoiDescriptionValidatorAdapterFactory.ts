import IDescriptionValidator from '@utils/validation/protocols/common/IDescriptionValidator';
import JoiDescriptionValidatorAdapter from '@infra/adapters/validators/common/joi/DescriptionValidatorAdapter';

const makeJoiDescriptionValidatorAdapter = (): IDescriptionValidator => {
  const joiDescriptionValidatorAdapter = new JoiDescriptionValidatorAdapter();

  return joiDescriptionValidatorAdapter;
};

export default { makeJoiDescriptionValidatorAdapter };
