import IPageValidator from '@utils/validation/protocols/common/IPageValidator';
import JoiPageValidatorAdapter from '@infra/adapters/validators/common/joi/PageValidatorAdapter';

const makeJoiPageValidatorAdapter = (): IPageValidator => {
  const joiPageValidatorAdapter = new JoiPageValidatorAdapter();

  return joiPageValidatorAdapter;
};

export default { makeJoiPageValidatorAdapter };
