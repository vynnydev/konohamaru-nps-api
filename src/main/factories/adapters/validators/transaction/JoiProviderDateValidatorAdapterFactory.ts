import IProviderDateValidator from '@utils/validation/protocols/transaction/IProviderDateValidator';
import JoiProviderDateValidatorAdapter from '@infra/adapters/validators/transaction/joi/ProviderDateValidatorAdapter';

const makeJoiProviderDateValidatorAdapter = (): IProviderDateValidator => {
  const joiProviderDateValidatorAdapter = new JoiProviderDateValidatorAdapter();

  return joiProviderDateValidatorAdapter;
};

export default { makeJoiProviderDateValidatorAdapter };
