import IStoreEvaluationValidator from '@utils/validation/protocols/transaction/IStoreEvaluationValidator';
import JoiStoreEvaluationValidatorAdapter from '@infra/adapters/validators/transaction/joi/StoreEvaluationValidatorAdapter';

const makeJoiStoreEvaluationValidatorAdapter = (): IStoreEvaluationValidator => {
  const joiStoreEvaluationValidatorAdapter = new JoiStoreEvaluationValidatorAdapter();

  return joiStoreEvaluationValidatorAdapter;
};

export default { makeJoiStoreEvaluationValidatorAdapter };
