import IValidation from '@presentation/protocols/IValidation';

import IsActiveValidation from '@utils/validation/validators/common/IsActiveValidation';
import joiIsActiveValidatorAdapterFactory from '@main/factories/adapters/validators/common/JoiIsActiveValidatorAdapterFactory';

const makeIsActiveValidation = (): IValidation => {
  const fieldName = 'is_active';
  const joiIsActiveValidatorAdapter =
    joiIsActiveValidatorAdapterFactory.makeJoiIsActiveValidatorAdapter();

  const isActiveValidation = new IsActiveValidation(fieldName, joiIsActiveValidatorAdapter);

  return isActiveValidation;
};

export default { makeIsActiveValidation };
