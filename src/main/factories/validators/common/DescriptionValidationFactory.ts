import IValidation from '@presentation/protocols/IValidation';

import DescriptionValidation from '@utils/validation/validators/common/DescriptionValidation';
import joiDescriptionValidatorAdapterFactory from '@main/factories/adapters/validators/common/JoiDescriptionValidatorAdapterFactory';

const makeDescriptionValidation = (): IValidation => {
  const fieldDescription = 'description';
  const joiDescriptionValidatorAdapter =
    joiDescriptionValidatorAdapterFactory.makeJoiDescriptionValidatorAdapter();

  const nameValidation = new DescriptionValidation(
    fieldDescription,
    joiDescriptionValidatorAdapter,
  );

  return nameValidation;
};

export default { makeDescriptionValidation };
