import IValidation from '@presentation/protocols/IValidation';

import PageValidation from '@utils/validation/validators/common/PageValidation';
import joiPageValidatorAdapterFactory from '@main/factories/adapters/validators/common/JoiPageValidatorAdapterFactory';

const makePageValidation = (): IValidation => {
  const fieldName = 'page';
  const joiPageValidatorAdapter = joiPageValidatorAdapterFactory.makeJoiPageValidatorAdapter();

  const pageValidation = new PageValidation(fieldName, joiPageValidatorAdapter);

  return pageValidation;
};

export default { makePageValidation };
