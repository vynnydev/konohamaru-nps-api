import IValidation from '@presentation/protocols/IValidation';

import LimitValidation from '@utils/validation/validators/common/LimitValidation';
import joiLimitValidatorAdapterFactory from '@main/factories/adapters/validators/common/JoiLimitValidatorAdapterFactory';

const makeLimitValidation = (): IValidation => {
  const fieldLimit = 'limit';
  const joiLimitValidatorAdapter = joiLimitValidatorAdapterFactory.makeJoiLimitValidatorAdapter();

  const limitValidation = new LimitValidation(fieldLimit, joiLimitValidatorAdapter);

  return limitValidation;
};

export default { makeLimitValidation };
