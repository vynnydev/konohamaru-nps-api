import IValidation from '@presentation/protocols/IValidation';

import StoreEvaluationValidation from '@utils/validation/validators/transaction/StoreEvaluationValidation';
import joiStoreEvaluationValidatorAdapterFactory from '@main/factories/adapters/validators/transaction/JoiStoreEvaluationValidatorAdapterFactory';

const makeStoreEvaluationValidation = (): IValidation => {
  const fieldName = 'store_evaluation';

  const joiStoreEvaluationValidatorAdapter =
    joiStoreEvaluationValidatorAdapterFactory.makeJoiStoreEvaluationValidatorAdapter();

  const currencyValidation = new StoreEvaluationValidation(
    fieldName,
    joiStoreEvaluationValidatorAdapter,
  );

  return currencyValidation;
};

export default { makeStoreEvaluationValidation };
