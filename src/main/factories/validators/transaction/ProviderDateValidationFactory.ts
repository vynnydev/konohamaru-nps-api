import IValidation from '@presentation/protocols/IValidation';

import ProviderDateValidation from '@utils/validation/validators/transaction/ProviderDateValidation';
import joiProviderDateValidatorAdapterFactory from '@main/factories/adapters/validators/transaction/JoiProviderDateValidatorAdapterFactory';

const makeProviderDateValidation = (): IValidation => {
  const fieldName = 'provider_date';
  const joiProviderDateValidatorAdapter =
    joiProviderDateValidatorAdapterFactory.makeJoiProviderDateValidatorAdapter();

  const providerDateValidation = new ProviderDateValidation(
    fieldName,
    joiProviderDateValidatorAdapter,
  );

  return providerDateValidation;
};

export default { makeProviderDateValidation };
