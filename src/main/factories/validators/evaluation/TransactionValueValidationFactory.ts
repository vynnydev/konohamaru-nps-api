import IValidation from '@presentation/protocols/IValidation';

import TransactionValueValidation from '@utils/validation/validators/evaluation/TransactionValueValidation';
import joiTransactionValueValidatorAdapterFactory from '@main/factories/adapters/validators/evaluation/JoiTransactionValueValidatorAdapterFactory';

const makeTransactionValueValidation = (): IValidation => {
  const fieldTransactionValue = 'transaction_value';
  const joiTransactionValueValidatorAdapter =
    joiTransactionValueValidatorAdapterFactory.makeJoiTransactionValueValidatorAdapter();

  const nameValidation = new TransactionValueValidation(
    fieldTransactionValue,
    joiTransactionValueValidatorAdapter,
  );

  return nameValidation;
};

export default { makeTransactionValueValidation };
