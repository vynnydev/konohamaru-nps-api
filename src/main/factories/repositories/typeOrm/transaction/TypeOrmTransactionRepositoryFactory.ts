import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';
import TypeOrmTransactionRepository from '@infra/database/repositories/transaction/typeOrm/TypeOrmTransactionRepository';

const makeTypeOrmTransactionRepository = (): ITransactionRepository => {
  const typeOrmTransactionRepository = new TypeOrmTransactionRepository();

  return typeOrmTransactionRepository;
};

export default { makeTypeOrmTransactionRepository };
