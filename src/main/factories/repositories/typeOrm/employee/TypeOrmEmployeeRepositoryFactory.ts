import IEmployeeRepository from '@data/protocols/database/repositories/employee/IEmployeeRepository';
import TypeOrmEmployeeRepository from '@infra/database/repositories/employee/typeOrm/TypeOrmEmployeeRepository';

const makeTypeOrmEmployeeRepository = (): IEmployeeRepository => {
  const typeOrmEmployeeRepository = new TypeOrmEmployeeRepository();

  return typeOrmEmployeeRepository;
};

export default { makeTypeOrmEmployeeRepository };
