import ICustomerRepository from '@data/protocols/database/repositories/customer/ICustomerRepository';
import TypeOrmCustomerRepository from '@infra/database/repositories/customer/typeOrm/TypeOrmCustomerRepository';

const makeTypeOrmCustomerRepository = (): ICustomerRepository => {
  const typeOrmCustomerRepository = new TypeOrmCustomerRepository();

  return typeOrmCustomerRepository;
};

export default { makeTypeOrmCustomerRepository };
