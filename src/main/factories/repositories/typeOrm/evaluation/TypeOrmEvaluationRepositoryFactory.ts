import IEvaluationRepository from '@data/protocols/database/repositories/evaluation/IEvaluationRepository';
import TypeOrmEvaluationRepository from '@infra/database/repositories/evaluation/typeOrm/TypeOrmEvaluationRepository';

const makeTypeOrmEvaluationRepository = (): IEvaluationRepository => {
  const typeOrmEvaluationRepository = new TypeOrmEvaluationRepository();

  return typeOrmEvaluationRepository;
};

export default { makeTypeOrmEvaluationRepository };
