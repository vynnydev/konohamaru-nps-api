import IStoreRepository from '@data/protocols/database/repositories/store/IStoreRepository';
import TypeOrmStoreRepository from '@infra/database/repositories/store/typeOrm/TypeOrmStoreRepository';

const makeTypeOrmStoreRepository = (): IStoreRepository => {
  const typeOrmStoreRepository = new TypeOrmStoreRepository();

  return typeOrmStoreRepository;
};

export default { makeTypeOrmStoreRepository };
