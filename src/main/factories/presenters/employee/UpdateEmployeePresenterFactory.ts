import IPresenter from '@presentation/protocols/IPresenter';

import UpdateEmployeePresenter from '@presentation/presenters/employee/UpdateEmployeePresenter';

const makeUpdateEmployeePresenter = (): IPresenter => {
  const updateEmployeePresenter = new UpdateEmployeePresenter();

  return updateEmployeePresenter;
};

export default { makeUpdateEmployeePresenter };
