import IPresenter from '@presentation/protocols/IPresenter';

import DeleteEmployeePresenter from '@presentation/presenters/employee/DeleteEmployeePresenter';

const makeDeleteEmployeePresenter = (): IPresenter => {
  const deleteEmployeePresenter = new DeleteEmployeePresenter();

  return deleteEmployeePresenter;
};

export default { makeDeleteEmployeePresenter };
