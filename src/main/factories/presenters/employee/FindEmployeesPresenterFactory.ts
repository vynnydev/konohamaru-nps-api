import IPresenter from '@presentation/protocols/IPresenter';

import FindEmployeesPresenter from '@presentation/presenters/employee/FindEmployeesPresenter';

const makeFindEmployeesPresenter = (): IPresenter => {
  const findEmployeesPresenter = new FindEmployeesPresenter();

  return findEmployeesPresenter;
};

export default { makeFindEmployeesPresenter };
