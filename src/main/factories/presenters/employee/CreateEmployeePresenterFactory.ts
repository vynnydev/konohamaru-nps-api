import IPresenter from '@presentation/protocols/IPresenter';

import CreateEmployeePresenter from '@presentation/presenters/employee/CreateEmployeePresenter';

const makeCreateEmployeePresenter = (): IPresenter => {
  const createEmployeePresenter = new CreateEmployeePresenter();

  return createEmployeePresenter;
};

export default { makeCreateEmployeePresenter };
