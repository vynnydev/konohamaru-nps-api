
import IPresenter from '@presentation/protocols/IPresenter';

import CreateTransactionPresenter from '@presentation/presenters/transaction/CreateTransactionPresenter';

const makeCreateTransactionPresenter = (): IPresenter => {
  const createTransactionPresenter = new CreateTransactionPresenter();

  return createTransactionPresenter;
};

export default { makeCreateTransactionPresenter };
