
import IPresenter from '@presentation/protocols/IPresenter';

import UpdateTransactionPresenter from '@presentation/presenters/transaction/UpdateTransactionPresenter';

const makeUpdateTransactionPresenter = (): IPresenter => {
  const updateTransactionPresenter = new UpdateTransactionPresenter();

  return updateTransactionPresenter;
};

export default { makeUpdateTransactionPresenter };
