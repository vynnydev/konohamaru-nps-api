import IPresenter from '@presentation/protocols/IPresenter';

import DeleteCustomerPresenter from '@presentation/presenters/customer/DeleteCustomerPresenter';

const makeDeleteCustomerPresenter = (): IPresenter => {
  const deleteCustomerPresenter = new DeleteCustomerPresenter();

  return deleteCustomerPresenter;
};

export default { makeDeleteCustomerPresenter };
