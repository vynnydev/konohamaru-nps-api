import IPresenter from '@presentation/protocols/IPresenter';

import UpdateCustomerPresenter from '@presentation/presenters/customer/UpdateCustomerPresenter';

const makeUpdateCustomerPresenter = (): IPresenter => {
  const updateCustomerPresenter = new UpdateCustomerPresenter();

  return updateCustomerPresenter;
};

export default { makeUpdateCustomerPresenter };
