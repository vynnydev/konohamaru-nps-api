import IPresenter from '@presentation/protocols/IPresenter';

import FindCustomersPresenter from '@presentation/presenters/customer/FindCustomersPresenter';

const makeFindCustomersPresenter = (): IPresenter => {
  const findCustomersPresenter = new FindCustomersPresenter();

  return findCustomersPresenter;
};

export default { makeFindCustomersPresenter };
