import IPresenter from '@presentation/protocols/IPresenter';

import CreateCustomerPresenter from '@presentation/presenters/customer/CreateCustomerPresenter';

const makeCreateCustomerPresenter = (): IPresenter => {
  const createCustomerPresenter = new CreateCustomerPresenter();

  return createCustomerPresenter;
};

export default { makeCreateCustomerPresenter };
