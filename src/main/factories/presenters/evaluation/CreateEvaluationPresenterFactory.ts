import IPresenter from '@presentation/protocols/IPresenter';

import CreateEvaluationPresenter from '@presentation/presenters/evaluation/CreateEvaluationPresenter';

const makeCreateEvaluationPresenter = (): IPresenter => {
  const createEvaluationPresenter = new CreateEvaluationPresenter();

  return createEvaluationPresenter;
};

export default { makeCreateEvaluationPresenter };
