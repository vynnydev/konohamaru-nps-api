import IPresenter from '@presentation/protocols/IPresenter';

import FindEvaluationsPresenter from '@presentation/presenters/evaluation/FindEvaluationsPresenter';

const makeFindEvaluationsPresenter = (): IPresenter => {
  const findEvaluationsPresenter = new FindEvaluationsPresenter();

  return findEvaluationsPresenter;
};

export default { makeFindEvaluationsPresenter };
