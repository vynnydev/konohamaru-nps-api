import IPresenter from '@presentation/protocols/IPresenter';

import DeleteStorePresenter from '@presentation/presenters/store/DeleteStorePresenter';

const makeDeleteStorePresenter = (): IPresenter => {
  const deleteStorePresenter = new DeleteStorePresenter();

  return deleteStorePresenter;
};

export default { makeDeleteStorePresenter };
