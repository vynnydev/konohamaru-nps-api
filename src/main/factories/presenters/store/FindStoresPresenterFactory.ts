import IPresenter from '@presentation/protocols/IPresenter';

import FindStoresPresenter from '@presentation/presenters/store/FindStoresPresenter';

const makeFindStoresPresenter = (): IPresenter => {
  const findStoresPresenter = new FindStoresPresenter();

  return findStoresPresenter;
};

export default { makeFindStoresPresenter };
