import IPresenter from '@presentation/protocols/IPresenter';

import UpdateStorePresenter from '@presentation/presenters/store/UpdateStorePresenter';

const makeUpdateStorePresenter = (): IPresenter => {
  const updateStorePresenter = new UpdateStorePresenter();

  return updateStorePresenter;
};

export default { makeUpdateStorePresenter };
