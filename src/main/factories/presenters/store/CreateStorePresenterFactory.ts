import IPresenter from '@presentation/protocols/IPresenter';

import CreateStorePresenter from '@presentation/presenters/store/CreateStorePresenter';

const makeCreateStorePresenter = (): IPresenter => {
  const createStorePresenter = new CreateStorePresenter();

  return createStorePresenter;
};

export default { makeCreateStorePresenter };
