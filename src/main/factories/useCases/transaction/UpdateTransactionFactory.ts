import TypeOrmTransactionRepositoryFactory from '@main/factories/repositories/typeOrm/transaction/TypeOrmTransactionRepositoryFactory';

import IUpdateTransaction from '@domain/useCases/transaction/IUpdateTransaction';
import UpdateTransaction from '@data/useCases/transaction/UpdateTransaction';

const makeUpdateTransaction = (): IUpdateTransaction => {
  const TypeOrmTransactionRepository = TypeOrmTransactionRepositoryFactory.makeTypeOrmTransactionRepository();

  const updateTransaction = new UpdateTransaction(TypeOrmTransactionRepository);

  return updateTransaction;
};

export default { makeUpdateTransaction };
