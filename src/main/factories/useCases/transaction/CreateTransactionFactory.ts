import aliasGeneratorFactory from '@main/factories/aliasGenerator/AliasGeneratorFactory';

import typeOrmStoreRepositoryFactory from '@main/factories/repositories/typeOrm/store/TypeOrmStoreRepositoryFactory';
import typeOrmEmployeeRepositoryFactory from '@main/factories/repositories/typeOrm/employee/TypeOrmEmployeeRepositoryFactory';
import typeOrmCustomerRepositoryFactory from '@main/factories/repositories/typeOrm/customer/TypeOrmCustomerRepositoryFactory';
import TypeOrmTransactionRepositoryFactory from '@main/factories/repositories/typeOrm/transaction/TypeOrmTransactionRepositoryFactory';

import ICreateTransaction from '@domain/useCases/transaction/ICreateTransaction';
import CreateTransaction from '@data/useCases/transaction/CreateTransaction';

const makeCreateTransaction = (): ICreateTransaction => {
  const aliasGenerator = aliasGeneratorFactory.makeAliasGenerator();

  const typeOrmStoreRepository = typeOrmStoreRepositoryFactory.makeTypeOrmStoreRepository();
  const typeOrmEmployeeRepository =
    typeOrmEmployeeRepositoryFactory.makeTypeOrmEmployeeRepository();
  const typeOrmCustomerRepository =
    typeOrmCustomerRepositoryFactory.makeTypeOrmCustomerRepository();
  const TypeOrmTransactionRepository =
    TypeOrmTransactionRepositoryFactory.makeTypeOrmTransactionRepository();

  const createTransaction = new CreateTransaction(
    typeOrmStoreRepository,
    typeOrmEmployeeRepository,
    typeOrmCustomerRepository,
    TypeOrmTransactionRepository,
    aliasGenerator,
  );

  return createTransaction;
};

export default { makeCreateTransaction };
