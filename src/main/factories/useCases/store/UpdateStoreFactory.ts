import typeOrmStoreRepositoryFactory from '@main/factories/repositories/typeOrm/store/TypeOrmStoreRepositoryFactory';

import IUpdateStore from '@domain/useCases/store/IUpdateStore';
import UpdateStore from '@data/useCases/store/UpdateStore';

const makeUpdateStore = (): IUpdateStore => {
  const typeOrmStoreRepository = typeOrmStoreRepositoryFactory.makeTypeOrmStoreRepository();

  const updateStore = new UpdateStore(typeOrmStoreRepository);

  return updateStore;
};

export default { makeUpdateStore };
