import aliasGeneratorFactory from '@main/factories/aliasGenerator/AliasGeneratorFactory';

import typeOrmStoreRepositoryFactory from '@main/factories/repositories/typeOrm/store/TypeOrmStoreRepositoryFactory';

import ICreateStore from '@domain/useCases/store/ICreateStore';
import CreateStore from '@data/useCases/store/CreateStore';

const makeCreateStore = (): ICreateStore => {
  const aliasGenerator = aliasGeneratorFactory.makeAliasGenerator();

  const typeOrmStoreRepository = typeOrmStoreRepositoryFactory.makeTypeOrmStoreRepository();

  const createStore = new CreateStore(typeOrmStoreRepository, aliasGenerator);

  return createStore;
};

export default { makeCreateStore };
