import typeOrmStoreRepositoryFactory from '@main/factories/repositories/typeOrm/store/TypeOrmStoreRepositoryFactory';

import IFindStores from '@domain/useCases/store/IFindStores';
import FindStores from '@data/useCases/store/FindStores';

const makeFindStores = (): IFindStores => {
  const typeOrmStoreRepository = typeOrmStoreRepositoryFactory.makeTypeOrmStoreRepository();

  const findStores = new FindStores(typeOrmStoreRepository);

  return findStores;
};

export default { makeFindStores };
