import typeOrmStoreRepositoryFactory from '@main/factories/repositories/typeOrm/store/TypeOrmStoreRepositoryFactory';

import IDeleteStore from '@domain/useCases/store/IDeleteStore';
import DeleteStore from '@data/useCases/store/DeleteStore';

const makeDeleteStore = (): IDeleteStore => {
  const typeOrmStoreRepository = typeOrmStoreRepositoryFactory.makeTypeOrmStoreRepository();

  const deleteStore = new DeleteStore(typeOrmStoreRepository);

  return deleteStore;
};

export default { makeDeleteStore };
