import typeOrmEmployeeRepositoryFactory from '@main/factories/repositories/typeOrm/employee/TypeOrmEmployeeRepositoryFactory';

import IDeleteEmployee from '@domain/useCases/employee/IDeleteEmployee';
import DeleteEmployee from '@data/useCases/employee/DeleteEmployee';

const makeDeleteEmployee = (): IDeleteEmployee => {
  const typeOrmEmployeeRepository =
    typeOrmEmployeeRepositoryFactory.makeTypeOrmEmployeeRepository();

  const deleteEmployee = new DeleteEmployee(typeOrmEmployeeRepository);

  return deleteEmployee;
};

export default { makeDeleteEmployee };
