import typeOrmEmployeeRepositoryFactory from '@main/factories/repositories/typeOrm/employee/TypeOrmEmployeeRepositoryFactory';

import IFindEmployees from '@domain/useCases/employee/IFindEmployees';
import FindEmployees from '@data/useCases/employee/FindEmployees';

const makeFindEmployees = (): IFindEmployees => {
  const typeOrmEmployeeRepository =
    typeOrmEmployeeRepositoryFactory.makeTypeOrmEmployeeRepository();

  const findEmployees = new FindEmployees(typeOrmEmployeeRepository);

  return findEmployees;
};

export default { makeFindEmployees };
