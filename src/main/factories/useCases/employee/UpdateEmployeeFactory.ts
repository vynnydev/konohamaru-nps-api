import typeOrmEmployeeRepositoryFactory from '@main/factories/repositories/typeOrm/employee/TypeOrmEmployeeRepositoryFactory';

import IUpdateEmployee from '@domain/useCases/employee/IUpdateEmployee';
import UpdateEmployee from '@data/useCases/employee/UpdateEmployee';

const makeUpdateEmployee = (): IUpdateEmployee => {
  const typeOrmEmployeeRepository = typeOrmEmployeeRepositoryFactory.makeTypeOrmEmployeeRepository();

  const updateEmployee = new UpdateEmployee(typeOrmEmployeeRepository);

  return updateEmployee;
};

export default { makeUpdateEmployee };
