import aliasGeneratorFactory from '@main/factories/aliasGenerator/AliasGeneratorFactory';

import typeOrmEmployeeRepositoryFactory from '@main/factories/repositories/typeOrm/employee/TypeOrmEmployeeRepositoryFactory';

import ICreateEmployee from '@domain/useCases/employee/ICreateEmployee';
import CreateEmployee from '@data/useCases/employee/CreateEmployee';

const makeCreateEmployee = (): ICreateEmployee => {
  const aliasGenerator = aliasGeneratorFactory.makeAliasGenerator();
  const typeOrmEmployeeRepository =
    typeOrmEmployeeRepositoryFactory.makeTypeOrmEmployeeRepository();

  const createEmployee = new CreateEmployee(typeOrmEmployeeRepository, aliasGenerator);

  return createEmployee;
};

export default { makeCreateEmployee };
