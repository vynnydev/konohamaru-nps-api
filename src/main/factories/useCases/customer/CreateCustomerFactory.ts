import aliasGeneratorFactory from '@main/factories/aliasGenerator/AliasGeneratorFactory';

import typeOrmCustomerRepositoryFactory from '@main/factories/repositories/typeOrm/customer/TypeOrmCustomerRepositoryFactory';

import ICreateCustomer from '@domain/useCases/customer/ICreateCustomer';
import CreateCustomer from '@data/useCases/customer/CreateCustomer';

const makeCreateCustomer = (): ICreateCustomer => {
  const aliasGenerator = aliasGeneratorFactory.makeAliasGenerator();
  const typeOrmCustomerRepository =
    typeOrmCustomerRepositoryFactory.makeTypeOrmCustomerRepository();

  const createCustomer = new CreateCustomer(typeOrmCustomerRepository, aliasGenerator);

  return createCustomer;
};

export default { makeCreateCustomer };
