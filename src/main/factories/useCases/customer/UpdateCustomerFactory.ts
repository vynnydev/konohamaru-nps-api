import typeOrmCustomerRepositoryFactory from '@main/factories/repositories/typeOrm/customer/TypeOrmCustomerRepositoryFactory';

import IUpdateCustomer from '@domain/useCases/customer/IUpdateCustomer';
import UpdateCustomer from '@data/useCases/customer/UpdateCustomer';

const makeUpdateCustomer = (): IUpdateCustomer => {
  const typeOrmCustomerRepository = typeOrmCustomerRepositoryFactory.makeTypeOrmCustomerRepository();

  const updateCustomer = new UpdateCustomer(typeOrmCustomerRepository);

  return updateCustomer;
};

export default { makeUpdateCustomer };
