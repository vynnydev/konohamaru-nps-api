import typeOrmCustomerRepositoryFactory from '@main/factories/repositories/typeOrm/customer/TypeOrmCustomerRepositoryFactory';

import IFindCustomers from '@domain/useCases/customer/IFindCustomers';
import FindCustomers from '@data/useCases/customer/FindCustomers';

const makeFindCustomers = (): IFindCustomers => {
  const typeOrmCustomerRepository =
    typeOrmCustomerRepositoryFactory.makeTypeOrmCustomerRepository();

  const findCustomers = new FindCustomers(typeOrmCustomerRepository);

  return findCustomers;
};

export default { makeFindCustomers };
