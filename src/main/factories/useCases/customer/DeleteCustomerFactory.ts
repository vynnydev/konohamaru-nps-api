import typeOrmCustomerRepositoryFactory from '@main/factories/repositories/typeOrm/customer/TypeOrmCustomerRepositoryFactory';

import IDeleteCustomer from '@domain/useCases/customer/IDeleteCustomer';
import DeleteCustomer from '@data/useCases/customer/DeleteCustomer';

const makeDeleteCustomer = (): IDeleteCustomer => {
  const typeOrmCustomerRepository = typeOrmCustomerRepositoryFactory.makeTypeOrmCustomerRepository();

  const deleteCustomer = new DeleteCustomer(typeOrmCustomerRepository);

  return deleteCustomer;
};

export default { makeDeleteCustomer };
