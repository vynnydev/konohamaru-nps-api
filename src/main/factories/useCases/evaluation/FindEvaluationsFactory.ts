import TypeOrmTransactionRepositoryFactory from '@main/factories/repositories/typeOrm/transaction/TypeOrmTransactionRepositoryFactory';
import TypeOrmEvaluationRepositoryFactory from '@main/factories/repositories/typeOrm/evaluation/TypeOrmEvaluationRepositoryFactory';

import IFindEvaluations from '@domain/useCases/evaluation/IFindEvaluations';
import FindEvaluations from '@data/useCases/evaluation/FindEvaluations';

const makeFindEvaluations = (): IFindEvaluations => {
  const TypeOrmTransactionRepository = TypeOrmTransactionRepositoryFactory.makeTypeOrmTransactionRepository();
  const TypeOrmEvaluationRepository = TypeOrmEvaluationRepositoryFactory.makeTypeOrmEvaluationRepository();

  const findEvaluations = new FindEvaluations(
    TypeOrmTransactionRepository,
    TypeOrmEvaluationRepository,
  );

  return findEvaluations;
};

export default { makeFindEvaluations };
