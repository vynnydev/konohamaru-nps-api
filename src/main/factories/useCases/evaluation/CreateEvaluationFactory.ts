import aliasGeneratorFactory from '@main/factories/aliasGenerator/AliasGeneratorFactory';

import TypeOrmTransactionRepositoryFactory from '@main/factories/repositories/typeOrm/transaction/TypeOrmTransactionRepositoryFactory';
import TypeOrmEvaluationRepositoryFactory from '@main/factories/repositories/typeOrm/evaluation/TypeOrmEvaluationRepositoryFactory';

import ICreateEvaluation from '@domain/useCases/evaluation/ICreateEvaluation';
import CreateEvaluation from '@data/useCases/evaluation/CreateEvaluation';

const makeCreateEvaluation = (): ICreateEvaluation => {
  const aliasGenerator = aliasGeneratorFactory.makeAliasGenerator();

  const TypeOrmTransactionRepository = TypeOrmTransactionRepositoryFactory.makeTypeOrmTransactionRepository();
  const TypeOrmEvaluationRepository = TypeOrmEvaluationRepositoryFactory.makeTypeOrmEvaluationRepository();

  const createEvaluation = new CreateEvaluation(
    TypeOrmTransactionRepository,
    TypeOrmEvaluationRepository,
    aliasGenerator,
  );

  return createEvaluation;
};

export default { makeCreateEvaluation };
