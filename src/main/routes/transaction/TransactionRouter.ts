import { Router } from 'express';

import { routeAdapter } from '@main/adapters/ExpressRouteAdapter';

import checkIfIsStoreTransaction from '@main/middlewares/transaction/CheckIfIsStoreTransaction';
import CheckIfIsEmployeeTransaction from '@main/middlewares/transaction/CheckIfIsEmployeeTransaction';
import CheckIfIsCustomerTransaction from '@main/middlewares/transaction/CheckIfIsCustomerTransaction';

import createTransactionControllerFactory from '@main/factories/controllers/transaction/CreateTransactionControllerFactory';
import updateTransactionControllerFactory from '@main/factories/controllers/transaction/UpdateTransactionControllerFactory';

const router = Router();

router
  .route(
    '/stores/:store_alias_id/employees/:employee_alias_id/customers/:customer_alias_id/transactions',
  )
  .post(routeAdapter(createTransactionControllerFactory.makeCreateTransactionController()));

router
  .route(
    '/stores/:store_alias_id/employees/:employee_alias_id/customers/:customer_alias_id/transactions/:transaction_alias_id',
  )
  .put(
    checkIfIsStoreTransaction,
    CheckIfIsEmployeeTransaction,
    CheckIfIsCustomerTransaction,
    routeAdapter(updateTransactionControllerFactory.makeUpdateTransactionController()),
  );

export default { router };
