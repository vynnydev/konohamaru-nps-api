import { Router } from 'express';

import { routeAdapter } from '@main/adapters/ExpressRouteAdapter';

import createStoreControllerFactory from '@main/factories/controllers/store/CreateStoreControllerFactory';
import findStoresControllerFactory from '@main/factories/controllers/store/FindStoresControllerFactory';
import updateStoreControllerFactory from '@main/factories/controllers/store/UpdateStoreControllerFactory';
import deleteStoreControllerFactory from '@main/factories/controllers/store/DeleteStoreControllerFactory';

const router = Router();

router
  .route('/stores')
  .post(routeAdapter(createStoreControllerFactory.makeCreateStoreController()))
  .get(routeAdapter(findStoresControllerFactory.makeFindStoresController()));

router
  .route('/stores/:store_alias_id')
  .put(routeAdapter(updateStoreControllerFactory.makeUpdateStoreController()))
  .delete(routeAdapter(deleteStoreControllerFactory.makeDeleteStoreController()));

export default { router };
