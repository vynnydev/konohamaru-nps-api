import { Router } from 'express';

import { routeAdapter } from '@main/adapters/ExpressRouteAdapter';

import createEmployeeControllerFactory from '@main/factories/controllers/employee/CreateEmployeeControllerFactory';
import findEmployeesControllerFactory from '@main/factories/controllers/employee/FindEmployeesControllerFactory';
import updateEmployeeControllerFactory from '@main/factories/controllers/employee/UpdateEmployeeControllerFactory';
import deleteEmployeeControllerFactory from '@main/factories/controllers/employee/DeleteEmployeeControllerFactory';

const router = Router();

router
  .route('/employees')
  .post(routeAdapter(createEmployeeControllerFactory.makeCreateEmployeeController()))
  .get(routeAdapter(findEmployeesControllerFactory.makeFindEmployeesController()));

router
  .route('/employees/:employee_alias_id')
  .put(routeAdapter(updateEmployeeControllerFactory.makeUpdateEmployeeController()))
  .delete(routeAdapter(deleteEmployeeControllerFactory.makeDeleteEmployeeController()));

export default { router };
