import { Router } from 'express';

import { routeAdapter } from '@main/adapters/ExpressRouteAdapter';

import createCustomerControllerFactory from '@main/factories/controllers/customer/CreateCustomerControllerFactory';
import findCustomersControllerFactory from '@main/factories/controllers/customer/FindCustomersControllerFactory';
import updateCustomerControllerFactory from '@main/factories/controllers/customer/UpdateCustomerControllerFactory';
import deleteCustomerControllerFactory from '@main/factories/controllers/customer/DeleteCustomerControllerFactory';

const router = Router();

router
  .route('/customers')
  .post(routeAdapter(createCustomerControllerFactory.makeCreateCustomerController()))
  .get(routeAdapter(findCustomersControllerFactory.makeFindCustomersController()));

router
  .route('/customers/:customer_alias_id')
  .put(routeAdapter(updateCustomerControllerFactory.makeUpdateCustomerController()))
  .delete(routeAdapter(deleteCustomerControllerFactory.makeDeleteCustomerController()));

export default { router };
