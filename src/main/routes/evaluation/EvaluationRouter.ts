import { Router } from 'express';

import { routeAdapter } from '@main/adapters/ExpressRouteAdapter';

import checkIfExistsTransactionEvaluation from '@main/middlewares/evaluation/CheckIfExistsTransactionEvaluation';

import createEvaluationControllerFactory from '@main/factories/controllers/evaluation/CreateEvaluationControllerFactory';
import findEvaluationsControllerFactory from '@main/factories/controllers/evaluation/FindEvaluationsControllerFactory';

const router = Router();

router
  .route('/transactions/:transaction_alias_id/evaluations')
  .post(
    checkIfExistsTransactionEvaluation,
    routeAdapter(createEvaluationControllerFactory.makeCreateEvaluationController()),
  )
  .get(routeAdapter(findEvaluationsControllerFactory.makeFindEvaluationsController()));

export default { router };
