import { Express } from 'express';

import storeRouter from '@main/routes/store/StoreRouter';
import employeeRouter from '@main/routes/employee/EmployeeRouter';
import customerRouter from '@main/routes/customer/CustomerRouter';
import transactionRouter from '@main/routes/transaction/TransactionRouter';
import evaluationRouter from '@main/routes/evaluation/EvaluationRouter';

export default (app: Express): void => {
  app.use(`/api/v1/nps`, storeRouter.router);
  app.use(`/api/v1/nps`, employeeRouter.router);
  app.use(`/api/v1/nps`, customerRouter.router);
  app.use(`/api/v1/nps`, transactionRouter.router);
  app.use(`/api/v1/nps`, evaluationRouter.router);
};
