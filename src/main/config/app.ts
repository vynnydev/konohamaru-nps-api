import express from 'express';

import setupErrorHandler from '@main/config/errorHandler';
import setupMiddlewares from '@main/config/middlewares';
import setupRoutes from '@main/config/routes';

const app = express();

setupMiddlewares(app);
setupRoutes(app);
setupErrorHandler(app);

export { app };
