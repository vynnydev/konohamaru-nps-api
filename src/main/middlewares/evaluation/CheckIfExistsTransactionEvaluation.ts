import { middlewareAdapter } from '@main/adapters/ExpressMiddlewareAdapter';
import checkIfExistsTransactionEvaluationMiddlewareFactory from '@main/factories/middlewares/evaluation/CheckIfExistsTransactionEvaluationMiddlewareFactory';

const checkIfExistsTransactionEvaluationMiddleware =
  checkIfExistsTransactionEvaluationMiddlewareFactory.makeCheckIfExistsTransactionEvaluationMiddleware();

const checkIfExistsTransactionEvaluation = middlewareAdapter(
  checkIfExistsTransactionEvaluationMiddleware,
);

export default checkIfExistsTransactionEvaluation;
