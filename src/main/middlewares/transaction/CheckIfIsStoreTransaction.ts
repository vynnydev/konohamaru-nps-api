import { middlewareAdapter } from '@main/adapters/ExpressMiddlewareAdapter';
import checkIfIsStoreTransactionMiddlewareFactory from '@main/factories/middlewares/transaction/CheckIfIsStoreTransactionMiddlewareFactory';

const checkIfIsStoreTransactionMiddleware =
  checkIfIsStoreTransactionMiddlewareFactory.makeCheckIfIsStoreTransactionMiddleware();

const checkIfIsStoreTransaction = middlewareAdapter(checkIfIsStoreTransactionMiddleware);

export default checkIfIsStoreTransaction;
