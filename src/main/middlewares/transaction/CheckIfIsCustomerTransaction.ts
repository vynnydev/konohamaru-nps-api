import { middlewareAdapter } from '@main/adapters/ExpressMiddlewareAdapter';
import checkIfIsCustomerTransactionMiddlewareFactory from '@main/factories/middlewares/transaction/CheckIfIsCustomerTransactionMiddlewareFactory';

const checkIfIsCustomerTransactionMiddleware =
  checkIfIsCustomerTransactionMiddlewareFactory.makeCheckIfIsCustomerTransactionMiddleware();

const checkIfIsCustomerTransaction = middlewareAdapter(checkIfIsCustomerTransactionMiddleware);

export default checkIfIsCustomerTransaction;
