import { middlewareAdapter } from '@main/adapters/ExpressMiddlewareAdapter';
import checkIfIsEmployeeTransactionMiddlewareFactory from '@main/factories/middlewares/transaction/CheckIfIsEmployeeTransactionMiddlewareFactory';

const checkIfIsEmployeeTransactionMiddleware =
  checkIfIsEmployeeTransactionMiddlewareFactory.makeCheckIfIsEmployeeTransactionMiddleware();

const checkIfIsEmployeeTransaction = middlewareAdapter(checkIfIsEmployeeTransactionMiddleware);

export default checkIfIsEmployeeTransaction;
