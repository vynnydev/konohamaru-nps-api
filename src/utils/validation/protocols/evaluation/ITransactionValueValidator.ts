export default interface ITransactionValueValidator {
  isValid(transaction_value: any): boolean;
}
