export default interface IStoreEvaluationValidator {
  isValid(store_evaluation: any): boolean;
}
