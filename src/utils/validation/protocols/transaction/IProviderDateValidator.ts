export default interface IProviderDateValidator {
  isValid(provider_date: any): boolean;
}
