export default interface ICnpjValidator {
  isValid(cnpj: any): boolean;
}
