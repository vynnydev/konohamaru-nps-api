export default interface ILimitValidator {
  isValid(limit: any): boolean;
}
