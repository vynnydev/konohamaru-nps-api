export default interface IDescriptionValidator {
  isValid(description: any): boolean;
}
