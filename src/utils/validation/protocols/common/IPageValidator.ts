export default interface IPageValidator {
  isValid(page: any): boolean;
}
