export default interface IIsActiveValidator {
  isValid(is_active: any): boolean;
}
