import AppError from '@utils/errors/AppError';

import IValidation from '@presentation/protocols/IValidation';

import ITransactionValueValidator from '@utils/validation/protocols/evaluation/ITransactionValueValidator';

export default class TransactionValueValidation implements IValidation {
  constructor(
    private readonly fieldName: string,
    private readonly transactionValidator: ITransactionValueValidator,
  ) {}

  public validate(input: any): void {
    const field = input[this.fieldName];

    const isValid = this.transactionValidator.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid transaction value', status_code: 400 });
  }
}
