import FakeTransactionValueValidatorAdapter from '@infra/adapters/validators/evaluation/fakes/FakeTransactionValueValidatorAdapter';

import TransactionValueValidation from '@utils/validation/validators/evaluation/TransactionValueValidation';

let startDateValidation: TransactionValueValidation;

let fakeTransactionValueValidatorAdapter: FakeTransactionValueValidatorAdapter;

describe('TransactionValueValidation', () => {
  beforeAll(() => {
    fakeTransactionValueValidatorAdapter = new FakeTransactionValueValidatorAdapter();

    startDateValidation = new TransactionValueValidation(
      'transaction_value',
      fakeTransactionValueValidatorAdapter,
    );
  });

  it('should be able to call is valid with correct values', () => {
    const isValidSpy = jest.spyOn(fakeTransactionValueValidatorAdapter, 'isValid');

    startDateValidation.validate({ transaction_value: 'any_transaction_value' });

    expect(isValidSpy).toHaveBeenCalledWith('any_transaction_value');
  });

  it('should be able to throw if is valid throws', () => {
    jest.spyOn(fakeTransactionValueValidatorAdapter, 'isValid').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(() => {
      startDateValidation.validate({ transaction_value: 'any_transaction_value' });
    }).toThrow();
  });

  it('should be able to throw if is valid returns false', () => {
    jest.spyOn(fakeTransactionValueValidatorAdapter, 'isValid').mockImplementationOnce(() => false);

    expect(() => {
      startDateValidation.validate({ transaction_value: 'any_transaction_value' });
    }).toThrow();
  });

  it('should be able to validate', () => {
    const error = startDateValidation.validate({ transaction_value: 'any_transaction_value' });

    expect(error).toBeFalsy();
  });
});
