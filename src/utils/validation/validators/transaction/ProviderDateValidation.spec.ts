import FakeProviderDateValidatorAdapter from '@infra/adapters/validators/transaction/fakes/FakeProviderDateValidatorAdapter';

import ProviderDateValidation from '@utils/validation/validators/transaction/ProviderDateValidation';

let providerDateValidation: ProviderDateValidation;

let fakeProviderDateValidatorAdapter: FakeProviderDateValidatorAdapter;

describe('ProviderDateValidation', () => {
  beforeAll(() => {
    fakeProviderDateValidatorAdapter = new FakeProviderDateValidatorAdapter();

    providerDateValidation = new ProviderDateValidation(
      'provider_date',
      fakeProviderDateValidatorAdapter,
    );
  });

  it('should be able to call is valid with correct values', () => {
    const isValidSpy = jest.spyOn(fakeProviderDateValidatorAdapter, 'isValid');

    providerDateValidation.validate({ provider_date: 'any_provider_date' });

    expect(isValidSpy).toHaveBeenCalledWith('any_provider_date');
  });

  it('should be able to throw if is valid throws', () => {
    jest.spyOn(fakeProviderDateValidatorAdapter, 'isValid').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(() => {
      providerDateValidation.validate({ provider_date: 'any_provider_date' });
    }).toThrow();
  });

  it('should be able to throw if is valid returns false', () => {
    jest.spyOn(fakeProviderDateValidatorAdapter, 'isValid').mockImplementationOnce(() => false);

    expect(() => {
      providerDateValidation.validate({ provider_date: 'any_provider_date' });
    }).toThrow();
  });

  it('should be able to validate', () => {
    const error = providerDateValidation.validate({ provider_date: 'any_provider_date' });

    expect(error).toBeFalsy();
  });
});
