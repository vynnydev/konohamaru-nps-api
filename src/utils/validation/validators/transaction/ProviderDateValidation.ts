import AppError from '@utils/errors/AppError';

import IValidation from '@presentation/protocols/IValidation';

import IProviderDateValidator from '@utils/validation/protocols/transaction/IProviderDateValidator';

export default class ProviderDateValidation implements IValidation {
  constructor(
    private readonly fieldName: string,
    private readonly providerDateValidator: IProviderDateValidator,
  ) {}

  public validate(input: any): void {
    const field = input[this.fieldName];

    const isValid = this.providerDateValidator.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid provider date', status_code: 400 });
  }
}
