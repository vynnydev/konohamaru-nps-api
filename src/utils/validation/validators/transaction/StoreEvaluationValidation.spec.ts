import FakeStoreEvaluationValidatorAdapter from '@infra/adapters/validators/transaction/fakes/FakeStoreEvaluationValidatorAdapter';

import StoreEvaluationValidation from '@utils/validation/validators/transaction/StoreEvaluationValidation';

let providerDateValidation: StoreEvaluationValidation;

let fakeStoreEvaluationValidatorAdapter: FakeStoreEvaluationValidatorAdapter;

describe('StoreEvaluationValidation', () => {
  beforeAll(() => {
    fakeStoreEvaluationValidatorAdapter = new FakeStoreEvaluationValidatorAdapter();

    providerDateValidation = new StoreEvaluationValidation(
      'store_evaluation',
      fakeStoreEvaluationValidatorAdapter,
    );
  });

  it('should be able to call is valid with correct values', () => {
    const isValidSpy = jest.spyOn(fakeStoreEvaluationValidatorAdapter, 'isValid');

    providerDateValidation.validate({ store_evaluation: 'any_store_evaluation' });

    expect(isValidSpy).toHaveBeenCalledWith('any_store_evaluation');
  });

  it('should be able to throw if is valid throws', () => {
    jest.spyOn(fakeStoreEvaluationValidatorAdapter, 'isValid').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(() => {
      providerDateValidation.validate({ store_evaluation: 'any_store_evaluation' });
    }).toThrow();
  });

  it('should be able to throw if is valid returns false', () => {
    jest.spyOn(fakeStoreEvaluationValidatorAdapter, 'isValid').mockImplementationOnce(() => false);

    expect(() => {
      providerDateValidation.validate({ store_evaluation: 'any_store_evaluation' });
    }).toThrow();
  });

  it('should be able to validate', () => {
    const error = providerDateValidation.validate({ store_evaluation: 'any_store_evaluation' });

    expect(error).toBeFalsy();
  });
});
