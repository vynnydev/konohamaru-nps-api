import AppError from '@utils/errors/AppError';

import IValidation from '@presentation/protocols/IValidation';

import IStoreEvaluationValidator from '@utils/validation/protocols/transaction/IStoreEvaluationValidator';

export default class StoreEvaluationValidation implements IValidation {
  constructor(
    private readonly fieldName: string,
    private readonly storeEvaluationValidator: IStoreEvaluationValidator,
  ) {}

  public validate(input: any): void {
    const field = input[this.fieldName];

    const isValid = this.storeEvaluationValidator.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid Store Evaluation', status_code: 400 });
  }
}
