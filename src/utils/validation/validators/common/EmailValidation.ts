import AppError from '@utils/errors/AppError';

import IEmailValidator from '@utils/validation/protocols/common/IEmailValidator';

import IValidation from '@presentation/protocols/IValidation';

export default class EmailValidation implements IValidation {
  constructor(
    private readonly fieldName: string,
    private readonly emailValidator: IEmailValidator,
  ) {}

  public validate(input: any): void {
    const field = input[this.fieldName];

    const isValid = this.emailValidator.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid email', status_code: 400 });
  }
}
