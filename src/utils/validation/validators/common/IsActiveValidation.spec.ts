import FakeIsActiveValidatorAdapter from '@infra/adapters/validators/common/fakes/FakeIsActiveValidatorAdapter';

import IsActiveValidation from '@utils/validation/validators/common/IsActiveValidation';

let isActiveValidation: IsActiveValidation;

let fakeIsActiveValidatorAdapter: FakeIsActiveValidatorAdapter;

describe('IsActiveValidation', () => {
  beforeAll(() => {
    fakeIsActiveValidatorAdapter = new FakeIsActiveValidatorAdapter();

    isActiveValidation = new IsActiveValidation('is_active', fakeIsActiveValidatorAdapter);
  });

  it('should be able to call is valid with correct values', () => {
    const isValidSpy = jest.spyOn(fakeIsActiveValidatorAdapter, 'isValid');

    isActiveValidation.validate({ is_active: 'any_is_active' });

    expect(isValidSpy).toHaveBeenCalledWith('any_is_active');
  });

  it('should be able to throw if is valid throws', () => {
    jest.spyOn(fakeIsActiveValidatorAdapter, 'isValid').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(() => {
      isActiveValidation.validate({ is_active: 'any_is_active' });
    }).toThrow();
  });

  it('should be able to throw if is valid returns false', () => {
    jest.spyOn(fakeIsActiveValidatorAdapter, 'isValid').mockImplementationOnce(() => false);

    expect(() => {
      isActiveValidation.validate({ is_active: 'any_is_active' });
    }).toThrow();
  });

  it('should be able to validate', () => {
    const error = isActiveValidation.validate({ is_active: 'any_is_active' });

    expect(error).toBeFalsy();
  });
});
