import AppError from '@utils/errors/AppError';

import IValidation from '@presentation/protocols/IValidation';

import ILimitValidator from '@utils/validation/protocols/common/ILimitValidator';

export default class LimitValidation implements IValidation {
  constructor(
    private readonly fieldName: string,
    private readonly limitValidation: ILimitValidator,
  ) {}

  public validate(input: any): void {
    const field = input[this.fieldName];

    const isValid = this.limitValidation.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid limit', status_code: 400 });
  }
}
