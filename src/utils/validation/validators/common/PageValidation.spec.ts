import FakePageValidatorAdapter from '@infra/adapters/validators/common/fakes/FakePageValidatorAdapter';

import PageValidation from '@utils/validation/validators/common/PageValidation';

let pageValidation: PageValidation;

let fakePageValidatorAdapter: FakePageValidatorAdapter;

describe('PageValidation', () => {
  beforeAll(() => {
    fakePageValidatorAdapter = new FakePageValidatorAdapter();

    pageValidation = new PageValidation('page', fakePageValidatorAdapter);
  });

  it('should be able to call is valid with correct values', () => {
    const isValidSpy = jest.spyOn(fakePageValidatorAdapter, 'isValid');

    pageValidation.validate({ page: 'any_page' });

    expect(isValidSpy).toHaveBeenCalledWith('any_page');
  });

  it('should be able to throw if is valid throws', () => {
    jest.spyOn(fakePageValidatorAdapter, 'isValid').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(() => {
      pageValidation.validate({ page: 'any_page' });
    }).toThrow();
  });

  it('should be able to throw if is valid returns false', () => {
    jest.spyOn(fakePageValidatorAdapter, 'isValid').mockImplementationOnce(() => false);

    expect(() => {
      pageValidation.validate({ page: 'any_page' });
    }).toThrow();
  });

  it('should be able to validate', () => {
    const error = pageValidation.validate({ page: 'any_page' });

    expect(error).toBeFalsy();
  });
});
