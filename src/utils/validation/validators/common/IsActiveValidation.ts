import AppError from '@utils/errors/AppError';

import IValidation from '@presentation/protocols/IValidation';

import IIsActiveValidator from '@utils/validation/protocols/common/IIsActiveValidator';

export default class IsActiveValidation implements IValidation {
  constructor(
    private readonly fieldName: string,
    private readonly isActiveValidator: IIsActiveValidator,
  ) {}

  public validate(input: any): void {
    const field = input[this.fieldName];

    const isValid = this.isActiveValidator.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid is active', status_code: 400 });
  }
}
