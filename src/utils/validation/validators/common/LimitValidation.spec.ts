import FakeLimitValidatorAdapter from '@infra/adapters/validators/common/fakes/FakeLimitValidatorAdapter';

import LimitValidation from '@utils/validation/validators/common/LimitValidation';

let limitValidation: LimitValidation;

let fakeLimitValidatorAdapter: FakeLimitValidatorAdapter;

describe('NameValidation', () => {
  beforeAll(() => {
    fakeLimitValidatorAdapter = new FakeLimitValidatorAdapter();

    limitValidation = new LimitValidation('limit', fakeLimitValidatorAdapter);
  });

  it('should be able to call is valid with correct values', () => {
    const isValidSpy = jest.spyOn(fakeLimitValidatorAdapter, 'isValid');

    limitValidation.validate({ limit: 'any_limit' });

    expect(isValidSpy).toHaveBeenCalledWith('any_limit');
  });

  it('should be able to throw if is valid throws', () => {
    jest.spyOn(fakeLimitValidatorAdapter, 'isValid').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(() => {
      limitValidation.validate({ limit: 'any_limit' });
    }).toThrow();
  });

  it('should be able to throw if is valid returns false', () => {
    jest.spyOn(fakeLimitValidatorAdapter, 'isValid').mockImplementationOnce(() => false);

    expect(() => {
      limitValidation.validate({ limit: 'any_limit' });
    }).toThrow();
  });

  it('should be able to validate', () => {
    const error = limitValidation.validate({ limit: 'any_limit' });

    expect(error).toBeFalsy();
  });
});
