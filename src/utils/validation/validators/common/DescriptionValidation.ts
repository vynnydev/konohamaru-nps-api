import AppError from '@utils/errors/AppError';

import IValidation from '@presentation/protocols/IValidation';

import IDescriptionValidator from '@utils/validation/protocols/common/IDescriptionValidator';

export default class DescriptionValidation implements IValidation {
  constructor(
    private readonly fieldDescription: string,
    private readonly descriptionValidator: IDescriptionValidator,
  ) {}

  public validate(input: any): void {
    const field = input[this.fieldDescription];

    const isValid = this.descriptionValidator.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid description', status_code: 400 });
  }
}
