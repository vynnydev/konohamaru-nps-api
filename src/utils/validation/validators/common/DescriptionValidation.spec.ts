import FakeDescriptionValidatorAdapter from '@infra/adapters/validators/common/fakes/FakeDescriptionValidatorAdapter';

import DescriptionValidation from '@utils/validation/validators/common/DescriptionValidation';

let descriptionValidation: DescriptionValidation;

let fakeDescriptionValidatorAdapter: FakeDescriptionValidatorAdapter;

describe('DescriptionValidation', () => {
  beforeAll(() => {
    fakeDescriptionValidatorAdapter = new FakeDescriptionValidatorAdapter();

    descriptionValidation = new DescriptionValidation(
      'description',
      fakeDescriptionValidatorAdapter,
    );
  });

  it('should be able to call is valid with correct values', () => {
    const isValidSpy = jest.spyOn(fakeDescriptionValidatorAdapter, 'isValid');

    descriptionValidation.validate({ description: 'any_description' });

    expect(isValidSpy).toHaveBeenCalledWith('any_description');
  });

  it('should be able to throw if is valid throws', () => {
    jest.spyOn(fakeDescriptionValidatorAdapter, 'isValid').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(() => {
      descriptionValidation.validate({ description: 'any_description' });
    }).toThrow();
  });

  it('should be able to throw if is valid returns false', () => {
    jest.spyOn(fakeDescriptionValidatorAdapter, 'isValid').mockImplementationOnce(() => false);

    expect(() => {
      descriptionValidation.validate({ description: 'any_description' });
    }).toThrow();
  });

  it('should be able to validate', () => {
    const error = descriptionValidation.validate({ description: 'any_description' });

    expect(error).toBeFalsy();
  });
});
