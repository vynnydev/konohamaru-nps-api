import AppError from '@utils/errors/AppError';

import IValidation from '@presentation/protocols/IValidation';

import IPageValidator from '@utils/validation/protocols/common/IPageValidator';

export default class PageValidation implements IValidation {
  constructor(private readonly fieldName: string, private readonly pageValidator: IPageValidator) {}

  public validate(input: any): void {
    const field = input[this.fieldName];

    const isValid = this.pageValidator.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid page', status_code: 400 });
  }
}
