import AppError from '@utils/errors/AppError';

import IPhoneNumberValidator from '@utils/validation/protocols/common/IPhoneNumberValidator';

import IValidation from '@presentation/protocols/IValidation';

export default class PhoneNumberValidation implements IValidation {
  constructor(
    private readonly fieldName: string,
    private readonly phoneNumberValidator: IPhoneNumberValidator,
  ) {}

  public validate(input: any): void {
    const field = input[this.fieldName];

    const isValid = this.phoneNumberValidator.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid phone number', status_code: 400 });
  }
}
