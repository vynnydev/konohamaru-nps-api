import AppError from '@utils/errors/AppError';

import ICpfValidator from '@utils/validation/protocols/common/ICpfValidator';

import IValidation from '@presentation/protocols/IValidation';

export default class CpfValidation implements IValidation {
  constructor(private readonly fieldName: string, private readonly cpfValidator: ICpfValidator) {}

  public validate(input: any): void {
    const field = input[this.fieldName];

    const isValid = this.cpfValidator.isValid(field);

    if (!isValid) throw new AppError({ message: 'Invalid cpf', status_code: 400 });
  }
}
