import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

export default class Transaction {
  id: string;

  alias_id: string;

  provider_date: Date;

  store_evaluation: EStoreEvaluation;

  customer_id: string;

  employee_id: string;

  store_id: string;

  created_at: Date;

  updated_at: Date;
}
