export default class Customer {
  id: string;

  alias_id: string;

  name: string;

  cpf: string;

  email: string;

  phone_number: string;

  created_at: Date;

  updated_at: Date;
}
