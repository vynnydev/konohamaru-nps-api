export default class Employee {
  id: string;

  alias_id: string;

  name: string;

  is_active: boolean;

  created_at: Date;

  updated_at: Date;
}
