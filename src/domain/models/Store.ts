export default class Store {
  id: string;

  alias_id: string;

  name: string;

  created_at: Date;

  updated_at: Date;
}
