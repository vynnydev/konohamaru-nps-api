import Evaluation from '@domain/models/Evaluation';

import ICreateEvaluationDTO from '@domain/useCases/evaluation/dtos/ICreateEvaluationDTO';

export default interface ICreateEvaluation {
  create(data: ICreateEvaluationDTO): Promise<Evaluation>;
}
