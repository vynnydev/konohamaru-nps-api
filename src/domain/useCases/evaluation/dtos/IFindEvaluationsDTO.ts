export default interface IFindEvaluationsDTO {
  page: number;
  limit: number;
  transaction_alias_id: string;
}
