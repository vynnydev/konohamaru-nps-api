import Evaluation from '@domain/models/Evaluation';

export default interface IFindEvaluationsResponse {
  evaluations: Evaluation[];
  total_pages: number;
}
