export default interface ICreateEvaluationDTO {
  description: string;
  transaction_value: number;
  transaction_alias_id: string;
}
