import IFindEvaluationsDTO from '@domain/useCases/evaluation/dtos/IFindEvaluationsDTO';
import IFindEvaluationsResponse from '@domain/useCases/evaluation/dtos/IFindEvaluationsResponse';

export default interface IFindEvaluations {
  find(data: IFindEvaluationsDTO): Promise<IFindEvaluationsResponse>;
}
