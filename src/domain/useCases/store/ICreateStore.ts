import Store from '@domain/models/Store';

export default interface ICreateStore {
  create(name: string): Promise<Store>;
}
