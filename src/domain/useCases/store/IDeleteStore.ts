export default interface IDeleteStore {
  delete(store_alias_id: string): Promise<void>;
}
