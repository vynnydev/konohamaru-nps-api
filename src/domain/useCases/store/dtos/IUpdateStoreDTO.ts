interface IUpdateStore {
  name: string;
}

export default interface IUpdateStoreDTO {
  store_alias_id: string;
  data: IUpdateStore;
}
