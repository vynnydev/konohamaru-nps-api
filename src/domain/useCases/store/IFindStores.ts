import Store from '@domain/models/Store';

export default interface IFindStores {
  find(): Promise<Store[]>;
}
