import Store from '@domain/models/Store';

import IUpdateStoreDTO from '@domain/useCases/store/dtos/IUpdateStoreDTO';

export default interface IUpdateStore {
  update(data: IUpdateStoreDTO): Promise<Store>;
}
