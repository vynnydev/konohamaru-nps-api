import Transaction from '@domain/models/Transaction';

import ICreateTransactionDTO from '@domain/useCases/transaction/dtos/ICreateTransactionDTO';

export default interface ICreateTransaction {
  create(data: ICreateTransactionDTO): Promise<Transaction>;
}
