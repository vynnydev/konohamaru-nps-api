import Transaction from '@domain/models/Transaction';

import IUpdateTransactionDTO from '@domain/useCases/transaction/dtos/IUpdateTransactionDTO';

export default interface IUpdateTransaction {
  update(data: IUpdateTransactionDTO): Promise<Transaction>;
}
