import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

interface IUpdateTransaction {
  store_evaluation: EStoreEvaluation;
  provider_date: Date;
}

export default interface IUpdateTransactionDTO {
  transaction_alias_id: string;
  data: IUpdateTransaction;
}
