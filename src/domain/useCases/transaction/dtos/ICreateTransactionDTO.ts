import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

export default interface ICreateTransactionDTO {
  store_alias_id: string;
  customer_alias_id: string;
  employee_alias_id: string;
  store_evaluation: EStoreEvaluation;
  provider_date: Date;
}
