import Employee from '@domain/models/Employee';

import IUpdateEmployeeDTO from '@domain/useCases/employee/dtos/IUpdateEmployeeDTO';

export default interface IUpdateEmployee {
  update(data: IUpdateEmployeeDTO): Promise<Employee>;
}
