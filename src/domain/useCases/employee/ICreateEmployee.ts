import Employee from '@domain/models/Employee';

export default interface ICreateEmployee {
  create(name: string): Promise<Employee>;
}
