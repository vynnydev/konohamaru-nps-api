export default interface IDeleteEmployee {
  delete(employee_alias_id: string): Promise<void>;
}
