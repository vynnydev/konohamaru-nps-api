interface IUpdateEmployee {
  name: string;
}

export default interface IUpdateEmployeeDTO {
  employee_alias_id: string;
  data: IUpdateEmployee;
}
