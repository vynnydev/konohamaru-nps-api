import Employee from '@domain/models/Employee';

export default interface IFindEmployees {
  find(): Promise<Employee[]>;
}
