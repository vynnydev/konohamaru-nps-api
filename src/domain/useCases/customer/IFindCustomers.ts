import Customer from '@domain/models/Customer';

export default interface IFindCustomers {
  find(): Promise<Customer[]>;
}
