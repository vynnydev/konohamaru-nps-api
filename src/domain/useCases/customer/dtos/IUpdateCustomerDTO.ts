interface IUpdateCustomer {
  cpf: string;
  email: string;
  name: string;
  phone_number: string;
}

export default interface IUpdateCustomerDTO {
  customer_alias_id: string;
  data: IUpdateCustomer;
}
