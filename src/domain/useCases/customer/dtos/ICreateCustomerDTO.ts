export default interface ICreateCustomerDTO {
  cpf: string;
  email: string;
  name: string;
  phone_number: string;
}
