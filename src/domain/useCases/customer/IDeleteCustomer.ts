export default interface IDeleteCustomer {
  delete(customer_alias_id: string): Promise<void>;
}
