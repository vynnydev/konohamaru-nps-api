import Customer from '@domain/models/Customer';

import IUpdateCustomerDTO from '@domain/useCases/customer/dtos/IUpdateCustomerDTO';

export default interface IUpdateCustomer {
  update(data: IUpdateCustomerDTO): Promise<Customer>;
}
