import Customer from '@domain/models/Customer';

import ICreateCustomerDTO from '@domain/useCases/customer/dtos/ICreateCustomerDTO';

export default interface ICreateCustomer {
  create(data: ICreateCustomerDTO): Promise<Customer>;
}
