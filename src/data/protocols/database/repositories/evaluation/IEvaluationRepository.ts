import Evaluation from '@domain/models/Evaluation';

import ICreateEvaluationDTO from '@data/protocols/database/repositories/evaluation/dtos/ICreateEvaluationDTO';
import IFindByTransactionDTO from '@data/protocols/database/repositories/evaluation/dtos/IFindByTransactionDTO';

export default interface IEvaluationRepository {
  create(data: ICreateEvaluationDTO): Promise<Evaluation>;
  findByAliasId(alias_id: string): Promise<Evaluation | undefined>;
  findByTransactionId(transaction_id: string): Promise<Evaluation | undefined>;
  findByTransaction(data: IFindByTransactionDTO): Promise<Evaluation[]>;
  countByTransactionId(transaction_id: string): Promise<number>;
}
