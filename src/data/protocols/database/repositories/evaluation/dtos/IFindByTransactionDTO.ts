export default interface IFindByTransactionDTO {
  transaction_id: string;
  limit: number;
  offset: number;
}
