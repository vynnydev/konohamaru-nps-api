export default interface ICreateEvaluationDTO {
  alias_id: string;
  description: string;
  transaction_value: number;
  transaction_id: string;
}
