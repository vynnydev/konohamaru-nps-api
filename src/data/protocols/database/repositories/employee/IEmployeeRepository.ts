import Employee from '@domain/models/Employee';

import ICreateEmployeeDTO from '@data/protocols/database/repositories/employee/dtos/ICreateEmployeeDTO';
import IUpdateEmployeeDTO from '@data/protocols/database/repositories/employee/dtos/IUpdateEmployeeDTO';

export default interface IEmployeeRepository {
  create(data: ICreateEmployeeDTO): Promise<Employee>;
  findByAliasId(alias_id: string): Promise<Employee | undefined>;
  findEmployees(): Promise<Employee[]>;
  update(data: IUpdateEmployeeDTO): Promise<Employee | undefined>;
}
