export default interface ICreateEmployeeDTO {
  alias_id: string;
  name: string;
  is_active: boolean;
}
