interface IUpdateEmployee {
  name?: string;
  is_active?: boolean;
}

export default interface IUpdateEmployeeDTO {
  id: string;
  data: IUpdateEmployee;
}
