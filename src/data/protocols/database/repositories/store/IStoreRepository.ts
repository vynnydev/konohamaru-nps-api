import Store from '@domain/models/Store';

import ICreateStoreDTO from '@data/protocols/database/repositories/store/dtos/ICreateStoreDTO';
import IUpdateStoreDTO from '@data/protocols/database/repositories/store/dtos/IUpdateStoreDTO';

export default interface IStoreRepository {
  create(data: ICreateStoreDTO): Promise<Store>;
  findByAliasId(alias_id: string): Promise<Store | undefined>;
  findByName(name: string): Promise<Store | undefined>;
  findById(id: string): Promise<Store | undefined>;
  findAll(): Promise<Store[]>;
  update(data: IUpdateStoreDTO): Promise<Store | undefined>;
  deleteById(id: string): Promise<void>;
}
