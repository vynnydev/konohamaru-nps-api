interface IUpdateStore {
  name?: string;
  is_active?: boolean;
}

export default interface IUpdateStoreDTO {
  id: string;
  data: IUpdateStore;
}
