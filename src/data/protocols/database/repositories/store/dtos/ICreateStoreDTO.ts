export default interface ICreateStoreDTO {
  alias_id: string;
  name: string;
}
