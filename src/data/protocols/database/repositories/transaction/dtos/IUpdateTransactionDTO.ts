import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

interface IUpdateTransaction {
  provider_date?: Date;
  store_evaluation?: EStoreEvaluation;
}

export default interface IUpdateTransactionDTO {
  id: string;
  data: IUpdateTransaction;
}
