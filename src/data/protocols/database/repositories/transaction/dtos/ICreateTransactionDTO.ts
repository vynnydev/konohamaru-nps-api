import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

export default interface ICreateTransactionDTO {
  alias_id: string;
  provider_date: Date;
  store_evaluation: EStoreEvaluation;
  store_id: string;
  customer_id: string;
  employee_id: string;
}
