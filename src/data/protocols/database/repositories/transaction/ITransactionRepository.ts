import Transaction from '@domain/models/Transaction';

import ICreateTransactionDTO from '@data/protocols/database/repositories/transaction/dtos/ICreateTransactionDTO';
import IUpdateTransactionDTO from '@data/protocols/database/repositories/transaction/dtos/IUpdateTransactionDTO';

export default interface ITransactionsRepository {
  create(data: ICreateTransactionDTO): Promise<Transaction>;
  findByAliasId(alias_id: string): Promise<Transaction | undefined>;
  update(data: IUpdateTransactionDTO): Promise<Transaction | undefined>;
}
