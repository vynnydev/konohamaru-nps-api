import Customer from '@domain/models/Customer';

import ICreateCustomerDTO from '@data/protocols/database/repositories/customer/dtos/ICreateCustomerDTO';
import IUpdateCustomerDTO from '@data/protocols/database/repositories/customer/dtos/IUpdateCustomerDTO';

export default interface ICustomerRepository {
  create(data: ICreateCustomerDTO): Promise<Customer>;
  findByAliasId(alias_id: string): Promise<Customer | undefined>;
  findCustomers(): Promise<Customer[]>;
  update(data: IUpdateCustomerDTO): Promise<Customer | undefined>;
  deleteById(id: string): Promise<void>;
}
