export default interface ICreateCustomerDTO {
  alias_id: string;
  name: string;
  email: string;
  phone_number: string;
  cpf: string;
}
