interface IUpdateCustomer {
  name?: string;
  email?: string;
  phone_number?: string;
  cpf?: string;
}

export default interface IUpdateCustomerDTO {
  id: string;
  data: IUpdateCustomer;
}
