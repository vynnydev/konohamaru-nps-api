import AppError from '@utils/errors/AppError';

import Customer from '@domain/models/Customer';
import ICustomerRepository from '@data/protocols/database/repositories/customer/ICustomerRepository';

import IUpdateCustomer from '@domain/useCases/customer/IUpdateCustomer';
import IUpdateCustomerDTO from '@domain/useCases/customer/dtos/IUpdateCustomerDTO';

export default class UpdateCustomer implements IUpdateCustomer {
  constructor(private readonly customerRepository: ICustomerRepository) {}

  public async update({ customer_alias_id, data }: IUpdateCustomerDTO): Promise<Customer> {
    const foundCustomer = await this.customerRepository.findByAliasId(customer_alias_id);

    if (!foundCustomer)
      throw new AppError({ message: 'customer does not exists', status_code: 400 });

    const { cpf, email, name, phone_number } = data;

    const updatedCustomer = await this.customerRepository.update({
      id: foundCustomer.id,
      data: { cpf, email, name, phone_number },
    });

    if (!updatedCustomer)
      throw new AppError({ message: 'Could not update customer', status_code: 400 });

    return updatedCustomer;
  }
}
