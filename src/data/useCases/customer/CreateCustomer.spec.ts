import FakeAliasGenerator from '@utils/aliasGenerator/fakes/FakeAliasGenerator';

import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import FakeCustomerRepository from '@infra/database/repositories/customer/fakes/FakeCustomerRepository';

import CreateCustomer from '@data/useCases/customer/CreateCustomer';

let fakeAliasGenerator: FakeAliasGenerator;

let fakeCustomerRepository: FakeCustomerRepository;

let createCustomer: CreateCustomer;

describe('CreateCustomer', () => {
  beforeEach(() => {
    fakeAliasGenerator = new FakeAliasGenerator();

    fakeCustomerRepository = new FakeCustomerRepository();

    createCustomer = new CreateCustomer(fakeCustomerRepository, fakeAliasGenerator);
  });

  it('should be able to call generate alias id with correct values', async () => {
    const generatedAliasId = jest.spyOn(fakeAliasGenerator, 'generate');

    await createCustomer.create({
      cpf: 'any_cpf',
      email: 'any_email',
      name: 'any_name',
      phone_number: 'any_phone_number',
    });

    expect(generatedAliasId).toHaveBeenCalledWith('customer');
  });

  it('should be able to throw if generate alias throws', async () => {
    jest.spyOn(fakeAliasGenerator, 'generate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createCustomer.create({
        cpf: 'any_cpf',
        email: 'any_email',
        name: 'any_name',
        phone_number: 'any_phone_number',
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call create customer with correct values', async () => {
    const createSpy = jest.spyOn(fakeCustomerRepository, 'create');

    await createCustomer.create({
      cpf: 'any_cpf',
      email: 'any_email',
      name: 'any_name',
      phone_number: 'any_phone_number',
    });

    expect(createSpy).toHaveBeenCalledWith({
      alias_id: 'any_alias_id',
      cpf: 'any_cpf',
      email: 'any_email',
      name: 'any_name',
      phone_number: 'any_phone_number',
    });
  });

  it('should be able to throw if create customer throws', async () => {
    await fakeCustomerRepository.create(mockCustomerModel.mock());

    jest.spyOn(fakeCustomerRepository, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createCustomer.create({
        cpf: 'any_cpf',
        email: 'any_email',
        name: 'any_name',
        phone_number: 'any_phone_number',
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to create customer', async () => {
    const createdCustomer = await createCustomer.create({
      cpf: 'any_cpf',
      email: 'any_email',
      name: 'any_name',
      phone_number: 'any_phone_number',
    });

    expect(createdCustomer).toEqual({
      id: createdCustomer.id,
      alias_id: createdCustomer.alias_id,
      cpf: createdCustomer.cpf,
      email: createdCustomer.email,
      name: createdCustomer.name,
      phone_number: createdCustomer.phone_number,
      created_at: createdCustomer.created_at,
      updated_at: createdCustomer.updated_at,
    });
  });
});
