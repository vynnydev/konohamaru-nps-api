import IAliasGenerator from '@data/protocols/utils/aliasGenerator/IAliasGenerator';

import Customer from '@domain/models/Customer';
import ICustomerRepository from '@data/protocols/database/repositories/customer/ICustomerRepository';

import ICreateCustomerDTO from '@domain/useCases/customer/dtos/ICreateCustomerDTO';
import ICreateCustomer from '@domain/useCases/customer/ICreateCustomer';

export default class CreateCustomer implements ICreateCustomer {
  constructor(
    private readonly customerRepository: ICustomerRepository,
    private readonly aliasGenerator: IAliasGenerator,
  ) {}

  public async create({ cpf, email, name, phone_number }: ICreateCustomerDTO): Promise<Customer> {
    const generatedAlias = this.aliasGenerator.generate('customer');

    const createdCustomer = await this.customerRepository.create({
      alias_id: generatedAlias,
      cpf,
      email,
      name,
      phone_number,
    });

    return createdCustomer;
  }
}
