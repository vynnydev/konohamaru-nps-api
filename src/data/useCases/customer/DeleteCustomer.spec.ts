import AppError from '@utils/errors/AppError';

import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import FakeCustomerRepository from '@infra/database/repositories/customer/fakes/FakeCustomerRepository';

import DeleteCustomer from '@data/useCases/customer/DeleteCustomer';

let fakeCustomerRepository: FakeCustomerRepository;

let deleteCustomer: DeleteCustomer;

describe('DeleteCustomer', () => {
  beforeEach(() => {
    fakeCustomerRepository = new FakeCustomerRepository();

    deleteCustomer = new DeleteCustomer(fakeCustomerRepository);
  });

  it('should be able to call delete customer with correct values', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const updateSpy = jest.spyOn(fakeCustomerRepository, 'findByAliasId');

    await deleteCustomer.delete(createdCustomer.alias_id);

    expect(updateSpy).toHaveBeenCalledWith(createdCustomer.alias_id);
  });

  it('should be able to throw if delete customer throws', async () => {
    await fakeCustomerRepository.create(mockCustomerModel.mock());

    jest.spyOn(fakeCustomerRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(deleteCustomer.delete('any_alias_id')).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if customer does not exists', async () => {
    await expect(deleteCustomer.delete('any_alias_id')).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to delete customer', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const error = await deleteCustomer.delete(createdCustomer.alias_id);

    expect(error).toBeFalsy();
  });
});
