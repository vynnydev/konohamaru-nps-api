import AppError from '@utils/errors/AppError';

import ICustomerRepository from '@data/protocols/database/repositories/customer/ICustomerRepository';

import IDeleteCustomer from '@domain/useCases/customer/IDeleteCustomer';

export default class DeleteCustomer implements IDeleteCustomer {
  constructor(private readonly customerRepository: ICustomerRepository) {}

  public async delete(customer_alias_id: string): Promise<void> {
    const foundCustomer = await this.customerRepository.findByAliasId(customer_alias_id);

    if (!foundCustomer)
      throw new AppError({ message: 'Customer credential does not exists', status_code: 400 });

    await this.customerRepository.deleteById(foundCustomer.id);
  }
}
