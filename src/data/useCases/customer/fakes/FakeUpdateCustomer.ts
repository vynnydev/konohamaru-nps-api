import Customer from '@domain/models/Customer';

import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import IUpdateCustomer from '@domain/useCases/customer/IUpdateCustomer';
import IUpdateCustomerDTO from '@domain/useCases/customer/dtos/IUpdateCustomerDTO';

export default class FakeUpdateCustomer implements IUpdateCustomer {
  public async update(data: IUpdateCustomerDTO): Promise<Customer> {
    const customer = mockCustomerModel.mock();

    return customer;
  }
}
