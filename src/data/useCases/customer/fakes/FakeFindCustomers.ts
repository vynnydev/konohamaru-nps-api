import Customer from '@domain/models/Customer';
import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import IFindCustomers from '@domain/useCases/customer/IFindCustomers';

export default class FakeFindCustomers implements IFindCustomers {
  public async find(): Promise<Customer[]> {
    return [mockCustomerModel.mock()];
  }
}
