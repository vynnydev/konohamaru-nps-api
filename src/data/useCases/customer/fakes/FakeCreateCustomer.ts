import Customer from '@domain/models/Customer';

import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import ICreateCustomerDTO from '@domain/useCases/customer/dtos/ICreateCustomerDTO';
import ICreateCustomer from '@domain/useCases/customer/ICreateCustomer';

export default class FakeCreateCustomer implements ICreateCustomer {
  public async create(data: ICreateCustomerDTO): Promise<Customer> {
    const customer = mockCustomerModel.mock();

    return customer;
  }
}
