import IDeleteCustomer from '@domain/useCases/customer/IDeleteCustomer';

export default class FakeDeleteCustomer implements IDeleteCustomer {
  private readonly customer: Array<string> = [];

  public async delete(customer_alias_id: string): Promise<void> {
    this.customer.push('deleted');
  }
}
