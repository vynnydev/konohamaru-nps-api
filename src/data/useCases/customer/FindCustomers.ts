import Customer from '@domain/models/Customer';

import ICustomerRepository from '@data/protocols/database/repositories/customer/ICustomerRepository';
import IFindCustomers from '@domain/useCases/customer/IFindCustomers';

export default class FindCustomers implements IFindCustomers {
  constructor(private readonly storeRepository: ICustomerRepository) {}

  public async find(): Promise<Customer[]> {
    const foundCustomers = await this.storeRepository.findCustomers();

    return foundCustomers;
  }
}
