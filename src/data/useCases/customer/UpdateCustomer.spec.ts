import AppError from '@utils/errors/AppError';

import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import FakeCustomerRepository from '@infra/database/repositories/customer/fakes/FakeCustomerRepository';

import UpdateCustomer from '@data/useCases/customer/UpdateCustomer';

let fakeCustomerRepository: FakeCustomerRepository;

let updateCustomer: UpdateCustomer;

describe('UpdateCustomer', () => {
  beforeEach(() => {
    fakeCustomerRepository = new FakeCustomerRepository();

    updateCustomer = new UpdateCustomer(fakeCustomerRepository);
  });

  it('should be able to call find customer by alias id with correct values', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const findByAliasId = jest.spyOn(fakeCustomerRepository, 'findByAliasId');

    await updateCustomer.update({
      customer_alias_id: createdCustomer.alias_id,
      data: {
        cpf: 'any_cpf',
        email: 'any_email',
        name: 'any_name',
        phone_number: 'any_phone_number',
      },
    });

    expect(findByAliasId).toHaveBeenCalledWith(createdCustomer.alias_id);
  });

  it('should be able to throw if find customer by alias id throws', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    jest.spyOn(fakeCustomerRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(
      updateCustomer.update({
        customer_alias_id: createdCustomer.alias_id,
        data: {
          cpf: 'any_cpf',
          email: 'any_email',
          name: 'any_name',
          phone_number: 'any_phone_number',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if customer does not exists', async () => {
    await expect(
      updateCustomer.update({
        customer_alias_id: 'any_alias_id',
        data: {
          cpf: 'any_cpf',
          email: 'any_email',
          name: 'any_name',
          phone_number: 'any_phone_number',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call update customer with correct values', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const updateSpy = jest.spyOn(fakeCustomerRepository, 'findByAliasId');

    await updateCustomer.update({
      customer_alias_id: createdCustomer.alias_id,
      data: {
        cpf: 'any_cpf',
        email: 'any_email',
        name: 'any_name',
        phone_number: 'any_phone_number',
      },
    });

    expect(updateSpy).toHaveBeenCalledWith(createdCustomer.alias_id);
  });

  it('should be able to throw if update customer throws', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    jest.spyOn(fakeCustomerRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(
      updateCustomer.update({
        customer_alias_id: createdCustomer.alias_id,
        data: {
          cpf: 'any_cpf',
          email: 'any_email',
          name: 'any_name',
          phone_number: 'any_phone_number',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if update return undefined', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    jest.spyOn(fakeCustomerRepository, 'update').mockImplementationOnce(async () => undefined);

    await expect(
      updateCustomer.update({
        customer_alias_id: createdCustomer.alias_id,
        data: {
          cpf: 'any_cpf',
          email: 'any_email',
          name: 'any_name',
          phone_number: 'any_phone_number',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to update customer', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const updatedCustomer = await updateCustomer.update({
      customer_alias_id: createdCustomer.alias_id,
      data: {
        cpf: 'any_cpf',
        email: 'any_email',
        name: 'any_name',
        phone_number: 'any_phone_number',
      },
    });

    expect(updatedCustomer).toEqual({
      id: updatedCustomer.id,
      alias_id: updatedCustomer.alias_id,
      cpf: createdCustomer.cpf,
      email: createdCustomer.email,
      name: createdCustomer.name,
      phone_number: createdCustomer.phone_number,
      created_at: createdCustomer.created_at,
      updated_at: createdCustomer.updated_at,
    });
  });
});
