import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import FakeCustomerRepository from '@infra/database/repositories/customer/fakes/FakeCustomerRepository';

import FindCustomers from '@data/useCases/customer/FindCustomers';

let findCustomers: FindCustomers;

let fakeCustomerRepository: FakeCustomerRepository;

describe('FindCustomers', () => {
  beforeEach(() => {
    fakeCustomerRepository = new FakeCustomerRepository();

    findCustomers = new FindCustomers(fakeCustomerRepository);
  });

  it('should be able to find stores', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const foundCustomers = await findCustomers.find();

    expect(foundCustomers).toEqual([
      {
        id: createdCustomer.id,
        alias_id: createdCustomer.alias_id,
        name: createdCustomer.name,
        cpf: createdCustomer.cpf,
        email: createdCustomer.email,
        phone_number: createdCustomer.phone_number,
        created_at: createdCustomer.created_at,
        updated_at: createdCustomer.updated_at,
      },
    ]);
  });
});
