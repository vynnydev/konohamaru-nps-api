import AppError from '@utils/errors/AppError';

import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';
import mockEvaluationModel from '@tests/domain/mocks/MockEvaluationModel';

import FakeTransactionRepository from '@infra/database/repositories/transaction/fakes/FakeTransactionRepository';
import FakeEvaluationRepository from '@infra/database/repositories/evaluation/fakes/FakeEvaluationRepository';

import FindEvaluations from '@data/useCases/evaluation/FindEvaluations';

let fakeTransactionRepository: FakeTransactionRepository;
let fakeEvaluationRepository: FakeEvaluationRepository;

let findEvaluations: FindEvaluations;

describe('FindEvaluations', () => {
  beforeEach(() => {
    fakeTransactionRepository = new FakeTransactionRepository();
    fakeEvaluationRepository = new FakeEvaluationRepository();

    findEvaluations = new FindEvaluations(fakeTransactionRepository, fakeEvaluationRepository);
  });

  it('should be able to call find transaction by alias id with correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const findByAliasIdSpy = jest.spyOn(fakeTransactionRepository, 'findByAliasId');

    await findEvaluations.find({
      page: 1,
      limit: 10,
      transaction_alias_id: createdTransaction.alias_id,
    });

    expect(findByAliasIdSpy).toHaveBeenCalledWith(createdTransaction.alias_id);
  });

  it('should be able to throw if find by alias id throws', async () => {
    await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeTransactionRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      findEvaluations.find({
        page: 1,
        limit: 10,
        transaction_alias_id: 'any_alias_id',
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if transaction does not exists', async () => {
    await expect(
      findEvaluations.find({
        page: 1,
        limit: 10,
        transaction_alias_id: 'any_alias_id',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call count by transaction with correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const countByCompanyIdSpy = jest.spyOn(fakeEvaluationRepository, 'countByTransactionId');

    await findEvaluations.find({
      transaction_alias_id: createdTransaction.alias_id,
      limit: 10,
      page: 1,
    });

    expect(countByCompanyIdSpy).toHaveBeenCalledWith(createdTransaction.id);
  });

  it('should be able to throw if count by transaction throws', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeEvaluationRepository, 'countByTransactionId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      findEvaluations.find({
        transaction_alias_id: createdTransaction.alias_id,
        limit: 10,
        page: 1,
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call find by transaction correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const findByTransactionSpy = jest.spyOn(fakeEvaluationRepository, 'findByTransaction');

    await findEvaluations.find({
      transaction_alias_id: createdTransaction.alias_id,
      limit: 10,
      page: 1,
    });

    expect(findByTransactionSpy).toHaveBeenCalledWith({
      limit: 10,
      offset: 0,
      transaction_id: createdTransaction.id,
    });
  });

  it('should be able to throw if find by transaction throws', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeEvaluationRepository, 'findByTransaction').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      findEvaluations.find({
        transaction_alias_id: createdTransaction.alias_id,
        limit: 10,
        page: 1,
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to find evaluation', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const createdEvaluation = await fakeEvaluationRepository.create(
      mockEvaluationModel.mock({ transaction_id: createdTransaction.id }),
    );

    const { evaluations, total_pages } = await findEvaluations.find({
      page: 1,
      limit: 10,
      transaction_alias_id: createdTransaction.alias_id,
    });

    expect(evaluations).toEqual([
      {
        id: createdEvaluation.id,
        alias_id: createdEvaluation.alias_id,
        description: createdEvaluation.description,
        transaction_value: createdEvaluation.transaction_value,
        transaction_id: createdEvaluation.transaction_id,
        created_at: createdEvaluation.created_at,
        updated_at: createdEvaluation.updated_at,
      },
    ]);
    expect(total_pages).toBe(1);
  });
});
