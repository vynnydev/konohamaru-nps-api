import AppError from '@utils/errors/AppError';

import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';
import IEvaluationRepository from '@data/protocols/database/repositories/evaluation/IEvaluationRepository';

import IFindEvaluations from '@domain/useCases/evaluation/IFindEvaluations';
import IFindEvaluationsDTO from '@domain/useCases/evaluation/dtos/IFindEvaluationsDTO';
import IFindEvaluationsResponse from '@domain/useCases/evaluation/dtos/IFindEvaluationsResponse';

export default class FindEvaluations implements IFindEvaluations {
  constructor(
    private readonly transactionRepository: ITransactionRepository,
    private readonly evaluationRepository: IEvaluationRepository,
  ) {}

  public async find({
    page,
    limit,
    transaction_alias_id,
  }: IFindEvaluationsDTO): Promise<IFindEvaluationsResponse> {
    const foundTransaction = await this.transactionRepository.findByAliasId(transaction_alias_id);

    if (!foundTransaction)
      throw new AppError({ message: 'Transaction does not exists', status_code: 400 });

    const offset = (page - 1) * limit;

    const count = await this.evaluationRepository.countByTransactionId(foundTransaction.id);

    const foundEvaluations = await this.evaluationRepository.findByTransaction({
      limit,
      offset,
      transaction_id: foundTransaction.id,
    });

    const totalPages = Math.ceil(count / limit);

    return { evaluations: foundEvaluations, total_pages: totalPages };
  }
}
