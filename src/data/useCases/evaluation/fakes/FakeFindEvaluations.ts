import mockEvaluationModel from '@tests/domain/mocks/MockEvaluationModel';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import IFindEvaluations from '@domain/useCases/evaluation/IFindEvaluations';
import IFindEvaluationsDTO from '@domain/useCases/evaluation/dtos/IFindEvaluationsDTO';
import IFindEvaluationsResponse from '@domain/useCases/evaluation/dtos/IFindEvaluationsResponse';

export default class FakeFindEvaluations implements IFindEvaluations {
  public async find(data: IFindEvaluationsDTO): Promise<IFindEvaluationsResponse> {
    const evaluation = mockEvaluationModel.mock();

    const evaluations = [{ ...evaluation, transaction: mockTransactionModel.mock() }];

    return { evaluations, total_pages: 1 };
  }
}
