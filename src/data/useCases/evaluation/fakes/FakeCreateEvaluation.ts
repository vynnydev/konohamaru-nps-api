import Evaluation from '@domain/models/Evaluation';

import mockEvaluationModel from '@tests/domain/mocks/MockEvaluationModel';

import ICreateEvaluationDTO from '@domain/useCases/evaluation/dtos/ICreateEvaluationDTO';
import ICreateEvaluation from '@domain/useCases/evaluation/ICreateEvaluation';

export default class FakeCreateEvaluation implements ICreateEvaluation {
  public async create(data: ICreateEvaluationDTO): Promise<Evaluation> {
    const evaluation = mockEvaluationModel.mock();

    return evaluation;
  }
}
