import AppError from '@utils/errors/AppError';
import FakeAliasGenerator from '@utils/aliasGenerator/fakes/FakeAliasGenerator';

import FakeTransactionRepository from '@infra/database/repositories/transaction/fakes/FakeTransactionRepository';
import FakeEvaluationRepository from '@infra/database/repositories/evaluation/fakes/FakeEvaluationRepository';

import mockEvaluationModel from '@tests/domain/mocks/MockEvaluationModel';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import CreateEvaluation from '@data/useCases/evaluation/CreateEvaluation';

let fakeAliasGenerator: FakeAliasGenerator;

let fakeEvaluationRepository: FakeEvaluationRepository;
let fakeTransactionRepository: FakeTransactionRepository;

let createEvaluation: CreateEvaluation;

describe('CreateEvaluation', () => {
  beforeEach(() => {
    fakeAliasGenerator = new FakeAliasGenerator();

    fakeTransactionRepository = new FakeTransactionRepository();
    fakeEvaluationRepository = new FakeEvaluationRepository();

    createEvaluation = new CreateEvaluation(
      fakeTransactionRepository,
      fakeEvaluationRepository,
      fakeAliasGenerator,
    );
  });

  it('should be able to call find transaction by alias id with correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const findByAliasIdSpy = jest.spyOn(fakeTransactionRepository, 'findByAliasId');

    await createEvaluation.create({
      description: 'any_description',
      transaction_value: 9,
      transaction_alias_id: createdTransaction.alias_id,
    });

    expect(findByAliasIdSpy).toHaveBeenCalledWith(createdTransaction.alias_id);
  });

  it('should be able to throw if find by alias id throws', async () => {
    await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeTransactionRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createEvaluation.create({
        description: 'any_description',
        transaction_value: 9,
        transaction_alias_id: 'any_alias_id',
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if transaction does not exists', async () => {
    await expect(
      createEvaluation.create({
        description: 'any_description',
        transaction_value: 9,
        transaction_alias_id: 'any_alias_id',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call generate alias id with correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const generatedAliasId = jest.spyOn(fakeAliasGenerator, 'generate');

    await createEvaluation.create({
      description: 'any_description',
      transaction_value: 9,
      transaction_alias_id: createdTransaction.alias_id,
    });

    expect(generatedAliasId).toHaveBeenCalledWith('evaluation');
  });

  it('should be able to throw if generate alias throws', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeAliasGenerator, 'generate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createEvaluation.create({
        description: 'any_description',
        transaction_value: 9,
        transaction_alias_id: createdTransaction.alias_id,
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call create evaluation with correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const createSpy = jest.spyOn(fakeEvaluationRepository, 'create');

    await createEvaluation.create({
      description: 'any_description',
      transaction_value: 9,
      transaction_alias_id: createdTransaction.alias_id,
    });

    expect(createSpy).toHaveBeenCalledWith({
      alias_id: 'any_alias_id',
      description: 'any_description',
      transaction_value: 9,
      transaction_id: createdTransaction.id,
    });
  });

  it('should be able to throw if create evaluation throws', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    await fakeEvaluationRepository.create(mockEvaluationModel.mock());

    jest.spyOn(fakeEvaluationRepository, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createEvaluation.create({
        description: 'any_description',
        transaction_value: 9,
        transaction_alias_id: createdTransaction.alias_id,
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to create evaluation', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const createdEvaluation = await createEvaluation.create({
      description: 'any_description',
      transaction_value: 9,
      transaction_alias_id: createdTransaction.alias_id,
    });

    expect(createdEvaluation).toEqual({
      id: createdEvaluation.id,
      alias_id: createdEvaluation.alias_id,
      description: createdEvaluation.description,
      transaction_value: createdEvaluation.transaction_value,
      transaction_id: createdEvaluation.transaction_id,
      created_at: createdEvaluation.created_at,
      updated_at: createdEvaluation.updated_at,
    });
  });
});
