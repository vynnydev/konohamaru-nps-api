import AppError from '@utils/errors/AppError';
import IAliasGenerator from '@data/protocols/utils/aliasGenerator/IAliasGenerator';

import Evaluation from '@domain/models/Evaluation';

import ITransactionsRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';
import IEvaluationRepository from '@data/protocols/database/repositories/evaluation/IEvaluationRepository';

import ICreateEvaluation from '@domain/useCases/evaluation/ICreateEvaluation';
import ICreateEvaluationDTO from '@domain/useCases/evaluation/dtos/ICreateEvaluationDTO';

export default class CreateEvaluation implements ICreateEvaluation {
  constructor(
    private readonly transactionRepository: ITransactionsRepository,
    private readonly evaluationRepository: IEvaluationRepository,
    private readonly aliasGenerator: IAliasGenerator,
  ) {}

  public async create({
    transaction_value,
    description,
    transaction_alias_id,
  }: ICreateEvaluationDTO): Promise<Evaluation> {
    const foundTransaction = await this.transactionRepository.findByAliasId(transaction_alias_id);

    if (!foundTransaction)
      throw new AppError({
        message: 'Evaluation experience does not exists',
        status_code: 400,
      });

    const generatedAlias = this.aliasGenerator.generate('evaluation');

    const createdEvaluation = await this.evaluationRepository.create({
      alias_id: generatedAlias,
      transaction_value,
      description,
      transaction_id: foundTransaction.id,
    });

    return createdEvaluation;
  }
}
