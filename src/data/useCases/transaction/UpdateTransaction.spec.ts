import AppError from '@utils/errors/AppError';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import FakeTransactionRepository from '@infra/database/repositories/transaction/fakes/FakeTransactionRepository';

import UpdateTransaction from '@data/useCases/transaction/UpdateTransaction';

let fakeTransactionRepository: FakeTransactionRepository;

let updateTransaction: UpdateTransaction;

describe('UpdateTransaction', () => {
  beforeEach(() => {
    fakeTransactionRepository = new FakeTransactionRepository();

    updateTransaction = new UpdateTransaction(fakeTransactionRepository);
  });

  it('should be able to call find transaction by alias id with correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const findByAliasId = jest.spyOn(fakeTransactionRepository, 'findByAliasId');

    await updateTransaction.update({
      transaction_alias_id: createdTransaction.alias_id,
      data: {
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
      },
    });

    expect(findByAliasId).toHaveBeenCalledWith(createdTransaction.alias_id);
  });

  it('should be able to throw if find transaction by alias id throws', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeTransactionRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(
      updateTransaction.update({
        transaction_alias_id: createdTransaction.alias_id,
        data: {
          store_evaluation: EStoreEvaluation.OTIMO,
          provider_date: new Date(2022, 3, 15),
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if transaction does not exists', async () => {
    await expect(
      updateTransaction.update({
        transaction_alias_id: 'any_alias_id',
        data: {
          store_evaluation: EStoreEvaluation.OTIMO,
          provider_date: new Date(2022, 3, 15),
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call update transaction with correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const updateSpy = jest.spyOn(fakeTransactionRepository, 'findByAliasId');

    await updateTransaction.update({
      transaction_alias_id: createdTransaction.alias_id,
      data: {
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
      },
    });

    expect(updateSpy).toHaveBeenCalledWith(createdTransaction.alias_id);
  });

  it('should be able to throw if update transaction throws', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeTransactionRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(
      updateTransaction.update({
        transaction_alias_id: createdTransaction.alias_id,
        data: {
          store_evaluation: EStoreEvaluation.OTIMO,
          provider_date: new Date(2022, 3, 15),
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if update return undefined', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeTransactionRepository, 'update').mockImplementationOnce(async () => undefined);

    await expect(
      updateTransaction.update({
        transaction_alias_id: createdTransaction.alias_id,
        data: {
          store_evaluation: EStoreEvaluation.OTIMO,
          provider_date: new Date(2022, 3, 15),
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to update transaction', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const updatedTransaction = await updateTransaction.update({
      transaction_alias_id: createdTransaction.alias_id,
      data: {
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
      },
    });

    expect(updatedTransaction).toEqual({
      id: updatedTransaction.id,
      alias_id: updatedTransaction.alias_id,
      store_evaluation: createdTransaction.store_evaluation,
      provider_date: createdTransaction.provider_date,
      store_id: createdTransaction.store_id,
      employee_id: createdTransaction.employee_id,
      customer_id: createdTransaction.customer_id,
      created_at: updatedTransaction.created_at,
      updated_at: updatedTransaction.updated_at,
    });
  });
});
