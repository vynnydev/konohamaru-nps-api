import AppError from '@utils/errors/AppError';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import Transaction from '@domain/models/Transaction';

import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';

import IUpdateTransaction from '@domain/useCases/transaction/IUpdateTransaction';
import IUpdateTransactionDTO from '@domain/useCases/transaction/dtos/IUpdateTransactionDTO';

export default class UpdateTransaction implements IUpdateTransaction {
  constructor(private readonly transactionRepository: ITransactionRepository) {}

  public async update({ transaction_alias_id, data }: IUpdateTransactionDTO): Promise<Transaction> {
    const foundTransaction = await this.transactionRepository.findByAliasId(transaction_alias_id);

    if (!foundTransaction)
      throw new AppError({ message: 'Transaction does not exists', status_code: 400 });

    const { store_evaluation, provider_date } = data;

    const updatedTransaction = await this.transactionRepository.update({
      id: foundTransaction.id,
      data: {
        store_evaluation: EStoreEvaluation[store_evaluation],
        provider_date,
      },
    });

    if (!updatedTransaction)
      throw new AppError({ message: 'Could not update transaction', status_code: 400 });

    return updatedTransaction;
  }
}
