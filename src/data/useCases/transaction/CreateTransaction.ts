import AppError from '@utils/errors/AppError';
import IAliasGenerator from '@data/protocols/utils/aliasGenerator/IAliasGenerator';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import Transaction from '@domain/models/Transaction';

import IStoreRepository from '@data/protocols/database/repositories/store/IStoreRepository';
import IEmployeeRepository from '@data/protocols/database/repositories/employee/IEmployeeRepository';
import ICustomerRepository from '@data/protocols/database/repositories/customer/ICustomerRepository';

import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';

import ICreateTransaction from '@domain/useCases/transaction/ICreateTransaction';
import ICreateTransactionDTO from '@domain/useCases/transaction/dtos/ICreateTransactionDTO';

export default class CreateTransaction implements ICreateTransaction {
  constructor(
    private readonly storeRepository: IStoreRepository,
    private readonly employeeRepository: IEmployeeRepository,
    private readonly customerRepository: ICustomerRepository,
    private readonly transactionRepository: ITransactionRepository,
    private readonly aliasGenerator: IAliasGenerator,
  ) {}

  public async create({
    store_alias_id,
    employee_alias_id,
    customer_alias_id,
    store_evaluation,
    provider_date,
  }: ICreateTransactionDTO): Promise<Transaction> {
    const foundStore = await this.storeRepository.findByAliasId(store_alias_id);

    if (!foundStore)
      throw new AppError({
        message: 'Store does not exists',
        status_code: 400,
      });

    const foundEmployee = await this.employeeRepository.findByAliasId(employee_alias_id);

    if (!foundEmployee || !foundEmployee.is_active)
      throw new AppError({
        message: 'Employee does not exists or is inactive',
        status_code: 400,
      });

    const foundCustomer = await this.customerRepository.findByAliasId(customer_alias_id);

    if (!foundCustomer)
      throw new AppError({
        message: 'Customer does not exists',
        status_code: 400,
      });

    const generatedAlias = this.aliasGenerator.generate('transaction');

    const createdTransaction = await this.transactionRepository.create({
      alias_id: generatedAlias,
      store_evaluation: EStoreEvaluation[store_evaluation],
      provider_date,
      store_id: foundStore.id,
      employee_id: foundEmployee.id,
      customer_id: foundCustomer.id,
    });

    return createdTransaction;
  }
}
