import AppError from '@utils/errors/AppError';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import FakeAliasGenerator from '@utils/aliasGenerator/fakes/FakeAliasGenerator';

import mockStoreModel from '@tests/domain/mocks/MockStoreModel';
import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';
import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import FakeStoreRepository from '@infra/database/repositories/store/fakes/FakeStoreRepository';
import FakeEmployeeRepository from '@infra/database/repositories/employee/fakes/FakeEmployeeRepository';
import FakeCustomerRepository from '@infra/database/repositories/customer/fakes/FakeCustomerRepository';

import FakeTransactionRepository from '@infra/database/repositories/transaction/fakes/FakeTransactionRepository';

import CreateTransaction from '@data/useCases/transaction/CreateTransaction';

let fakeStoreRepository: FakeStoreRepository;
let fakeEmployeeRepository: FakeEmployeeRepository;
let fakeCustomerRepository: FakeCustomerRepository;

let fakeTransactionRepository: FakeTransactionRepository;

let fakeAliasGenerator: FakeAliasGenerator;

let createTransaction: CreateTransaction;

describe('CreateTransaction', () => {
  beforeEach(() => {
    fakeAliasGenerator = new FakeAliasGenerator();

    fakeStoreRepository = new FakeStoreRepository();
    fakeEmployeeRepository = new FakeEmployeeRepository();
    fakeCustomerRepository = new FakeCustomerRepository();
    fakeTransactionRepository = new FakeTransactionRepository();

    fakeAliasGenerator = new FakeAliasGenerator();

    createTransaction = new CreateTransaction(
      fakeStoreRepository,
      fakeEmployeeRepository,
      fakeCustomerRepository,
      fakeTransactionRepository,
      fakeAliasGenerator,
    );
  });

  it('should be able to call find store by alias id with correct values', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const findByAliasIdSpy = jest.spyOn(fakeStoreRepository, 'findByAliasId');

    await createTransaction.create({
      store_evaluation: EStoreEvaluation.OTIMO,
      provider_date: new Date(2022, 3, 15),
      store_alias_id: createdStore.alias_id,
      customer_alias_id: createdCustomer.alias_id,
      employee_alias_id: createdEmployee.alias_id,
    });

    expect(findByAliasIdSpy).toHaveBeenCalledWith(createdStore.alias_id);
  });

  it('should be able to throw if find by alias id throws', async () => {
    await fakeStoreRepository.create(mockStoreModel.mock());

    jest.spyOn(fakeStoreRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createTransaction.create({
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
        store_alias_id: 'any_alias_id',
        customer_alias_id: 'any_alias_id',
        employee_alias_id: 'any_alias_id',
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if store does not exists', async () => {
    await expect(
      createTransaction.create({
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
        store_alias_id: 'any_alias_id',
        customer_alias_id: 'any_alias_id',
        employee_alias_id: 'any_alias_id',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call generate alias id with correct values', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const generatedAliasId = jest.spyOn(fakeAliasGenerator, 'generate');

    await createTransaction.create({
      store_evaluation: EStoreEvaluation.OTIMO,
      provider_date: new Date(2022, 3, 15),
      store_alias_id: createdStore.alias_id,
      customer_alias_id: createdCustomer.alias_id,
      employee_alias_id: createdEmployee.alias_id,
    });

    expect(generatedAliasId).toHaveBeenCalledWith('transaction');
  });

  it('should be able to throw if generate alias throws', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    jest.spyOn(fakeAliasGenerator, 'generate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createTransaction.create({
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
        store_alias_id: createdStore.alias_id,
        customer_alias_id: createdCustomer.alias_id,
        employee_alias_id: createdEmployee.alias_id,
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if employee does not exists', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    await expect(
      createTransaction.create({
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
        store_alias_id: createdStore.alias_id,
        employee_alias_id: 'any_alias_id',
        customer_alias_id: 'any_alias_id',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to throw if customer does not exists', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    await expect(
      createTransaction.create({
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
        store_alias_id: createdStore.alias_id,
        employee_alias_id: createdEmployee.alias_id,
        customer_alias_id: 'any_alias_id',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call create transaction with correct values', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const createSpy = jest.spyOn(fakeTransactionRepository, 'create');

    await createTransaction.create({
      store_evaluation: EStoreEvaluation.OTIMO,
      provider_date: new Date(2022, 3, 15),
      store_alias_id: createdStore.alias_id,
      customer_alias_id: createdCustomer.alias_id,
      employee_alias_id: createdEmployee.alias_id,
    });

    expect(createSpy).toHaveBeenCalledWith({
      alias_id: 'any_alias_id',
      store_evaluation: EStoreEvaluation.OTIMO,
      provider_date: new Date(2022, 3, 15),
      store_id: createdStore.id,
      customer_id: createdCustomer.id,
      employee_id: createdEmployee.id,
    });
  });

  it('should be able to throw if create transaction throws', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeTransactionRepository, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createTransaction.create({
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
        store_alias_id: createdStore.alias_id,
        customer_alias_id: createdCustomer.alias_id,
        employee_alias_id: createdEmployee.alias_id,
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to create transaction', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const createdTransaction = await createTransaction.create({
      store_evaluation: EStoreEvaluation.OTIMO,
      provider_date: new Date(2022, 3, 15),
      store_alias_id: createdStore.alias_id,
      customer_alias_id: createdCustomer.alias_id,
      employee_alias_id: createdEmployee.alias_id,
    });

    expect(createdTransaction).toEqual({
      id: createdTransaction.id,
      alias_id: createdTransaction.alias_id,
      store_evaluation: createdTransaction.store_evaluation,
      provider_date: createdTransaction.provider_date,
      store_id: createdTransaction.store_id,
      employee_id: createdTransaction.employee_id,
      customer_id: createdTransaction.customer_id,
      created_at: createdTransaction.created_at,
      updated_at: createdTransaction.updated_at,
    });
  });
});
