import Transaction from '@domain/models/Transaction';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import ICreateTransactionDTO from '@domain/useCases/transaction/dtos/ICreateTransactionDTO';
import ICreateTransaction from '@domain/useCases/transaction/ICreateTransaction';

export default class FakeCreateTransaction implements ICreateTransaction {
  public async create(data: ICreateTransactionDTO): Promise<Transaction> {
    const transaction = mockTransactionModel.mock();

    return transaction;
  }
}
