import FakeCreateTransaction from '@data/useCases/transaction/fakes/FakeCreateTransaction';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

let fakeCreateTransaction: FakeCreateTransaction;

describe('FakeCreateTransaction', () => {
  beforeEach(() => {
    fakeCreateTransaction = new FakeCreateTransaction();
  });

  it('should be able to create transaction', async () => {
    const createdTransaction = await fakeCreateTransaction.create({
      store_evaluation: EStoreEvaluation.OTIMO,
      provider_date: new Date(2022, 3, 15),
      store_alias_id: 'any_alias_id',
      customer_alias_id: 'any_alias_id',
      employee_alias_id: 'any_alias_id',
    });

    expect(createdTransaction).toEqual({
      id: expect.any(String),
      alias_id: expect.any(String),
      store_evaluation: expect.any(String),
      provider_date: expect.any(Date),
      store_id: expect.any(String),
      customer_id: expect.any(String),
      employee_id: expect.any(String),
      created_at: expect.any(Date),
      updated_at: expect.any(Date),
    });
  });
});
