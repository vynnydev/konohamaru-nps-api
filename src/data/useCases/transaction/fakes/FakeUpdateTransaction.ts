import Transaction from '@domain/models/Transaction';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import IUpdateTransaction from '@domain/useCases/transaction/IUpdateTransaction';
import IUpdateTransactionDTO from '@domain/useCases/transaction/dtos/IUpdateTransactionDTO';

export default class FakeUpdateTransaction implements IUpdateTransaction {
  public async update(data: IUpdateTransactionDTO): Promise<Transaction> {
    const transaction = mockTransactionModel.mock();

    return transaction;
  }
}
