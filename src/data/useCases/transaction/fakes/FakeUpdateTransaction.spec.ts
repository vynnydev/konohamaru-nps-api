import FakeUpdateTransaction from '@data/useCases/transaction/fakes/FakeUpdateTransaction';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

let fakeUpdateTransaction: FakeUpdateTransaction;

describe('FakeUpdateTransaction', () => {
  beforeEach(() => {
    fakeUpdateTransaction = new FakeUpdateTransaction();
  });

  it('should be able to update transaction', async () => {
    const updatedTransaction = await fakeUpdateTransaction.update({
      transaction_alias_id: 'any_alias_id',
      data: {
        store_evaluation: EStoreEvaluation.OTIMO,
        provider_date: new Date(2022, 3, 15),
      },
    });

    expect(updatedTransaction).toEqual({
      id: expect.any(String),
      alias_id: expect.any(String),
      store_evaluation: expect.any(String),
      provider_date: expect.any(Date),
      store_id: expect.any(String),
      customer_id: expect.any(String),
      employee_id: expect.any(String),
      created_at: expect.any(Date),
      updated_at: expect.any(Date),
    });
  });
});
