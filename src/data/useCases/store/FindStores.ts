import Store from '@domain/models/Store';

import IStoreRepository from '@data/protocols/database/repositories/store/IStoreRepository';
import IFindStores from '@domain/useCases/store/IFindStores';

export default class FindStores implements IFindStores {
  constructor(private readonly storeRepository: IStoreRepository) {}

  public async find(): Promise<Store[]> {
    const foundStores = await this.storeRepository.findAll();

    return foundStores;
  }
}
