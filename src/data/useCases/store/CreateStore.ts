import AppError from '@utils/errors/AppError';
import IAliasGenerator from '@data/protocols/utils/aliasGenerator/IAliasGenerator';

import Store from '@domain/models/Store';
import IStoreRepository from '@data/protocols/database/repositories/store/IStoreRepository';

import ICreateStore from '@domain/useCases/store/ICreateStore';

export default class CreateStore implements ICreateStore {
  constructor(
    private readonly storeRepository: IStoreRepository,
    private readonly aliasGenerator: IAliasGenerator,
  ) {}

  public async create(name: string): Promise<Store> {
    const foundStore = await this.storeRepository.findByName(name);

    if (foundStore)
      throw new AppError({ message: 'Store name already not exists', status_code: 400 });

    const generatedAlias = this.aliasGenerator.generate('store');

    const createdStore = await this.storeRepository.create({
      alias_id: generatedAlias,
      name,
    });

    return createdStore;
  }
}
