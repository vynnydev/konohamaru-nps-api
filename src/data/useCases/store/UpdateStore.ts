import AppError from '@utils/errors/AppError';

import Store from '@domain/models/Store';

import IStoreRepository from '@data/protocols/database/repositories/store/IStoreRepository';

import IUpdateStore from '@domain/useCases/store/IUpdateStore';
import IUpdateStoreDTO from '@domain/useCases/store/dtos/IUpdateStoreDTO';

export default class UpdateStore implements IUpdateStore {
  constructor(private readonly storeRepository: IStoreRepository) {}

  public async update({ store_alias_id, data }: IUpdateStoreDTO): Promise<Store> {
    const foundStore = await this.storeRepository.findByAliasId(store_alias_id);

    if (!foundStore) throw new AppError({ message: 'store does not exists', status_code: 400 });

    const { name } = data;

    const updatedStore = await this.storeRepository.update({
      id: foundStore.id,
      data: { name },
    });

    if (!updatedStore) throw new AppError({ message: 'Could not update store', status_code: 400 });

    return updatedStore;
  }
}
