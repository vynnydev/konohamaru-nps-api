import AppError from '@utils/errors/AppError';
import FakeAliasGenerator from '@utils/aliasGenerator/fakes/FakeAliasGenerator';

import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import FakeStoreRepository from '@infra/database/repositories/store/fakes/FakeStoreRepository';

import UpdateStore from '@data/useCases/store/UpdateStore';

let fakeStoreRepository: FakeStoreRepository;

let updateStore: UpdateStore;

describe('UpdateStore', () => {
  beforeEach(() => {
    fakeStoreRepository = new FakeStoreRepository();

    updateStore = new UpdateStore(fakeStoreRepository);
  });

  it('should be able to call find store by alias id with correct values', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const findByAliasId = jest.spyOn(fakeStoreRepository, 'findByAliasId');

    await updateStore.update({
      store_alias_id: createdStore.alias_id,
      data: {
        name: 'any_name',
      },
    });

    expect(findByAliasId).toHaveBeenCalledWith(createdStore.alias_id);
  });

  it('should be able to throw if find store by alias id throws', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    jest.spyOn(fakeStoreRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(
      updateStore.update({
        store_alias_id: createdStore.alias_id,
        data: {
          name: 'any_name',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if store does not exists', async () => {
    await expect(
      updateStore.update({
        store_alias_id: 'any_alias_id',
        data: {
          name: 'any_name',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call update store with correct values', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const updateSpy = jest.spyOn(fakeStoreRepository, 'findByAliasId');

    await updateStore.update({
      store_alias_id: createdStore.alias_id,
      data: {
        name: 'any_name',
      },
    });

    expect(updateSpy).toHaveBeenCalledWith(createdStore.alias_id);
  });

  it('should be able to throw if update store throws', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    jest.spyOn(fakeStoreRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(
      updateStore.update({
        store_alias_id: createdStore.alias_id,
        data: {
          name: 'any_name',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if update return undefined', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    jest.spyOn(fakeStoreRepository, 'update').mockImplementationOnce(async () => undefined);

    await expect(
      updateStore.update({
        store_alias_id: createdStore.alias_id,
        data: {
          name: 'any_name',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to update store', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const updatedStore = await updateStore.update({
      store_alias_id: createdStore.alias_id,
      data: {
        name: 'any_name',
      },
    });

    expect(updatedStore).toEqual({
      id: updatedStore.id,
      alias_id: updatedStore.alias_id,
      name: updatedStore.name,
      created_at: updatedStore.created_at,
      updated_at: updatedStore.updated_at,
    });
  });
});
