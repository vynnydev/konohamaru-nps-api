import AppError from '@utils/errors/AppError';

import IStoreRepository from '@data/protocols/database/repositories/store/IStoreRepository';

import IDeleteStore from '@domain/useCases/store/IDeleteStore';

export default class DeleteStore implements IDeleteStore {
  constructor(private readonly storeRepository: IStoreRepository) {}

  public async delete(store_alias_id: string): Promise<void> {
    const foundStore = await this.storeRepository.findByAliasId(store_alias_id);

    if (!foundStore)
      throw new AppError({ message: 'store credential does not exists', status_code: 400 });

    await this.storeRepository.deleteById(foundStore.id);
  }
}
