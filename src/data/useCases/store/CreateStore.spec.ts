import AppError from '@utils/errors/AppError';

import FakeAliasGenerator from '@utils/aliasGenerator/fakes/FakeAliasGenerator';

import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import FakeStoreRepository from '@infra/database/repositories/store/fakes/FakeStoreRepository';

import CreateStore from '@data/useCases/store/CreateStore';

let fakeAliasGenerator: FakeAliasGenerator;

let fakeStoreRepository: FakeStoreRepository;

let createStore: CreateStore;

describe('CreateStore', () => {
  beforeEach(() => {
    fakeAliasGenerator = new FakeAliasGenerator();

    fakeStoreRepository = new FakeStoreRepository();

    createStore = new CreateStore(fakeStoreRepository, fakeAliasGenerator);
  });

  it('should be able to throw if store already exists', async () => {
    const store = mockStoreModel.mock();

    jest.spyOn(fakeStoreRepository, 'findByName').mockImplementationOnce(async () => store);

    await expect(createStore.create('any_name')).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call generate alias id with correct values', async () => {
    const generatedAliasId = jest.spyOn(fakeAliasGenerator, 'generate');

    await createStore.create('any_name');

    expect(generatedAliasId).toHaveBeenCalledWith('store');
  });

  it('should be able to throw if generate alias throws', async () => {
    jest.spyOn(fakeAliasGenerator, 'generate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(createStore.create('any_name')).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call create store with correct values', async () => {
    const createSpy = jest.spyOn(fakeStoreRepository, 'create');

    await createStore.create('any_name');

    expect(createSpy).toHaveBeenCalledWith({
      alias_id: 'any_alias_id',
      name: 'any_name',
    });
  });

  it('should be able to throw if create store throws', async () => {
    await fakeStoreRepository.create(mockStoreModel.mock());

    jest.spyOn(fakeStoreRepository, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(createStore.create('any_name')).rejects.toBeInstanceOf(Error);
  });

  it('should be able to create store', async () => {
    const createdStore = await createStore.create('any_name');

    expect(createdStore).toEqual({
      id: createdStore.id,
      alias_id: createdStore.alias_id,
      name: createdStore.name,
      created_at: createdStore.created_at,
      updated_at: createdStore.updated_at,
    });
  });
});
