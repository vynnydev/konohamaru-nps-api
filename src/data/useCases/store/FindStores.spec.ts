import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import FakeStoreRepository from '@infra/database/repositories/store/fakes/FakeStoreRepository';

import FindStores from '@data/useCases/store/FindStores';

let findStores: FindStores;

let fakeStoreRepository: FakeStoreRepository;

describe('FindStores', () => {
  beforeEach(() => {
    fakeStoreRepository = new FakeStoreRepository();

    findStores = new FindStores(fakeStoreRepository);
  });

  it('should be able to find stores', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const foundStores = await findStores.find();

    expect(foundStores).toEqual([
      {
        id: createdStore.id,
        alias_id: createdStore.alias_id,
        name: createdStore.name,
        created_at: createdStore.created_at,
        updated_at: createdStore.updated_at,
      },
    ]);
  });
});
