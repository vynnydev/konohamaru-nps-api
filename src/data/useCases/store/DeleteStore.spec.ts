import AppError from '@utils/errors/AppError';

import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import FakeStoreRepository from '@infra/database/repositories/store/fakes/FakeStoreRepository';

import DeleteStore from '@data/useCases/store/DeleteStore';

let fakeStoreRepository: FakeStoreRepository;

let deleteStore: DeleteStore;

describe('DeleteStore', () => {
  beforeEach(() => {
    fakeStoreRepository = new FakeStoreRepository();

    deleteStore = new DeleteStore(fakeStoreRepository);
  });

  it('should be able to call delete store with correct values', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const updateSpy = jest.spyOn(fakeStoreRepository, 'findByAliasId');

    await deleteStore.delete(createdStore.alias_id);

    expect(updateSpy).toHaveBeenCalledWith(createdStore.alias_id);
  });

  it('should be able to throw if delete store throws', async () => {
    await fakeStoreRepository.create(mockStoreModel.mock());

    jest.spyOn(fakeStoreRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(deleteStore.delete('any_alias_id')).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if store does not exists', async () => {
    await expect(deleteStore.delete('any_alias_id')).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to delete store', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const error = await deleteStore.delete(createdStore.alias_id);

    expect(error).toBeFalsy();
  });
});
