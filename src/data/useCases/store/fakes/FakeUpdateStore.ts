import Store from '@domain/models/Store';

import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import IUpdateStore from '@domain/useCases/store/IUpdateStore';
import IUpdateStoreDTO from '@domain/useCases/store/dtos/IUpdateStoreDTO';

export default class FakeUpdateStore implements IUpdateStore {
  public async update(data: IUpdateStoreDTO): Promise<Store> {
    const store = mockStoreModel.mock();

    return store;
  }
}
