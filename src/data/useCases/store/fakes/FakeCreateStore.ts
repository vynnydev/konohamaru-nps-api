import Store from '@domain/models/Store';

import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import ICreateStore from '@domain/useCases/store/ICreateStore';

export default class FakeCreateStore implements ICreateStore {
  public async create(name: string): Promise<Store> {
    const store = mockStoreModel.mock();

    return store;
  }
}
