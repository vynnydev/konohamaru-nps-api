import IDeleteStore from '@domain/useCases/store/IDeleteStore';

export default class FakeDeleteStore implements IDeleteStore {
  private readonly store: Array<string> = [];

  public async delete(store_alias_id: string): Promise<void> {
    this.store.push('deleted');
  }
}
