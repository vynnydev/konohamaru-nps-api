import Store from '@domain/models/Store';
import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import IFindStores from '@domain/useCases/store/IFindStores';

export default class FakeFindStores implements IFindStores {
  public async find(): Promise<Store[]> {
    return [mockStoreModel.mock()];
  }
}
