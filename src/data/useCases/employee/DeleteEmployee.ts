import AppError from '@utils/errors/AppError';

import IEmployeeRepository from '@data/protocols/database/repositories/employee/IEmployeeRepository';

import IDeleteEmployee from '@domain/useCases/employee/IDeleteEmployee';

export default class DeleteEmployee implements IDeleteEmployee {
  constructor(private readonly employeeRepository: IEmployeeRepository) {}

  public async delete(employee_alias_id: string): Promise<void> {
    const foundEmployee = await this.employeeRepository.findByAliasId(employee_alias_id);

    if (!foundEmployee)
      throw new AppError({ message: 'Employee credential does not exists', status_code: 400 });

    const updatedEmployee = await this.employeeRepository.update({
      id: foundEmployee.id,
      data: { is_active: false },
    });

    if (!updatedEmployee)
      throw new AppError({ message: 'Could not update employee', status_code: 400 });
  }
}
