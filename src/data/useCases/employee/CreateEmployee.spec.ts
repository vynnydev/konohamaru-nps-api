import FakeAliasGenerator from '@utils/aliasGenerator/fakes/FakeAliasGenerator';

import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import FakeEmployeeRepository from '@infra/database/repositories/employee/fakes/FakeEmployeeRepository';

import CreateEmployee from '@data/useCases/employee/CreateEmployee';

let fakeAliasGenerator: FakeAliasGenerator;

let fakeEmployeeRepository: FakeEmployeeRepository;

let createEmployee: CreateEmployee;

describe('CreateEmployee', () => {
  beforeEach(() => {
    fakeAliasGenerator = new FakeAliasGenerator();

    fakeEmployeeRepository = new FakeEmployeeRepository();

    createEmployee = new CreateEmployee(fakeEmployeeRepository, fakeAliasGenerator);
  });

  it('should be able to call generate alias id with correct values', async () => {
    const generatedAliasId = jest.spyOn(fakeAliasGenerator, 'generate');

    await createEmployee.create('any_name');

    expect(generatedAliasId).toHaveBeenCalledWith('employee');
  });

  it('should be able to throw if generate alias throws', async () => {
    jest.spyOn(fakeAliasGenerator, 'generate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(createEmployee.create('any_name')).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call create employee with correct values', async () => {
    const createSpy = jest.spyOn(fakeEmployeeRepository, 'create');

    await createEmployee.create('any_name');

    expect(createSpy).toHaveBeenCalledWith({
      alias_id: 'any_alias_id',
      name: 'any_name',
      is_active: true,
    });
  });

  it('should be able to throw if create employee throws', async () => {
    await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    jest.spyOn(fakeEmployeeRepository, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(createEmployee.create('any_name')).rejects.toBeInstanceOf(Error);
  });

  it('should be able to create employee', async () => {
    const createdEmployee = await createEmployee.create('any_name');

    expect(createdEmployee).toEqual({
      id: createdEmployee.id,
      alias_id: createdEmployee.alias_id,
      name: createdEmployee.name,
      is_active: true,
      created_at: createdEmployee.created_at,
      updated_at: createdEmployee.updated_at,
    });
  });
});
