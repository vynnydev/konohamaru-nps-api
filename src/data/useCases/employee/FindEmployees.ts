import Employee from '@domain/models/Employee';

import IEmployeeRepository from '@data/protocols/database/repositories/employee/IEmployeeRepository';
import IFindEmployees from '@domain/useCases/employee/IFindEmployees';

export default class FindEmployees implements IFindEmployees {
  constructor(private readonly employeeRepository: IEmployeeRepository) {}

  public async find(): Promise<Employee[]> {
    const foundEmployees = await this.employeeRepository.findEmployees();

    const isActiveFiltered = foundEmployees.filter(employee => employee.is_active);

    return isActiveFiltered;
  }
}
