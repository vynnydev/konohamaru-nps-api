import Employee from '@domain/models/Employee';
import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import IFindEmployees from '@domain/useCases/employee/IFindEmployees';

export default class FakeFindEmployees implements IFindEmployees {
  public async find(): Promise<Employee[]> {
    return [mockEmployeeModel.mock()];
  }
}
