import Employee from '@domain/models/Employee';

import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import IUpdateEmployee from '@domain/useCases/employee/IUpdateEmployee';
import IUpdateEmployeeDTO from '@domain/useCases/employee/dtos/IUpdateEmployeeDTO';

export default class FakeUpdateEmployee implements IUpdateEmployee {
  public async update(data: IUpdateEmployeeDTO): Promise<Employee> {
    const employee = mockEmployeeModel.mock();

    return employee;
  }
}
