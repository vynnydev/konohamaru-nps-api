import IDeleteEmployee from '@domain/useCases/employee/IDeleteEmployee';

export default class FakeDeleteEmployee implements IDeleteEmployee {
  private readonly employee: Array<string> = [];

  public async delete(employee_alias_id: string): Promise<void> {
    this.employee.push('deleted');
  }
}
