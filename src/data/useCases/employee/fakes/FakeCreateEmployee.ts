import Employee from '@domain/models/Employee';

import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import ICreateEmployee from '@domain/useCases/employee/ICreateEmployee';

export default class FakeCreateEmployee implements ICreateEmployee {
  public async create(name: string): Promise<Employee> {
    const employee = mockEmployeeModel.mock();

    return employee;
  }
}
