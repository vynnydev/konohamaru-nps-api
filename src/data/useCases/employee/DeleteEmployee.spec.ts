import AppError from '@utils/errors/AppError';

import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import FakeEmployeeRepository from '@infra/database/repositories/employee/fakes/FakeEmployeeRepository';

import DeleteEmployee from '@data/useCases/employee/DeleteEmployee';

let fakeEmployeeRepository: FakeEmployeeRepository;

let deleteEmployee: DeleteEmployee;

describe('DeleteEmployee', () => {
  beforeEach(() => {
    fakeEmployeeRepository = new FakeEmployeeRepository();

    deleteEmployee = new DeleteEmployee(fakeEmployeeRepository);
  });

  it('should be able to call delete employee with correct values', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    const updateSpy = jest.spyOn(fakeEmployeeRepository, 'findByAliasId');

    await deleteEmployee.delete(createdEmployee.alias_id);

    expect(updateSpy).toHaveBeenCalledWith(createdEmployee.alias_id);
  });

  it('should be able to throw if delete employee throws', async () => {
    await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    jest.spyOn(fakeEmployeeRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(deleteEmployee.delete('any_alias_id')).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if employee does not exists', async () => {
    await expect(deleteEmployee.delete('any_alias_id')).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to throw if delete return undefined', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    jest.spyOn(fakeEmployeeRepository, 'update').mockImplementationOnce(async () => undefined);

    await expect(deleteEmployee.delete(createdEmployee.alias_id)).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to delete employee', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    const error = await deleteEmployee.delete(createdEmployee.alias_id);

    expect(error).toBeFalsy();
  });
});
