import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import FakeEmployeeRepository from '@infra/database/repositories/employee/fakes/FakeEmployeeRepository';

import FindEmployees from '@data/useCases/employee/FindEmployees';

let findEmployees: FindEmployees;

let fakeEmployeeRepository: FakeEmployeeRepository;

describe('FindEmployees', () => {
  beforeEach(() => {
    fakeEmployeeRepository = new FakeEmployeeRepository();

    findEmployees = new FindEmployees(fakeEmployeeRepository);
  });

  it('should be able to find employees', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const foundEmployees = await findEmployees.find();

    expect(foundEmployees).toEqual([
      {
        id: createdEmployee.id,
        alias_id: createdEmployee.alias_id,
        name: createdEmployee.name,
        is_active: true,
        created_at: createdEmployee.created_at,
        updated_at: createdEmployee.updated_at,
      },
    ]);
  });
});
