import IAliasGenerator from '@data/protocols/utils/aliasGenerator/IAliasGenerator';

import Employee from '@domain/models/Employee';
import IEmployeeRepository from '@data/protocols/database/repositories/employee/IEmployeeRepository';

import ICreateEmployee from '@domain/useCases/employee/ICreateEmployee';

export default class CreateEmployee implements ICreateEmployee {
  constructor(
    private readonly employeeRepository: IEmployeeRepository,
    private readonly aliasGenerator: IAliasGenerator,
  ) {}

  public async create(name: string): Promise<Employee> {
    const generatedAlias = this.aliasGenerator.generate('employee');

    const createdEmployee = await this.employeeRepository.create({
      alias_id: generatedAlias,
      name,
      is_active: true,
    });

    return createdEmployee;
  }
}
