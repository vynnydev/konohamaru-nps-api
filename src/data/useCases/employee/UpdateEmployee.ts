import AppError from '@utils/errors/AppError';

import Employee from '@domain/models/Employee';
import IEmployeeRepository from '@data/protocols/database/repositories/employee/IEmployeeRepository';

import IUpdateEmployee from '@domain/useCases/employee/IUpdateEmployee';
import IUpdateEmployeeDTO from '@domain/useCases/employee/dtos/IUpdateEmployeeDTO';

export default class UpdateEmployee implements IUpdateEmployee {
  constructor(private readonly employeeRepository: IEmployeeRepository) {}

  public async update({ employee_alias_id, data }: IUpdateEmployeeDTO): Promise<Employee> {
    const foundEmployee = await this.employeeRepository.findByAliasId(employee_alias_id);

    if (!foundEmployee || !foundEmployee.is_active)
      throw new AppError({ message: 'Employee does not exists or is inactive', status_code: 400 });

    const { name } = data;

    const updatedEmployee = await this.employeeRepository.update({
      id: foundEmployee.id,
      data: { name },
    });

    if (!updatedEmployee)
      throw new AppError({ message: 'Could not update employee', status_code: 400 });

    return updatedEmployee;
  }
}
