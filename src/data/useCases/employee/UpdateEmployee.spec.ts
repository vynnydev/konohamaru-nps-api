import AppError from '@utils/errors/AppError';

import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import FakeEmployeeRepository from '@infra/database/repositories/employee/fakes/FakeEmployeeRepository';

import UpdateEmployee from '@data/useCases/employee/UpdateEmployee';

let fakeEmployeeRepository: FakeEmployeeRepository;

let updateEmployee: UpdateEmployee;

describe('UpdateEmployee', () => {
  beforeEach(() => {
    fakeEmployeeRepository = new FakeEmployeeRepository();

    updateEmployee = new UpdateEmployee(fakeEmployeeRepository);
  });

  it('should be able to call find employee by alias id with correct values', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const findByAliasId = jest.spyOn(fakeEmployeeRepository, 'findByAliasId');

    await updateEmployee.update({
      employee_alias_id: createdEmployee.alias_id,
      data: { name: 'any_name' },
    });

    expect(findByAliasId).toHaveBeenCalledWith(createdEmployee.alias_id);
  });

  it('should be able to throw if find employee by alias id throws', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    jest.spyOn(fakeEmployeeRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(
      updateEmployee.update({
        employee_alias_id: createdEmployee.alias_id,
        data: { name: 'any_name' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if employee does not exists', async () => {
    await expect(
      updateEmployee.update({
        employee_alias_id: 'any_alias_id',
        data: { name: 'any_name' },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call update employee with correct values', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const updateSpy = jest.spyOn(fakeEmployeeRepository, 'findByAliasId');

    await updateEmployee.update({
      employee_alias_id: createdEmployee.alias_id,
      data: { name: 'any_name' },
    });

    expect(updateSpy).toHaveBeenCalledWith(createdEmployee.alias_id);
  });

  it('should be able to throw if update employee throws', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    jest.spyOn(fakeEmployeeRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    expect(
      updateEmployee.update({
        employee_alias_id: createdEmployee.alias_id,
        data: { name: 'any_name' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if update return undefined', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    jest.spyOn(fakeEmployeeRepository, 'update').mockImplementationOnce(async () => undefined);

    await expect(
      updateEmployee.update({
        employee_alias_id: createdEmployee.alias_id,
        data: { name: 'any_name' },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to update employee', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(
      mockEmployeeModel.mock({ is_active: true }),
    );

    const updatedEmployee = await updateEmployee.update({
      employee_alias_id: createdEmployee.alias_id,
      data: { name: 'any_name' },
    });

    expect(updatedEmployee).toEqual({
      id: updatedEmployee.id,
      alias_id: updatedEmployee.alias_id,
      name: updatedEmployee.name,
      is_active: updatedEmployee.is_active,
      created_at: updatedEmployee.created_at,
      updated_at: updatedEmployee.updated_at,
    });
  });
});
