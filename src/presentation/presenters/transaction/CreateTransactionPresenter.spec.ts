import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import CreateTransactionPresenter from '@presentation/presenters/transaction/CreateTransactionPresenter';

let createTransactionPresenter: CreateTransactionPresenter;

describe('CreateTransactionPresenter', () => {
  beforeEach(() => {
    createTransactionPresenter = new CreateTransactionPresenter();
  });

  it('should be able to reply data on success', async () => {
    const transaction = mockTransactionModel.mock();

    const httpResponse = await createTransactionPresenter.reply({
      data: { transaction },
    });

    expect(httpResponse.status_code).toBe(201);
    expect(httpResponse.body).toEqual({
      status: 'success',
      transaction: {
        id: transaction.id,
        alias_id: transaction.alias_id,
        provider_date: transaction.provider_date,
        store_evaluation: transaction.store_evaluation,
        store_id: transaction.store_id,
        employee_id: transaction.employee_id,
        customer_id: transaction.customer_id,
        created_at: transaction.created_at,
        updated_at: transaction.updated_at,
      },
    });
  });
});
