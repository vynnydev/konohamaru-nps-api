import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import { IHttpResponse } from '@presentation/protocols/IHttp';
import IReplyDTO from '@presentation/protocols/dtos/IReplyDTO';
import IPresenter from '@presentation/protocols/IPresenter';

export default class FakeUpdateTransactionPresenter implements IPresenter {
  public async reply({ data }: IReplyDTO): Promise<IHttpResponse> {
    const transaction = mockTransactionModel.mock();

    return {
      status_code: 200,
      body: { status: 'success', transaction },
    };
  }
}
