import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import { IHttpResponse } from '@presentation/protocols/IHttp';
import IReplyDTO from '@presentation/protocols/dtos/IReplyDTO';
import IPresenter from '@presentation/protocols/IPresenter';

export default class FakeCreateTransactionPresenter implements IPresenter {
  public async reply({ data }: IReplyDTO): Promise<IHttpResponse> {
    const transaction = mockTransactionModel.mock();

    return {
      status_code: 201,
      body: { status: 'success', transaction },
    };
  }
}
