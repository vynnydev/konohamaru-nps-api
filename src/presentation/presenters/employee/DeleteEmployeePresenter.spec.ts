import DeleteEmployee from '@presentation/presenters/employee/DeleteEmployeePresenter';

let deleteEmployee: DeleteEmployee;

describe('DeleteEmployeePresenter', () => {
  beforeEach(() => {
    deleteEmployee = new DeleteEmployee();
  });

  it('should be able to reply data on success', async () => {
    const httpResponse = await deleteEmployee.reply({
      data: {},
    });

    expect(httpResponse.status_code).toBe(204);
    expect(httpResponse.body).toEqual({ status: 'success' });
  });
});
