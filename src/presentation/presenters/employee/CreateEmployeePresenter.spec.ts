import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import CreateEmployeePresenter from '@presentation/presenters/employee/CreateEmployeePresenter';

let createEmployeePresenter: CreateEmployeePresenter;

describe('CreateEmployeePresenter', () => {
  beforeEach(() => {
    createEmployeePresenter = new CreateEmployeePresenter();
  });

  it('should be able to reply data on success', async () => {
    const employee = mockEmployeeModel.mock();

    const httpResponse = await createEmployeePresenter.reply({
      data: { employee },
    });

    expect(httpResponse.status_code).toBe(201);
    expect(httpResponse.body).toEqual({
      status: 'success',
      employee: {
        id: employee.id,
        alias_id: employee.alias_id,
        name: employee.name,
        is_active: employee.is_active,
        created_at: employee.created_at,
        updated_at: employee.updated_at,
      },
    });
  });
});
