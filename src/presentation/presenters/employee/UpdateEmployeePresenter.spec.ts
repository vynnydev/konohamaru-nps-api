import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import UpdateEmployeePresenter from '@presentation/presenters/employee/UpdateEmployeePresenter';

let updateEmployeePresenter: UpdateEmployeePresenter;

describe('UpdateEmployeePresenter', () => {
  beforeEach(() => {
    updateEmployeePresenter = new UpdateEmployeePresenter();
  });

  it('should be able to reply data on success', async () => {
    const employee = mockEmployeeModel.mock();

    const httpResponse = await updateEmployeePresenter.reply({
      data: { employee },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({
      status: 'success',
      employee: {
        id: employee.id,
        alias_id: employee.alias_id,
        name: employee.name,
        is_active: employee.is_active,
        created_at: employee.created_at,
        updated_at: employee.updated_at,
      },
    });
  });
});
