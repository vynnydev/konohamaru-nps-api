import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import FindEmployeesPresenter from '@presentation/presenters/employee/FindEmployeesPresenter';

let findEmployeesPresenter: FindEmployeesPresenter;

describe('FindEmployeesPresenter', () => {
  beforeEach(() => {
    findEmployeesPresenter = new FindEmployeesPresenter();
  });

  it('should be able to reply data on success', async () => {
    const Employee = mockEmployeeModel.mock();

    const httpResponse = await findEmployeesPresenter.reply({
      data: { employees: [Employee] },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({
      status: 'success',
      employees: [
        {
          id: Employee.id,
          alias_id: Employee.alias_id,
          name: Employee.name,
          is_active: Employee.is_active,
          created_at: Employee.created_at,
          updated_at: Employee.updated_at,
        },
      ],
    });
  });
});
