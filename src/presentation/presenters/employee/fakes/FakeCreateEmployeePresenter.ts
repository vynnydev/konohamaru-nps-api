import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import { IHttpResponse } from '@presentation/protocols/IHttp';
import IReplyDTO from '@presentation/protocols/dtos/IReplyDTO';
import IPresenter from '@presentation/protocols/IPresenter';

export default class FakeCreateEmployeePresenter implements IPresenter {
  public async reply({ data }: IReplyDTO): Promise<IHttpResponse> {
    const employee = mockEmployeeModel.mock();

    return {
      status_code: 201,
      body: { status: 'success', employee },
    };
  }
}
