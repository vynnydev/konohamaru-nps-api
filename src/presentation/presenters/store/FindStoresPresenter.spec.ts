import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import FindStoresPresenter from '@presentation/presenters/store/FindStoresPresenter';

let findStoresPresenter: FindStoresPresenter;

describe('FindStoresPresenter', () => {
  beforeEach(() => {
    findStoresPresenter = new FindStoresPresenter();
  });

  it('should be able to reply data on success', async () => {
    const store = mockStoreModel.mock();

    const httpResponse = await findStoresPresenter.reply({
      data: { stores: [store] },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({
      status: 'success',
      stores: [
        {
          id: store.id,
          alias_id: store.alias_id,
          name: store.name,
          created_at: store.created_at,
          updated_at: store.updated_at,
        },
      ],
    });
  });
});
