import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import { IHttpResponse } from '@presentation/protocols/IHttp';
import IReplyDTO from '@presentation/protocols/dtos/IReplyDTO';
import IPresenter from '@presentation/protocols/IPresenter';

export default class FakeCreateStorePresenter implements IPresenter {
  public async reply({ data }: IReplyDTO): Promise<IHttpResponse> {
    const store = mockStoreModel.mock();

    return {
      status_code: 201,
      body: { status: 'success', store },
    };
  }
}
