import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import UpdateStorePresenter from '@presentation/presenters/store/UpdateStorePresenter';

let updateStorePresenter: UpdateStorePresenter;

describe('UpdateStorePresenter', () => {
  beforeEach(() => {
    updateStorePresenter = new UpdateStorePresenter();
  });

  it('should be able to reply data on success', async () => {
    const store = mockStoreModel.mock();

    const httpResponse = await updateStorePresenter.reply({
      data: { store },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({
      status: 'success',
      store: {
        id: store.id,
        alias_id: store.alias_id,
        name: store.name,
        created_at: store.created_at,
        updated_at: store.updated_at,
      },
    });
  });
});
