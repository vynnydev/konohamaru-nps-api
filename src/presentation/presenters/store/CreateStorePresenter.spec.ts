import mockStoreModel from '@tests/domain/mocks/MockStoreModel';

import CreateStorePresenter from '@presentation/presenters/store/CreateStorePresenter';

let createStorePresenter: CreateStorePresenter;

describe('CreateStorePresenter', () => {
  beforeEach(() => {
    createStorePresenter = new CreateStorePresenter();
  });

  it('should be able to reply data on success', async () => {
    const store = mockStoreModel.mock();

    const httpResponse = await createStorePresenter.reply({
      data: { store },
    });

    expect(httpResponse.status_code).toBe(201);
    expect(httpResponse.body).toEqual({
      status: 'success',
      store: {
        id: store.id,
        alias_id: store.alias_id,
        name: store.name,
        created_at: store.created_at,
        updated_at: store.updated_at,
      },
    });
  });
});
