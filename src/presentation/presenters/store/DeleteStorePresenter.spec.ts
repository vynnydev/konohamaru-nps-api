import DeleteStore from '@presentation/presenters/store/DeleteStorePresenter';

let deleteStore: DeleteStore;

describe('DeleteStorePresenter', () => {
  beforeEach(() => {
    deleteStore = new DeleteStore();
  });

  it('should be able to reply data on success', async () => {
    const httpResponse = await deleteStore.reply({
      data: {},
    });

    expect(httpResponse.status_code).toBe(204);
    expect(httpResponse.body).toEqual({ status: 'success' });
  });
});
