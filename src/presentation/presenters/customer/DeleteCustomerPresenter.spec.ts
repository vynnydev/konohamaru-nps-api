import DeleteCustomer from '@presentation/presenters/customer/DeleteCustomerPresenter';

let deleteCustomer: DeleteCustomer;

describe('DeleteCustomerPresenter', () => {
  beforeEach(() => {
    deleteCustomer = new DeleteCustomer();
  });

  it('should be able to reply data on success', async () => {
    const httpResponse = await deleteCustomer.reply({
      data: {},
    });

    expect(httpResponse.status_code).toBe(204);
    expect(httpResponse.body).toEqual({ status: 'success' });
  });
});
