import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import CreateCustomerPresenter from '@presentation/presenters/customer/CreateCustomerPresenter';

let createCustomerPresenter: CreateCustomerPresenter;

describe('CreateCustomerPresenter', () => {
  beforeEach(() => {
    createCustomerPresenter = new CreateCustomerPresenter();
  });

  it('should be able to reply data on success', async () => {
    const customer = mockCustomerModel.mock();

    const httpResponse = await createCustomerPresenter.reply({
      data: { customer },
    });

    expect(httpResponse.status_code).toBe(201);
    expect(httpResponse.body).toEqual({
      status: 'success',
      customer: {
        id: customer.id,
        alias_id: customer.alias_id,
        name: customer.name,
        email: customer.email,
        cpf: customer.cpf,
        phone_number: customer.phone_number,
        created_at: customer.created_at,
        updated_at: customer.updated_at,
      },
    });
  });
});
