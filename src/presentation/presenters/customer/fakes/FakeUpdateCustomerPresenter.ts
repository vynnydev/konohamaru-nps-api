import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import { IHttpResponse } from '@presentation/protocols/IHttp';
import IReplyDTO from '@presentation/protocols/dtos/IReplyDTO';
import IPresenter from '@presentation/protocols/IPresenter';

export default class FakeUpdateCustomerPresenter implements IPresenter {
  public async reply({ data }: IReplyDTO): Promise<IHttpResponse> {
    const customer = mockCustomerModel.mock();

    return {
      status_code: 200,
      body: { status: 'success', customer },
    };
  }
}
