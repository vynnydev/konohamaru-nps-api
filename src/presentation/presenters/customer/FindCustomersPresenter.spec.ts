import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';

import FindCustomersPresenter from '@presentation/presenters/customer/FindCustomersPresenter';

let findCustomersPresenter: FindCustomersPresenter;

describe('FindCustomersPresenter', () => {
  beforeEach(() => {
    findCustomersPresenter = new FindCustomersPresenter();
  });

  it('should be able to reply data on success', async () => {
    const customer = mockCustomerModel.mock();

    const httpResponse = await findCustomersPresenter.reply({
      data: { customers: [customer] },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({
      status: 'success',
      customers: [
        {
          id: customer.id,
          alias_id: customer.alias_id,
          name: customer.name,
          email: customer.email,
          cpf: customer.cpf,
          phone_number: customer.phone_number,
          created_at: customer.created_at,
          updated_at: customer.updated_at,
        },
      ],
    });
  });
});
