import mockEvaluationModel from '@tests/domain/mocks/MockEvaluationModel';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import FindEvaluationsPresenter from '@presentation/presenters/evaluation/FindEvaluationsPresenter';

let findEvaluationsPresenter: FindEvaluationsPresenter;

describe('FindEvaluationsPresenter', () => {
  beforeEach(() => {
    findEvaluationsPresenter = new FindEvaluationsPresenter();
  });

  it('should be able to reply data on success', async () => {
    const evaluation = mockEvaluationModel.mock();
    const transaction = mockTransactionModel.mock();

    const evaluations = [{ ...evaluation, transaction: { ...transaction } }];

    const httpResponse = await findEvaluationsPresenter.reply({
      data: {
        evaluations,
        total_pages: 1,
      },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({
      status: 'success',
      evaluations: [
        {
          id: evaluation.id,
          alias_id: evaluation.alias_id,
          description: evaluation.description,
          transaction_value: evaluation.transaction_value,
          transaction_id: evaluation.transaction_id,
          transaction: {
            id: transaction.id,
            alias_id: transaction.alias_id,
            provider_date: transaction.provider_date,
            store_evaluation: transaction.store_evaluation,
            store_id: transaction.store_id,
            employee_id: transaction.employee_id,
            customer_id: transaction.customer_id,
            created_at: transaction.created_at,
            updated_at: transaction.updated_at,
          },
          created_at: evaluation.created_at,
          updated_at: evaluation.updated_at,
        },
      ],
      total_pages: 1,
    });
  });
});
