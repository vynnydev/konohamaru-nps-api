import mockEvaluationModel from '@tests/domain/mocks/MockEvaluationModel';

import CreateEvaluationPresenter from '@presentation/presenters/evaluation/CreateEvaluationPresenter';

let createEvaluationPresenter: CreateEvaluationPresenter;

describe('CreateEvaluationPresenter', () => {
  beforeEach(() => {
    createEvaluationPresenter = new CreateEvaluationPresenter();
  });

  it('should be able to reply data on success', async () => {
    const evaluation = mockEvaluationModel.mock();

    const httpResponse = await createEvaluationPresenter.reply({
      data: { evaluation },
    });

    expect(httpResponse.status_code).toBe(201);
    expect(httpResponse.body).toEqual({
      status: 'success',
      evaluation: {
        id: evaluation.id,
        alias_id: evaluation.alias_id,
        description: evaluation.description,
        transaction_value: evaluation.transaction_value,
        transaction_id: evaluation.transaction_id,
        created_at: evaluation.created_at,
        updated_at: evaluation.updated_at,
      },
    });
  });
});
