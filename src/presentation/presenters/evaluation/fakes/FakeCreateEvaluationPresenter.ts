import mockEvaluationModel from '@tests/domain/mocks/MockEvaluationModel';

import { IHttpResponse } from '@presentation/protocols/IHttp';
import IReplyDTO from '@presentation/protocols/dtos/IReplyDTO';
import IPresenter from '@presentation/protocols/IPresenter';

export default class FakeCreateEvaluationPresenter implements IPresenter {
  public async reply({ data }: IReplyDTO): Promise<IHttpResponse> {
    const evaluation = mockEvaluationModel.mock();

    return {
      status_code: 201,
      body: { status: 'success', evaluation },
    };
  }
}
