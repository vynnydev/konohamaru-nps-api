import mockEvaluationModel from '@tests/domain/mocks/MockEvaluationModel';

import { IHttpResponse } from '@presentation/protocols/IHttp';
import IReplyDTO from '@presentation/protocols/dtos/IReplyDTO';
import IPresenter from '@presentation/protocols/IPresenter';

export default class FakeFindEvaluations implements IPresenter {
  public async reply({ data }: IReplyDTO): Promise<IHttpResponse> {
    const evaluation = mockEvaluationModel.mock();

    return {
      status_code: 200,
      body: { status: 'success', total_pages: 1, evaluations: [evaluation] },
    };
  }
}
