import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IPresenter from '@presentation/protocols/IPresenter';
import IController from '@presentation/protocols/IController';
import IValidation from '@presentation/protocols/IValidation';

import ICreateTransaction from '@domain/useCases/transaction/ICreateTransaction';

export default class CreateTransactionController implements IController {
  constructor(
    private readonly createTransaction: ICreateTransaction,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ params, body }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(body);

    const { store_alias_id, employee_alias_id, customer_alias_id } = params;
    const { provider_date, store_evaluation } = body;

    const createdTransaction = await this.createTransaction.create({
      provider_date,
      store_evaluation,
      store_alias_id,
      employee_alias_id,
      customer_alias_id,
    });

    return this.presenter.reply({ data: { transaction: createdTransaction } });
  }
}
