import CreateTransactionController from '@presentation/controllers/transaction/CreateTransactionController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import FakeProviderDateValidation from '@utils/validation/validators/transaction/fakes/FakeProviderDateValidation';
import FakeStoreEvaluationValidation from '@utils/validation/validators/transaction/fakes/FakeStoreEvaluationValidation';

import FakeCreateTransaction from '@data/useCases/transaction/fakes/FakeCreateTransaction';
import FakeCreateTransactionPresenter from '@presentation/presenters/transaction/fakes/FakeCreateTransactionPresenter';

let createTransactionController: CreateTransactionController;

let validationComposite: ValidationComposite;

let fakeProviderDateValidation: FakeProviderDateValidation;
let fakeStoreEvaluationValidation: FakeStoreEvaluationValidation;

let fakeCreateTransaction: FakeCreateTransaction;
let fakeCreateTransactionPresenter: FakeCreateTransactionPresenter;

describe('CreateTransactionController', () => {
  beforeEach(() => {
    fakeProviderDateValidation = new FakeProviderDateValidation();
    fakeStoreEvaluationValidation = new FakeStoreEvaluationValidation();

    fakeCreateTransaction = new FakeCreateTransaction();
    fakeCreateTransactionPresenter = new FakeCreateTransactionPresenter();

    validationComposite = new ValidationComposite([
      fakeProviderDateValidation,
      fakeStoreEvaluationValidation,
    ]);

    createTransactionController = new CreateTransactionController(
      fakeCreateTransaction,
      validationComposite,
      fakeCreateTransactionPresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await createTransactionController.handle({
      body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
      params: {
        store_alias_id: 'any_alias_id',
        employee_alias_id: 'any_alias_id',
        customer_alias_id: 'any_alias_id',
      },
    });

    expect(validateSpy).toHaveBeenCalledWith({
      provider_date: new Date(2022, 3, 25),
      store_evaluation: EStoreEvaluation.BOM,
    });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createTransactionController.handle({
        body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
        params: {
          store_alias_id: 'any_alias_id',
          employee_alias_id: 'any_alias_id',
          customer_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call create transaction with correct values', async () => {
    const createSpy = jest.spyOn(fakeCreateTransaction, 'create');

    await createTransactionController.handle({
      body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
      params: {
        store_alias_id: 'any_alias_id',
        employee_alias_id: 'any_alias_id',
        customer_alias_id: 'any_alias_id',
      },
    });

    expect(createSpy).toHaveBeenCalledWith({
      provider_date: new Date(2022, 3, 25),
      store_evaluation: EStoreEvaluation.BOM,
      store_alias_id: 'any_alias_id',
      employee_alias_id: 'any_alias_id',
      customer_alias_id: 'any_alias_id',
    });
  });

  it('should be able to throw if create transaction throws', async () => {
    jest.spyOn(fakeCreateTransaction, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createTransactionController.handle({
        body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
        params: {
          store_alias_id: 'any_alias_id',
          employee_alias_id: 'any_alias_id',
          customer_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeCreateTransactionPresenter, 'reply');

    await createTransactionController.handle({
      body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
      params: {
        store_alias_id: 'any_alias_id',
        employee_alias_id: 'any_alias_id',
        customer_alias_id: 'any_alias_id',
      },
    });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        transaction: {
          id: expect.any(String),
          alias_id: expect.any(String),
          provider_date: expect.any(Date),
          store_evaluation: expect.any(String),
          store_id: expect.any(String),
          employee_id: expect.any(String),
          customer_id: expect.any(String),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeCreateTransactionPresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createTransactionController.handle({
        body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
        params: {
          store_alias_id: 'any_alias_id',
          employee_alias_id: 'any_alias_id',
          customer_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });
});
