import UpdateTransactionController from '@presentation/controllers/transaction/UpdateTransactionController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import FakeProviderDateValidation from '@utils/validation/validators/transaction/fakes/FakeProviderDateValidation';
import FakeStoreEvaluationValidation from '@utils/validation/validators/transaction/fakes/FakeStoreEvaluationValidation';

import FakeUpdateTransaction from '@data/useCases/transaction/fakes/FakeUpdateTransaction';
import FakeUpdateTransactionPresenter from '@presentation/presenters/transaction/fakes/FakeUpdateTransactionPresenter';

let updateTransactionController: UpdateTransactionController;

let validationComposite: ValidationComposite;

let fakeProviderDateValidation: FakeProviderDateValidation;
let fakeStoreEvaluationValidation: FakeStoreEvaluationValidation;

let fakeUpdateTransaction: FakeUpdateTransaction;
let fakeUpdateTransactionPresenter: FakeUpdateTransactionPresenter;

describe('UpdateTransactionController', () => {
  beforeEach(() => {
    fakeProviderDateValidation = new FakeProviderDateValidation();
    fakeStoreEvaluationValidation = new FakeStoreEvaluationValidation();

    fakeUpdateTransaction = new FakeUpdateTransaction();
    fakeUpdateTransactionPresenter = new FakeUpdateTransactionPresenter();
    validationComposite = new ValidationComposite([
      fakeProviderDateValidation,
      fakeStoreEvaluationValidation,
    ]);

    updateTransactionController = new UpdateTransactionController(
      fakeUpdateTransaction,
      validationComposite,
      fakeUpdateTransactionPresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await updateTransactionController.handle({
      body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
      params: { transaction_alias_id: 'any_alias_id' },
    });

    expect(validateSpy).toHaveBeenCalledWith({
      provider_date: new Date(2022, 3, 25),
      store_evaluation: EStoreEvaluation.BOM,
    });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateTransactionController.handle({
        body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call update with correct values', async () => {
    const updateSpy = jest.spyOn(fakeUpdateTransaction, 'update');

    await updateTransactionController.handle({
      body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
      params: { transaction_alias_id: 'any_alias_id' },
    });

    expect(updateSpy).toHaveBeenCalledWith({
      transaction_alias_id: 'any_alias_id',
      data: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
    });
  });

  it('should be able to throw if update throws', async () => {
    jest.spyOn(fakeUpdateTransaction, 'update').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateTransactionController.handle({
        body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeUpdateTransactionPresenter, 'reply');

    await updateTransactionController.handle({
      body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
      params: { transaction_alias_id: 'any_alias_id' },
    });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        transaction: {
          id: expect.any(String),
          alias_id: expect.any(String),
          provider_date: expect.any(Date),
          store_evaluation: expect.any(String),
          store_id: expect.any(String),
          employee_id: expect.any(String),
          customer_id: expect.any(String),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeUpdateTransactionPresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateTransactionController.handle({
        body: { provider_date: new Date(2022, 3, 25), store_evaluation: EStoreEvaluation.BOM },
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });
});
