import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IController from '@presentation/protocols/IController';
import IPresenter from '@presentation/protocols/IPresenter';
import IValidation from '@presentation/protocols/IValidation';

import IUpdateTransaction from '@domain/useCases/transaction/IUpdateTransaction';

export default class UpdateTransactionController implements IController {
  constructor(
    private readonly updateTransaction: IUpdateTransaction,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ params, body }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(body);

    const { transaction_alias_id } = params;

    const { provider_date, store_evaluation } = body;

    const updatedTransaction = await this.updateTransaction.update({
      transaction_alias_id,
      data: { provider_date, store_evaluation },
    });

    return this.presenter.reply({ data: { transaction: updatedTransaction } });
  }
}
