import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IPresenter from '@presentation/protocols/IPresenter';
import IController from '@presentation/protocols/IController';
import IValidation from '@presentation/protocols/IValidation';

import ICreateStore from '@domain/useCases/store/ICreateStore';

export default class CreateStoreController implements IController {
  constructor(
    private readonly createStore: ICreateStore,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ body }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(body);

    const { name } = body;

    const createdStore = await this.createStore.create(name);

    return this.presenter.reply({ data: { store: createdStore } });
  }
}
