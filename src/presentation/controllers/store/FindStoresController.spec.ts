import FindStoresController from '@presentation/controllers/store/FindStoresController';

import FakeFindStores from '@data/useCases/store/fakes/FakeFindStores';
import FakeFindStoresPresenter from '@presentation/presenters/store/fakes/FakeFindStoresPresenter';

let findStoresController: FindStoresController;

let fakeFindStores: FakeFindStores;
let fakeFindStoresPresenter: FakeFindStoresPresenter;

describe('FindStoresController', () => {
  beforeEach(() => {
    fakeFindStores = new FakeFindStores();

    fakeFindStoresPresenter = new FakeFindStoresPresenter();

    findStoresController = new FindStoresController(fakeFindStores, fakeFindStoresPresenter);
  });

  it('should be able to handle find stores', async () => {
    const httpResponse = await findStoresController.handle();

    expect(httpResponse.status_code).toEqual(200);
    expect(httpResponse.body).toEqual({
      status: 'success',
      stores: [
        {
          id: expect.any(String),
          alias_id: expect.any(String),
          name: expect.any(String),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      ],
    });
  });
});
