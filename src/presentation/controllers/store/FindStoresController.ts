import { IHttpResponse } from '@presentation/protocols/IHttp';
import IPresenter from '@presentation/protocols/IPresenter';
import IController from '@presentation/protocols/IController';

import IFindStores from '@domain/useCases/store/IFindStores';

export default class FindStoresController implements IController {
  constructor(private readonly findStores: IFindStores, private readonly presenter: IPresenter) {}

  public async handle(): Promise<IHttpResponse> {
    const foundStores = await this.findStores.find();

    return this.presenter.reply({ data: { stores: foundStores } });
  }
}
