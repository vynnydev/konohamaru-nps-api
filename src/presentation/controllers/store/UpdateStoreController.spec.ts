import UpdateStoreController from '@presentation/controllers/store/UpdateStoreController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import FakeNameValidation from '@utils/validation/validators/common/fakes/FakeNameValidation';

import FakeUpdateStore from '@data/useCases/store/fakes/FakeUpdateStore';
import FakeUpdateStorePresenter from '@presentation/presenters/store/fakes/FakeUpdateStorePresenter';

let updateStoreController: UpdateStoreController;

let validationComposite: ValidationComposite;

let fakeNameValidation: FakeNameValidation;

let fakeUpdateStore: FakeUpdateStore;
let fakeUpdateStorePresenter: FakeUpdateStorePresenter;

describe('UpdateStoreController', () => {
  beforeEach(() => {
    fakeNameValidation = new FakeNameValidation();

    fakeUpdateStore = new FakeUpdateStore();
    fakeUpdateStorePresenter = new FakeUpdateStorePresenter();
    validationComposite = new ValidationComposite([fakeNameValidation]);

    updateStoreController = new UpdateStoreController(
      fakeUpdateStore,
      validationComposite,
      fakeUpdateStorePresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await updateStoreController.handle({
      body: { name: 'any name' },
      params: { store_alias_id: 'any_alias' },
    });

    expect(validateSpy).toHaveBeenCalledWith({ name: 'any name' });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateStoreController.handle({
        body: { name: 'any name' },
        params: { store_alias_id: 'any_alias' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call update with correct values', async () => {
    const updateSpy = jest.spyOn(fakeUpdateStore, 'update');

    await updateStoreController.handle({
      body: { name: 'any name' },
      params: { store_alias_id: 'any_alias' },
    });

    expect(updateSpy).toHaveBeenCalledWith({
      store_alias_id: 'any_alias',
      data: { name: 'any name' },
    });
  });

  it('should be able to throw if update throws', async () => {
    jest.spyOn(fakeUpdateStore, 'update').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateStoreController.handle({
        body: { name: 'any name' },
        params: { store_alias_id: 'any_alias' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeUpdateStorePresenter, 'reply');

    await updateStoreController.handle({
      body: { name: 'any name' },
      params: { store_alias_id: 'any_alias' },
    });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        store: {
          id: expect.any(String),
          alias_id: expect.any(String),
          name: expect.any(String),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeUpdateStorePresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateStoreController.handle({
        body: { name: 'any name' },
        params: { store_alias_id: 'any_alias' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });
});
