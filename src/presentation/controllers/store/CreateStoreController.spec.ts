import CreateStoreController from '@presentation/controllers/store/CreateStoreController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import FakeNameValidation from '@utils/validation/validators/common/fakes/FakeNameValidation';

import FakeCreateStore from '@data/useCases/store/fakes/FakeCreateStore';
import FakeCreateStorePresenter from '@presentation/presenters/store/fakes/FakeCreateStorePresenter';

let createStoreController: CreateStoreController;

let validationComposite: ValidationComposite;

let fakeNameValidation: FakeNameValidation;

let fakeCreateStore: FakeCreateStore;
let fakeCreateStorePresenter: FakeCreateStorePresenter;

describe('CreateStoreController', () => {
  beforeEach(() => {
    fakeNameValidation = new FakeNameValidation();

    fakeCreateStore = new FakeCreateStore();
    fakeCreateStorePresenter = new FakeCreateStorePresenter();

    validationComposite = new ValidationComposite([fakeNameValidation]);

    createStoreController = new CreateStoreController(
      fakeCreateStore,
      validationComposite,
      fakeCreateStorePresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await createStoreController.handle({
      body: { name: 'any_name' },
    });

    expect(validateSpy).toHaveBeenCalledWith({ name: 'any_name' });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createStoreController.handle({ body: { name: 'any_name' } }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call create store with correct values', async () => {
    const createSpy = jest.spyOn(fakeCreateStore, 'create');

    await createStoreController.handle({ body: { name: 'any_name' } });

    expect(createSpy).toHaveBeenCalledWith('any_name');
  });

  it('should be able to throw if create store throws', async () => {
    jest.spyOn(fakeCreateStore, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createStoreController.handle({ body: { name: 'any_name' } }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeCreateStorePresenter, 'reply');

    await createStoreController.handle({ body: { name: 'any_name' } });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        store: {
          id: expect.any(String),
          alias_id: expect.any(String),
          name: expect.any(String),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeCreateStorePresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createStoreController.handle({ body: { name: 'any_name' } }),
    ).rejects.toBeInstanceOf(Error);
  });
});
