import DeleteStoreController from '@presentation/controllers/store/DeleteStoreController';

import FakeDeleteStore from '@data/useCases/store/fakes/FakeDeleteStore';
import FakeDeleteStorePresenter from '@presentation/presenters/store/fakes/FakeDeleteStorePresenter';

let deleteStoreController: DeleteStoreController;

let fakeDeleteStore: FakeDeleteStore;
let fakeDeleteStorePresenter: FakeDeleteStorePresenter;

describe('DeleteStoreController', () => {
  beforeEach(() => {
    fakeDeleteStore = new FakeDeleteStore();
    fakeDeleteStorePresenter = new FakeDeleteStorePresenter();

    deleteStoreController = new DeleteStoreController(fakeDeleteStore, fakeDeleteStorePresenter);
  });

  it('should be able to call delete store with correct values', async () => {
    const deleteSpy = jest.spyOn(fakeDeleteStore, 'delete');

    await deleteStoreController.handle({ params: { store_alias_id: 'any_alias_id' } });

    expect(deleteSpy).toHaveBeenCalledWith('any_alias_id');
  });

  it('should be able to throw if delete store throws', async () => {
    jest.spyOn(fakeDeleteStore, 'delete').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      deleteStoreController.handle({ params: { store_alias_id: 'any_alias_id' } }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeDeleteStorePresenter, 'reply');

    await deleteStoreController.handle({ params: { store_alias_id: 'any_alias_id' } });

    expect(replySpy).toHaveBeenCalledWith({ data: {} });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeDeleteStorePresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      deleteStoreController.handle({ params: { store_alias_id: 'any_alias_id' } }),
    ).rejects.toBeInstanceOf(Error);
  });
});
