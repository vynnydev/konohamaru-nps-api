import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IController from '@presentation/protocols/IController';
import IPresenter from '@presentation/protocols/IPresenter';

import IDeleteStore from '@domain/useCases/store/IDeleteStore';

export default class DeleteStoreController implements IController {
  constructor(private readonly deleteStore: IDeleteStore, private readonly presenter: IPresenter) {}

  public async handle({ params }: IHttpRequest): Promise<IHttpResponse> {
    const { store_alias_id } = params;

    await this.deleteStore.delete(store_alias_id);

    return this.presenter.reply({ data: {} });
  }
}
