import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IController from '@presentation/protocols/IController';
import IPresenter from '@presentation/protocols/IPresenter';
import IValidation from '@presentation/protocols/IValidation';

import IUpdateStore from '@domain/useCases/store/IUpdateStore';

export default class UpdateStoreController implements IController {
  constructor(
    private readonly updateStore: IUpdateStore,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ params, body }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(body);

    const { store_alias_id } = params;

    const { name } = body;

    const updatedStore = await this.updateStore.update({
      store_alias_id,
      data: { name },
    });

    return this.presenter.reply({ data: { store: updatedStore } });
  }
}
