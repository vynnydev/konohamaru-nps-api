import FindEvaluationsController from '@presentation/controllers/evaluation/FindEvaluationsController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import FakeFindEvaluationsPresenter from '@presentation/presenters/evaluation/fakes/FakeFindEvaluationsPresenter';
import FakeFindEvaluations from '@data/useCases/evaluation/fakes/FakeFindEvaluations';

import FakeLimitValidation from '@utils/validation/validators/common/fakes/FakeLimitValidation';
import FakePageValidation from '@utils/validation/validators/common/fakes/FakePageValidation';

let findEvaluationsController: FindEvaluationsController;

let validationComposite: ValidationComposite;

let fakeLimitValidation: FakeLimitValidation;
let fakePageValidation: FakePageValidation;

let fakeFindEvaluations: FakeFindEvaluations;
let fakeFindEvaluationsPresenter: FakeFindEvaluationsPresenter;

describe('FindEvaluationsController', () => {
  beforeEach(() => {
    fakeLimitValidation = new FakeLimitValidation();
    fakePageValidation = new FakePageValidation();

    fakeFindEvaluations = new FakeFindEvaluations();
    fakeFindEvaluationsPresenter = new FakeFindEvaluationsPresenter();

    validationComposite = new ValidationComposite([fakeLimitValidation, fakePageValidation]);

    findEvaluationsController = new FindEvaluationsController(
      fakeFindEvaluations,
      validationComposite,
      fakeFindEvaluationsPresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await findEvaluationsController.handle({
      query: { limit: 10, page: 1 },
      params: { transaction_alias_id: 'any_alias_id' },
    });

    expect(validateSpy).toHaveBeenCalledWith({ limit: 10, page: 1 });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      findEvaluationsController.handle({
        query: { limit: 10, page: 1 },
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call find evaluation with correct values', async () => {
    const findEvaluationsSpy = jest.spyOn(fakeFindEvaluations, 'find');

    await findEvaluationsController.handle({
      query: { limit: 10, page: 1 },
      params: { transaction_alias_id: 'any_alias_id' },
    });

    expect(findEvaluationsSpy).toHaveBeenCalledWith({
      limit: 10,
      page: 1,
      transaction_alias_id: 'any_alias_id',
    });
  });

  it('should be able to throw if find evaluation throws', async () => {
    jest.spyOn(fakeFindEvaluations, 'find').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      findEvaluationsController.handle({
        query: { limit: 10, page: 1 },
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeFindEvaluationsPresenter, 'reply');

    await findEvaluationsController.handle({
      query: { limit: 10, page: 1 },
      params: { transaction_alias_id: 'any_alias_id' },
    });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        evaluations: [
          {
            id: expect.any(String),
            alias_id: expect.any(String),
            description: expect.any(String),
            transaction_value: expect.any(Number),
            transaction_id: expect.any(String),
            transaction: {
              id: expect.any(String),
              alias_id: expect.any(String),
              provider_date: expect.any(Date),
              store_evaluation: expect.any(String),
              store_id: expect.any(String),
              employee_id: expect.any(String),
              customer_id: expect.any(String),
              created_at: expect.any(Date),
              updated_at: expect.any(Date),
            },
            created_at: expect.any(Date),
            updated_at: expect.any(Date),
          },
        ],
        total_pages: 1,
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeFindEvaluationsPresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      findEvaluationsController.handle({
        query: { limit: 10, page: 1 },
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });
});
