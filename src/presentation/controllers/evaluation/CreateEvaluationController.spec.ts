import CreateEvaluationController from '@presentation/controllers/evaluation/CreateEvaluationController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import FakeDescriptionValidation from '@utils/validation/validators/common/fakes/FakeDescriptionValidation';
import FakeTransactionValueValidation from '@utils/validation/validators/evaluation/fakes/FakeTransactionValueValidation';

import FakeCreateEvaluation from '@data/useCases/evaluation/fakes/FakeCreateEvaluation';
import FakeCreateEvaluationPresenter from '@presentation/presenters/evaluation/fakes/FakeCreateEvaluationPresenter';

let createEvaluationController: CreateEvaluationController;

let validationComposite: ValidationComposite;

let fakeDescriptionValidation: FakeDescriptionValidation;
let fakeTransactionValueValidation: FakeTransactionValueValidation;

let fakeCreateEvaluation: FakeCreateEvaluation;
let fakeCreateEvaluationPresenter: FakeCreateEvaluationPresenter;

describe('CreateEvaluationController', () => {
  beforeEach(() => {
    fakeDescriptionValidation = new FakeDescriptionValidation();
    fakeTransactionValueValidation = new FakeTransactionValueValidation();

    fakeCreateEvaluation = new FakeCreateEvaluation();
    fakeCreateEvaluationPresenter = new FakeCreateEvaluationPresenter();

    validationComposite = new ValidationComposite([
      fakeDescriptionValidation,
      fakeTransactionValueValidation,
    ]);

    createEvaluationController = new CreateEvaluationController(
      fakeCreateEvaluation,
      validationComposite,
      fakeCreateEvaluationPresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await createEvaluationController.handle({
      body: { description: 'any_description', transaction_value: 10 },
      params: { transaction_alias_id: 'any_alias_id' },
    });

    expect(validateSpy).toHaveBeenCalledWith({
      description: 'any_description',
      transaction_value: 10,
    });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createEvaluationController.handle({
        body: { description: 'any_description', transaction_value: 10 },
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call create evaluation with correct values', async () => {
    const createSpy = jest.spyOn(fakeCreateEvaluation, 'create');

    await createEvaluationController.handle({
      body: { description: 'any_description', transaction_value: 10 },
      params: { transaction_alias_id: 'any_alias_id' },
    });

    expect(createSpy).toHaveBeenCalledWith({
      description: 'any_description',
      transaction_value: 10,
      transaction_alias_id: 'any_alias_id',
    });
  });

  it('should be able to throw if create evaluation throws', async () => {
    jest.spyOn(fakeCreateEvaluation, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createEvaluationController.handle({
        body: { description: 'any_description', transaction_value: 10 },
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeCreateEvaluationPresenter, 'reply');

    await createEvaluationController.handle({
      body: { description: 'any_description', transaction_value: 10 },
      params: { transaction_alias_id: 'any_alias_id' },
    });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        evaluation: {
          id: expect.any(String),
          alias_id: expect.any(String),
          description: expect.any(String),
          transaction_value: expect.any(Number),
          transaction_id: expect.any(String),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeCreateEvaluationPresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createEvaluationController.handle({
        body: { description: 'any_description', transaction_value: 10 },
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });
});
