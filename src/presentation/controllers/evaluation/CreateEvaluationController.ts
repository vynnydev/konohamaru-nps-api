import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IPresenter from '@presentation/protocols/IPresenter';
import IController from '@presentation/protocols/IController';
import IValidation from '@presentation/protocols/IValidation';

import ICreateEvaluation from '@domain/useCases/evaluation/ICreateEvaluation';

export default class CreateEvaluationController implements IController {
  constructor(
    private readonly createEvaluation: ICreateEvaluation,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ params, body }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(body);

    const { transaction_alias_id } = params;
    const { description, transaction_value } = body;

    const createdEvaluation = await this.createEvaluation.create({
      description,
      transaction_value,
      transaction_alias_id,
    });

    return this.presenter.reply({ data: { evaluation: createdEvaluation } });
  }
}
