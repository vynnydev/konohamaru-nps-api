import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IController from '@presentation/protocols/IController';
import IPresenter from '@presentation/protocols/IPresenter';
import IValidation from '@presentation/protocols/IValidation';

import IFindEvaluations from '@domain/useCases/evaluation/IFindEvaluations';

export default class FindEvaluationsController implements IController {
  constructor(
    private readonly findEvaluations: IFindEvaluations,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ params, query }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(query);

    const { transaction_alias_id } = params;
    const { limit, page } = query;

    const { evaluations, total_pages } = await this.findEvaluations.find({
      limit,
      page,
      transaction_alias_id,
    });

    return this.presenter.reply({ data: { evaluations, total_pages } });
  }
}
