import UpdateEmployeeController from '@presentation/controllers/employee/UpdateEmployeeController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import FakeNameValidation from '@utils/validation/validators/common/fakes/FakeNameValidation';

import FakeUpdateEmployee from '@data/useCases/employee/fakes/FakeUpdateEmployee';
import FakeUpdateEmployeePresenter from '@presentation/presenters/employee/fakes/FakeUpdateEmployeePresenter';

let updateEmployeeController: UpdateEmployeeController;

let validationComposite: ValidationComposite;

let fakeNameValidation: FakeNameValidation;

let fakeUpdateEmployee: FakeUpdateEmployee;
let fakeUpdateEmployeePresenter: FakeUpdateEmployeePresenter;

describe('UpdateEmployeeController', () => {
  beforeEach(() => {
    fakeNameValidation = new FakeNameValidation();

    fakeUpdateEmployee = new FakeUpdateEmployee();
    fakeUpdateEmployeePresenter = new FakeUpdateEmployeePresenter();
    validationComposite = new ValidationComposite([fakeNameValidation]);

    updateEmployeeController = new UpdateEmployeeController(
      fakeUpdateEmployee,
      validationComposite,
      fakeUpdateEmployeePresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await updateEmployeeController.handle({
      body: { name: 'any_name', is_active: true },
      params: { employee_alias_id: 'any_alias_id' },
    });

    expect(validateSpy).toHaveBeenCalledWith({
      name: 'any_name',
      is_active: true,
    });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateEmployeeController.handle({
        body: { name: 'any_name', is_active: true },
        params: { employee_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call update with correct values', async () => {
    const updateSpy = jest.spyOn(fakeUpdateEmployee, 'update');

    await updateEmployeeController.handle({
      body: { name: 'any_name' },
      params: { employee_alias_id: 'any_alias_id' },
    });

    expect(updateSpy).toHaveBeenCalledWith({
      employee_alias_id: 'any_alias_id',
      data: { name: 'any_name' },
    });
  });

  it('should be able to throw if update throws', async () => {
    jest.spyOn(fakeUpdateEmployee, 'update').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateEmployeeController.handle({
        body: { name: 'any_name' },
        params: { employee_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeUpdateEmployeePresenter, 'reply');

    await updateEmployeeController.handle({
      body: { name: 'any_name', is_active: true },
      params: { employee_alias_id: 'any_alias_id' },
    });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        employee: {
          id: expect.any(String),
          alias_id: expect.any(String),
          name: expect.any(String),
          is_active: expect.any(Boolean),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeUpdateEmployeePresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateEmployeeController.handle({
        body: { name: 'any_name', is_active: true },
        params: { employee_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });
});
