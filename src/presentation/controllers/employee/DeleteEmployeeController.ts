import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IController from '@presentation/protocols/IController';
import IPresenter from '@presentation/protocols/IPresenter';

import IDeleteEmployee from '@domain/useCases/employee/IDeleteEmployee';

export default class DeleteEmployeeController implements IController {
  constructor(
    private readonly deleteEmployee: IDeleteEmployee,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ params }: IHttpRequest): Promise<IHttpResponse> {
    const { employee_alias_id } = params;

    await this.deleteEmployee.delete(employee_alias_id);

    return this.presenter.reply({ data: {} });
  }
}
