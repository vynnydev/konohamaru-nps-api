import FindEmployeesController from '@presentation/controllers/employee/FindEmployeesController';

import FakeFindEmployees from '@data/useCases/employee/fakes/FakeFindEmployees';
import FakeFindEmployeesPresenter from '@presentation/presenters/employee/fakes/FakeFindEmployeesPresenter';

let findEmployeesController: FindEmployeesController;

let fakeFindEmployees: FakeFindEmployees;
let fakeFindEmployeesPresenter: FakeFindEmployeesPresenter;

describe('FindEmployeesController', () => {
  beforeEach(() => {
    fakeFindEmployees = new FakeFindEmployees();

    fakeFindEmployeesPresenter = new FakeFindEmployeesPresenter();

    findEmployeesController = new FindEmployeesController(
      fakeFindEmployees,
      fakeFindEmployeesPresenter,
    );
  });

  it('should be able to handle find employees', async () => {
    const httpResponse = await findEmployeesController.handle();

    expect(httpResponse.status_code).toEqual(200);
    expect(httpResponse.body).toEqual({
      status: 'success',
      employees: [
        {
          id: expect.any(String),
          alias_id: expect.any(String),
          name: expect.any(String),
          is_active: expect.any(Boolean),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      ],
    });
  });
});
