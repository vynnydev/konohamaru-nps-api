import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IController from '@presentation/protocols/IController';
import IPresenter from '@presentation/protocols/IPresenter';
import IValidation from '@presentation/protocols/IValidation';

import IUpdateEmployee from '@domain/useCases/employee/IUpdateEmployee';

export default class UpdateEmployeeController implements IController {
  constructor(
    private readonly updateEmployee: IUpdateEmployee,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ params, body }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(body);

    const { employee_alias_id } = params;

    const { name } = body;

    const updatedEmployee = await this.updateEmployee.update({
      employee_alias_id,
      data: { name },
    });

    return this.presenter.reply({ data: { employee: updatedEmployee } });
  }
}
