import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IPresenter from '@presentation/protocols/IPresenter';
import IController from '@presentation/protocols/IController';
import IValidation from '@presentation/protocols/IValidation';

import ICreateEmployee from '@domain/useCases/employee/ICreateEmployee';

export default class CreateEmployeeController implements IController {
  constructor(
    private readonly createEmployee: ICreateEmployee,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ body }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(body);

    const { name } = body;

    const createdEmployee = await this.createEmployee.create(name);

    return this.presenter.reply({ data: { employee: createdEmployee } });
  }
}
