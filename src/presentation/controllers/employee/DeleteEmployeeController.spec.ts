import DeleteEmployeeController from '@presentation/controllers/employee/DeleteEmployeeController';

import FakeDeleteEmployee from '@data/useCases/employee/fakes/FakeDeleteEmployee';
import FakeDeleteEmployeePresenter from '@presentation/presenters/employee/fakes/FakeDeleteEmployeePresenter';

let deleteEmployeeController: DeleteEmployeeController;

let fakeDeleteEmployee: FakeDeleteEmployee;
let fakeDeleteEmployeePresenter: FakeDeleteEmployeePresenter;

describe('DeleteEmployeeController', () => {
  beforeEach(() => {
    fakeDeleteEmployee = new FakeDeleteEmployee();
    fakeDeleteEmployeePresenter = new FakeDeleteEmployeePresenter();

    deleteEmployeeController = new DeleteEmployeeController(
      fakeDeleteEmployee,
      fakeDeleteEmployeePresenter,
    );
  });

  it('should be able to call delete employee with correct values', async () => {
    const deleteSpy = jest.spyOn(fakeDeleteEmployee, 'delete');

    await deleteEmployeeController.handle({ params: { employee_alias_id: 'any_alias_id' } });

    expect(deleteSpy).toHaveBeenCalledWith('any_alias_id');
  });

  it('should be able to throw if delete employee throws', async () => {
    jest.spyOn(fakeDeleteEmployee, 'delete').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      deleteEmployeeController.handle({ params: { employee_alias_id: 'any_alias_id' } }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeDeleteEmployeePresenter, 'reply');

    await deleteEmployeeController.handle({ params: { employee_alias_id: 'any_alias_id' } });

    expect(replySpy).toHaveBeenCalledWith({ data: {} });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeDeleteEmployeePresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      deleteEmployeeController.handle({ params: { employee_alias_id: 'any_alias_id' } }),
    ).rejects.toBeInstanceOf(Error);
  });
});
