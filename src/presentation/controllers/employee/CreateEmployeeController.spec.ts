import CreateEmployeeController from '@presentation/controllers/employee/CreateEmployeeController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import FakeNameValidation from '@utils/validation/validators/common/fakes/FakeNameValidation';

import FakeCreateEmployee from '@data/useCases/employee/fakes/FakeCreateEmployee';
import FakeCreateEmployeePresenter from '@presentation/presenters/employee/fakes/FakeCreateEmployeePresenter';

let createEmployeeController: CreateEmployeeController;

let validationComposite: ValidationComposite;

let fakeNameValidation: FakeNameValidation;

let fakeCreateEmployee: FakeCreateEmployee;
let fakeCreateEmployeePresenter: FakeCreateEmployeePresenter;

describe('CreateEmployeeController', () => {
  beforeEach(() => {
    fakeNameValidation = new FakeNameValidation();

    fakeCreateEmployee = new FakeCreateEmployee();
    fakeCreateEmployeePresenter = new FakeCreateEmployeePresenter();

    validationComposite = new ValidationComposite([fakeNameValidation]);

    createEmployeeController = new CreateEmployeeController(
      fakeCreateEmployee,
      validationComposite,
      fakeCreateEmployeePresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await createEmployeeController.handle({
      body: { name: 'any_name' },
    });

    expect(validateSpy).toHaveBeenCalledWith({ name: 'any_name' });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createEmployeeController.handle({
        body: { name: 'any_name' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call create employee with correct values', async () => {
    const createSpy = jest.spyOn(fakeCreateEmployee, 'create');

    await createEmployeeController.handle({
      body: { name: 'any_name' },
    });

    expect(createSpy).toHaveBeenCalledWith('any_name');
  });

  it('should be able to throw if create employee throws', async () => {
    jest.spyOn(fakeCreateEmployee, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createEmployeeController.handle({
        body: { name: 'any_name' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeCreateEmployeePresenter, 'reply');

    await createEmployeeController.handle({
      body: { name: 'any_name' },
    });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        employee: {
          id: expect.any(String),
          alias_id: expect.any(String),
          name: expect.any(String),
          is_active: expect.any(Boolean),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeCreateEmployeePresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createEmployeeController.handle({
        body: { name: 'any_name' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });
});
