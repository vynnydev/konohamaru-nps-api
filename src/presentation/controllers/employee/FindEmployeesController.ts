import { IHttpResponse } from '@presentation/protocols/IHttp';
import IPresenter from '@presentation/protocols/IPresenter';
import IController from '@presentation/protocols/IController';

import IFindEmployees from '@domain/useCases/employee/IFindEmployees';

export default class FindEmployeesController implements IController {
  constructor(
    private readonly findEmployees: IFindEmployees,
    private readonly presenter: IPresenter,
  ) {}

  public async handle(): Promise<IHttpResponse> {
    const foundEmployees = await this.findEmployees.find();

    return this.presenter.reply({ data: { employees: foundEmployees } });
  }
}
