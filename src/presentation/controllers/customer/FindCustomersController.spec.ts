import FindCustomersController from '@presentation/controllers/customer/FindCustomersController';

import FakeFindCustomers from '@data/useCases/customer/fakes/FakeFindCustomers';
import FakeFindCustomersPresenter from '@presentation/presenters/customer/fakes/FakeFindCustomersPresenter';

let findCustomersController: FindCustomersController;

let fakeFindCustomers: FakeFindCustomers;
let fakeFindCustomersPresenter: FakeFindCustomersPresenter;

describe('FindCustomersController', () => {
  beforeEach(() => {
    fakeFindCustomers = new FakeFindCustomers();

    fakeFindCustomersPresenter = new FakeFindCustomersPresenter();

    findCustomersController = new FindCustomersController(
      fakeFindCustomers,
      fakeFindCustomersPresenter,
    );
  });

  it('should be able to handle find customers', async () => {
    const httpResponse = await findCustomersController.handle();

    expect(httpResponse.status_code).toEqual(200);
    expect(httpResponse.body).toEqual({
      status: 'success',
      customers: [
        {
          id: expect.any(String),
          alias_id: expect.any(String),
          cpf: expect.any(String),
          name: expect.any(String),
          email: expect.any(String),
          phone_number: expect.any(String),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      ],
    });
  });
});
