import CreateCustomerController from '@presentation/controllers/customer/CreateCustomerController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import FakeCpfValidation from '@utils/validation/validators/common/fakes/FakeCpfValidation';
import FakeEmailValidation from '@utils/validation/validators/common/fakes/FakeEmailValidation';
import FakeNameValidation from '@utils/validation/validators/common/fakes/FakeNameValidation';
import FakePhoneNumberValidation from '@utils/validation/validators/common/fakes/FakePhoneNumberValidation';

import FakeCreateCustomer from '@data/useCases/customer/fakes/FakeCreateCustomer';
import FakeCreateCustomerPresenter from '@presentation/presenters/customer/fakes/FakeCreateCustomerPresenter';

let createCustomerController: CreateCustomerController;

let validationComposite: ValidationComposite;

let fakeCpfValidation: FakeCpfValidation;
let fakeEmailValidation: FakeEmailValidation;
let fakeNameValidation: FakeNameValidation;
let fakePhoneNumberValidation: FakePhoneNumberValidation;

let fakeCreateCustomer: FakeCreateCustomer;
let fakeCreateCustomerPresenter: FakeCreateCustomerPresenter;

describe('CreateCustomerController', () => {
  beforeEach(() => {
    fakeCpfValidation = new FakeCpfValidation();
    fakeEmailValidation = new FakeEmailValidation();
    fakeNameValidation = new FakeNameValidation();
    fakePhoneNumberValidation = new FakePhoneNumberValidation();

    fakeCreateCustomer = new FakeCreateCustomer();
    fakeCreateCustomerPresenter = new FakeCreateCustomerPresenter();

    validationComposite = new ValidationComposite([
      fakeCpfValidation,
      fakeEmailValidation,
      fakeNameValidation,
      fakePhoneNumberValidation,
    ]);

    createCustomerController = new CreateCustomerController(
      fakeCreateCustomer,
      validationComposite,
      fakeCreateCustomerPresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await createCustomerController.handle({
      body: {
        cpf: 'any_cpf',
        name: 'any_name',
        email: 'any_email',
        phone_number: 'any_phone_number',
      },
    });

    expect(validateSpy).toHaveBeenCalledWith({
      cpf: 'any_cpf',
      name: 'any_name',
      email: 'any_email',
      phone_number: 'any_phone_number',
    });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createCustomerController.handle({
        body: {
          cpf: 'any_cpf',
          name: 'any_name',
          email: 'any_email',
          phone_number: 'any_phone_number',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call create customer with correct values', async () => {
    const createSpy = jest.spyOn(fakeCreateCustomer, 'create');

    await createCustomerController.handle({
      body: {
        cpf: 'any_cpf',
        name: 'any_name',
        email: 'any_email',
        phone_number: 'any_phone_number',
      },
    });

    expect(createSpy).toHaveBeenCalledWith({
      cpf: 'any_cpf',
      name: 'any_name',
      email: 'any_email',
      phone_number: 'any_phone_number',
    });
  });

  it('should be able to throw if create customer throws', async () => {
    jest.spyOn(fakeCreateCustomer, 'create').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createCustomerController.handle({
        body: {
          cpf: 'any_cpf',
          name: 'any_name',
          email: 'any_email',
          phone_number: 'any_phone_number',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeCreateCustomerPresenter, 'reply');

    await createCustomerController.handle({
      body: {
        cpf: 'any_cpf',
        name: 'any_name',
        email: 'any_email',
        phone_number: 'any_phone_number',
      },
    });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        customer: {
          id: expect.any(String),
          alias_id: expect.any(String),
          cpf: expect.any(String),
          name: expect.any(String),
          email: expect.any(String),
          phone_number: expect.any(String),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeCreateCustomerPresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      createCustomerController.handle({
        body: {
          cpf: 'any_cpf',
          name: 'any_name',
          email: 'any_email',
          phone_number: 'any_phone_number',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });
});
