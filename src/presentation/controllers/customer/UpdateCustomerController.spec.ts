import UpdateCustomerController from '@presentation/controllers/customer/UpdateCustomerController';

import ValidationComposite from '@utils/validation/validators/ValidationComposite';

import FakeCpfValidation from '@utils/validation/validators/common/fakes/FakeCpfValidation';
import FakeEmailValidation from '@utils/validation/validators/common/fakes/FakeEmailValidation';
import FakeNameValidation from '@utils/validation/validators/common/fakes/FakeNameValidation';
import FakePhoneNumberValidation from '@utils/validation/validators/common/fakes/FakePhoneNumberValidation';

import FakeUpdateCustomer from '@data/useCases/customer/fakes/FakeUpdateCustomer';
import FakeUpdateCustomerPresenter from '@presentation/presenters/customer/fakes/FakeUpdateCustomerPresenter';

let updateCustomerController: UpdateCustomerController;

let validationComposite: ValidationComposite;

let fakeCpfValidation: FakeCpfValidation;
let fakeEmailValidation: FakeEmailValidation;
let fakeNameValidation: FakeNameValidation;
let fakePhoneNumberValidation: FakePhoneNumberValidation;

let fakeUpdateCustomer: FakeUpdateCustomer;
let fakeUpdateCustomerPresenter: FakeUpdateCustomerPresenter;

describe('UpdateCustomerController', () => {
  beforeEach(() => {
    fakeCpfValidation = new FakeCpfValidation();
    fakeEmailValidation = new FakeEmailValidation();
    fakeNameValidation = new FakeNameValidation();
    fakePhoneNumberValidation = new FakePhoneNumberValidation();

    fakeUpdateCustomer = new FakeUpdateCustomer();
    fakeUpdateCustomerPresenter = new FakeUpdateCustomerPresenter();
    validationComposite = new ValidationComposite([
      fakeCpfValidation,
      fakeEmailValidation,
      fakeNameValidation,
      fakePhoneNumberValidation,
    ]);

    updateCustomerController = new UpdateCustomerController(
      fakeUpdateCustomer,
      validationComposite,
      fakeUpdateCustomerPresenter,
    );
  });

  it('should be able to call validate with correct values', async () => {
    const validateSpy = jest.spyOn(validationComposite, 'validate');

    await updateCustomerController.handle({
      body: {
        cpf: 'any_cpf',
        name: 'any_name',
        email: 'any_email',
        phone_number: 'any_phone_number',
      },
      params: { customer_alias_id: 'any_alias_id' },
    });

    expect(validateSpy).toHaveBeenCalledWith({
      cpf: 'any_cpf',
      name: 'any_name',
      email: 'any_email',
      phone_number: 'any_phone_number',
    });
  });

  it('should be able to throw if validate throws', async () => {
    jest.spyOn(validationComposite, 'validate').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateCustomerController.handle({
        body: {
          cpf: 'any_cpf',
          name: 'any_name',
          email: 'any_email',
          phone_number: 'any_phone_number',
        },
        params: { customer_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call update with correct values', async () => {
    const updateSpy = jest.spyOn(fakeUpdateCustomer, 'update');

    await updateCustomerController.handle({
      body: {
        cpf: 'any_cpf',
        name: 'any_name',
        email: 'any_email',
        phone_number: 'any_phone_number',
      },
      params: { customer_alias_id: 'any_alias_id' },
    });

    expect(updateSpy).toHaveBeenCalledWith({
      customer_alias_id: 'any_alias_id',
      data: {
        cpf: 'any_cpf',
        name: 'any_name',
        email: 'any_email',
        phone_number: 'any_phone_number',
      },
    });
  });

  it('should be able to throw if update throws', async () => {
    jest.spyOn(fakeUpdateCustomer, 'update').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateCustomerController.handle({
        body: {
          cpf: 'any_cpf',
          name: 'any_name',
          email: 'any_email',
          phone_number: 'any_phone_number',
        },
        params: {
          customer_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeUpdateCustomerPresenter, 'reply');

    await updateCustomerController.handle({
      body: {
        cpf: 'any_cpf',
        name: 'any_name',
        email: 'any_email',
        phone_number: 'any_phone_number',
      },
      params: {
        customer_alias_id: 'any_alias_id',
      },
    });

    expect(replySpy).toHaveBeenCalledWith({
      data: {
        customer: {
          id: expect.any(String),
          alias_id: expect.any(String),
          cpf: expect.any(String),
          name: expect.any(String),
          email: expect.any(String),
          phone_number: expect.any(String),
          created_at: expect.any(Date),
          updated_at: expect.any(Date),
        },
      },
    });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeUpdateCustomerPresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      updateCustomerController.handle({
        body: {
          cpf: 'any_cpf',
          name: 'any_name',
          email: 'any_email',
          phone_number: 'any_phone_number',
        },
        params: {
          customer_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });
});
