import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IPresenter from '@presentation/protocols/IPresenter';
import IController from '@presentation/protocols/IController';
import IValidation from '@presentation/protocols/IValidation';

import ICreateCustomer from '@domain/useCases/customer/ICreateCustomer';

export default class CreateCustomerController implements IController {
  constructor(
    private readonly createCustomer: ICreateCustomer,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ body }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(body);

    const { cpf, name, email, phone_number } = body;

    const createdCustomer = await this.createCustomer.create({
      cpf,
      name,
      email,
      phone_number,
    });

    return this.presenter.reply({ data: { customer: createdCustomer } });
  }
}
