import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IController from '@presentation/protocols/IController';
import IPresenter from '@presentation/protocols/IPresenter';

import IDeleteCustomer from '@domain/useCases/customer/IDeleteCustomer';

export default class DeleteCustomerController implements IController {
  constructor(
    private readonly deleteCustomer: IDeleteCustomer,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ params }: IHttpRequest): Promise<IHttpResponse> {
    const { customer_alias_id } = params;

    await this.deleteCustomer.delete(customer_alias_id);

    return this.presenter.reply({ data: {} });
  }
}
