import { IHttpResponse } from '@presentation/protocols/IHttp';
import IPresenter from '@presentation/protocols/IPresenter';
import IController from '@presentation/protocols/IController';

import IFindCustomers from '@domain/useCases/customer/IFindCustomers';

export default class FindCustomersController implements IController {
  constructor(
    private readonly findCustomers: IFindCustomers,
    private readonly presenter: IPresenter,
  ) {}

  public async handle(): Promise<IHttpResponse> {
    const foundCustomers = await this.findCustomers.find();

    return this.presenter.reply({ data: { customers: foundCustomers } });
  }
}
