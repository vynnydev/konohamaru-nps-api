import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';
import IController from '@presentation/protocols/IController';
import IPresenter from '@presentation/protocols/IPresenter';
import IValidation from '@presentation/protocols/IValidation';

import IUpdateCustomer from '@domain/useCases/customer/IUpdateCustomer';

export default class UpdateCustomerController implements IController {
  constructor(
    private readonly updateCustomer: IUpdateCustomer,
    private readonly validation: IValidation,
    private readonly presenter: IPresenter,
  ) {}

  public async handle({ params, body }: IHttpRequest): Promise<IHttpResponse> {
    this.validation.validate(body);

    const { customer_alias_id } = params;

    const { cpf, name, email, phone_number } = body;

    const updatedCustomer = await this.updateCustomer.update({
      customer_alias_id,
      data: {
        cpf,
        name,
        email,
        phone_number,
      },
    });

    return this.presenter.reply({ data: { customer: updatedCustomer } });
  }
}
