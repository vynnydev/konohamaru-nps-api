import DeleteCustomerController from '@presentation/controllers/customer/DeleteCustomerController';

import FakeDeleteCustomer from '@data/useCases/customer/fakes/FakeDeleteCustomer';
import FakeDeleteCustomerPresenter from '@presentation/presenters/customer/fakes/FakeDeleteCustomerPresenter';

let deleteCustomerController: DeleteCustomerController;

let fakeDeleteCustomer: FakeDeleteCustomer;
let fakeDeleteCustomerPresenter: FakeDeleteCustomerPresenter;

describe('DeleteCustomerController', () => {
  beforeEach(() => {
    fakeDeleteCustomer = new FakeDeleteCustomer();
    fakeDeleteCustomerPresenter = new FakeDeleteCustomerPresenter();

    deleteCustomerController = new DeleteCustomerController(
      fakeDeleteCustomer,
      fakeDeleteCustomerPresenter,
    );
  });

  it('should be able to call delete customer with correct values', async () => {
    const deleteSpy = jest.spyOn(fakeDeleteCustomer, 'delete');

    await deleteCustomerController.handle({ params: { customer_alias_id: 'any_alias_id' } });

    expect(deleteSpy).toHaveBeenCalledWith('any_alias_id');
  });

  it('should be able to throw if delete customer throws', async () => {
    jest.spyOn(fakeDeleteCustomer, 'delete').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      deleteCustomerController.handle({ params: { customer_alias_id: 'any_alias_id' } }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to call reply with correct values', async () => {
    const replySpy = jest.spyOn(fakeDeleteCustomerPresenter, 'reply');

    await deleteCustomerController.handle({ params: { customer_alias_id: 'any_alias_id' } });

    expect(replySpy).toHaveBeenCalledWith({ data: {} });
  });

  it('should be able to throw if reply throws', async () => {
    jest.spyOn(fakeDeleteCustomerPresenter, 'reply').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      deleteCustomerController.handle({ params: { customer_alias_id: 'any_alias_id' } }),
    ).rejects.toBeInstanceOf(Error);
  });
});
