import AppError from '@utils/errors/AppError';

import IStoreRepository from '@data/protocols/database/repositories/store/IStoreRepository';
import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';

import IMiddleware from '@presentation/protocols/IMiddleware';
import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';

export default class CheckIfIsStoreTransactionMiddleware implements IMiddleware {
  constructor(
    private readonly storeRepository: IStoreRepository,
    private readonly transactionRepository: ITransactionRepository,
  ) {}

  public async handle({ params }: IHttpRequest): Promise<IHttpResponse> {
    const { transaction_alias_id, store_alias_id } = params;

    const foundStore = await this.storeRepository.findByAliasId(store_alias_id);

    if (!foundStore) throw new AppError({ message: 'store does not exists', status_code: 400 });

    const foundTransaction = await this.transactionRepository.findByAliasId(transaction_alias_id);

    if (!foundTransaction)
      throw new AppError({ message: 'transaction does not exists', status_code: 400 });

    if (foundStore.id !== foundTransaction.store_id)
      throw new AppError({
        message: 'This transaction is not from this store',
        status_code: 403,
      });

    return { status_code: 200, body: { status: 'success' } };
  }
}
