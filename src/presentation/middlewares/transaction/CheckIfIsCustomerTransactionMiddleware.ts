import AppError from '@utils/errors/AppError';

import ICustomerRepository from '@data/protocols/database/repositories/customer/ICustomerRepository';
import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';

import IMiddleware from '@presentation/protocols/IMiddleware';
import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';

export default class CheckIfIsCustomerTransactionMiddleware implements IMiddleware {
  constructor(
    private readonly customerRepository: ICustomerRepository,
    private readonly transactionRepository: ITransactionRepository,
  ) {}

  public async handle({ params }: IHttpRequest): Promise<IHttpResponse> {
    const { transaction_alias_id, customer_alias_id } = params;

    const foundCustomer = await this.customerRepository.findByAliasId(customer_alias_id);

    if (!foundCustomer) throw new AppError({ message: 'store does not exists', status_code: 400 });

    const foundTransaction = await this.transactionRepository.findByAliasId(transaction_alias_id);

    if (!foundTransaction)
      throw new AppError({ message: 'transaction does not exists', status_code: 400 });

    if (foundCustomer.id !== foundTransaction.customer_id)
      throw new AppError({
        message: 'This transaction is not from this customer',
        status_code: 403,
      });

    return { status_code: 200, body: { status: 'success' } };
  }
}
