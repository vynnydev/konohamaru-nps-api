import AppError from '@utils/errors/AppError';

import FakeEmployeeRepository from '@infra/database/repositories/employee/fakes/FakeEmployeeRepository';
import FakeTransactionRepository from '@infra/database/repositories/transaction/fakes/FakeTransactionRepository';

import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import CheckIfIsEmployeeTransactionMiddleware from '@presentation/middlewares/transaction/CheckIfIsEmployeeTransactionMiddleware';

let checkIfIsEmployeeTransactionMiddleware: CheckIfIsEmployeeTransactionMiddleware;

let fakeEmployeeRepository: FakeEmployeeRepository;
let fakeTransactionRepository: FakeTransactionRepository;

describe('CheckIfIsEmployeeTransactionMiddleware', () => {
  beforeEach(() => {
    fakeEmployeeRepository = new FakeEmployeeRepository();
    fakeTransactionRepository = new FakeTransactionRepository();

    checkIfIsEmployeeTransactionMiddleware = new CheckIfIsEmployeeTransactionMiddleware(
      fakeEmployeeRepository,
      fakeTransactionRepository,
    );
  });

  it('should be able to call find by employee alias id with correct values', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ employee_id: createdEmployee.id }),
    );

    const findByAliasIdSpy = jest.spyOn(fakeEmployeeRepository, 'findByAliasId');

    await checkIfIsEmployeeTransactionMiddleware.handle({
      params: {
        employee_alias_id: createdEmployee.alias_id,
        transaction_alias_id: createdTransaction.alias_id,
      },
    });

    expect(findByAliasIdSpy).toHaveBeenCalledWith(createdEmployee.alias_id);
  });

  it('should be able to throw if find employee by alias id throws', async () => {
    jest.spyOn(fakeEmployeeRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      checkIfIsEmployeeTransactionMiddleware.handle({
        params: {
          employee_alias_id: 'any_alias_id',
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if does not exists employee', async () => {
    await expect(
      checkIfIsEmployeeTransactionMiddleware.handle({
        params: {
          employee_alias_id: 'any_alias_id',
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call find transaction by alias id with correct values', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ employee_id: createdEmployee.id }),
    );

    const findByAliasIdSpy = jest.spyOn(fakeTransactionRepository, 'findByAliasId');

    await checkIfIsEmployeeTransactionMiddleware.handle({
      params: {
        employee_alias_id: createdEmployee.alias_id,
        transaction_alias_id: createdTransaction.alias_id,
      },
    });

    expect(findByAliasIdSpy).toHaveBeenCalledWith(createdTransaction.alias_id);
  });

  it('should be able to throw if find transaction by alias id throws', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    jest.spyOn(fakeTransactionRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      checkIfIsEmployeeTransactionMiddleware.handle({
        params: {
          employee_alias_id: createdEmployee.alias_id,
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if does not exists transaction', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    await expect(
      checkIfIsEmployeeTransactionMiddleware.handle({
        params: {
          employee_alias_id: createdEmployee.alias_id,
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to throw if not employee transaction', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ employee_id: 'any_employee_id' }),
    );

    await expect(
      checkIfIsEmployeeTransactionMiddleware.handle({
        params: {
          employee_alias_id: createdEmployee.alias_id,
          transaction_alias_id: createdTransaction.alias_id,
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to check if is employee transaction', async () => {
    const createdEmployee = await fakeEmployeeRepository.create(mockEmployeeModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ employee_id: createdEmployee.id }),
    );

    const httpResponse = await checkIfIsEmployeeTransactionMiddleware.handle({
      params: {
        employee_alias_id: createdEmployee.alias_id,
        transaction_alias_id: createdTransaction.alias_id,
      },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({ status: 'success' });
  });
});
