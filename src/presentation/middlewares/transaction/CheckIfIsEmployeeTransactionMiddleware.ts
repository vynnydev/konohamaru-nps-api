import AppError from '@utils/errors/AppError';

import IEmployeeRepository from '@data/protocols/database/repositories/employee/IEmployeeRepository';
import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';

import IMiddleware from '@presentation/protocols/IMiddleware';
import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';

export default class CheckIfIsEmployeeTransactionMiddleware implements IMiddleware {
  constructor(
    private readonly employeeRepository: IEmployeeRepository,
    private readonly transactionRepository: ITransactionRepository,
  ) {}

  public async handle({ params }: IHttpRequest): Promise<IHttpResponse> {
    const { transaction_alias_id, employee_alias_id } = params;

    const foundEmployee = await this.employeeRepository.findByAliasId(employee_alias_id);

    if (!foundEmployee) throw new AppError({ message: 'store does not exists', status_code: 400 });

    const foundTransaction = await this.transactionRepository.findByAliasId(transaction_alias_id);

    if (!foundTransaction)
      throw new AppError({ message: 'transaction does not exists', status_code: 400 });

    if (foundEmployee.id !== foundTransaction.employee_id)
      throw new AppError({
        message: 'This transaction is not from this employee',
        status_code: 403,
      });

    return { status_code: 200, body: { status: 'success' } };
  }
}
