import AppError from '@utils/errors/AppError';

import FakeCustomerRepository from '@infra/database/repositories/customer/fakes/FakeCustomerRepository';
import FakeTransactionRepository from '@infra/database/repositories/transaction/fakes/FakeTransactionRepository';

import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import CheckIfIsCustomerTransactionMiddleware from '@presentation/middlewares/transaction/CheckIfIsCustomerTransactionMiddleware';

let checkIfIsCustomerTransactionMiddleware: CheckIfIsCustomerTransactionMiddleware;

let fakeCustomerRepository: FakeCustomerRepository;
let fakeTransactionRepository: FakeTransactionRepository;

describe('CheckIfIsCustomerTransactionMiddleware', () => {
  beforeEach(() => {
    fakeCustomerRepository = new FakeCustomerRepository();
    fakeTransactionRepository = new FakeTransactionRepository();

    checkIfIsCustomerTransactionMiddleware = new CheckIfIsCustomerTransactionMiddleware(
      fakeCustomerRepository,
      fakeTransactionRepository,
    );
  });

  it('should be able to call find by customer alias id with correct values', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ customer_id: createdCustomer.id }),
    );

    const findByAliasIdSpy = jest.spyOn(fakeCustomerRepository, 'findByAliasId');

    await checkIfIsCustomerTransactionMiddleware.handle({
      params: {
        customer_alias_id: createdCustomer.alias_id,
        transaction_alias_id: createdTransaction.alias_id,
      },
    });

    expect(findByAliasIdSpy).toHaveBeenCalledWith(createdCustomer.alias_id);
  });

  it('should be able to throw if find customer by alias id throws', async () => {
    jest.spyOn(fakeCustomerRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      checkIfIsCustomerTransactionMiddleware.handle({
        params: {
          customer_alias_id: 'any_alias_id',
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if does not exists customer', async () => {
    await expect(
      checkIfIsCustomerTransactionMiddleware.handle({
        params: {
          customer_alias_id: 'any_alias_id',
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call find transaction by alias id with correct values', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ customer_id: createdCustomer.id }),
    );

    const findByAliasIdSpy = jest.spyOn(fakeTransactionRepository, 'findByAliasId');

    await checkIfIsCustomerTransactionMiddleware.handle({
      params: {
        customer_alias_id: createdCustomer.alias_id,
        transaction_alias_id: createdTransaction.alias_id,
      },
    });

    expect(findByAliasIdSpy).toHaveBeenCalledWith(createdTransaction.alias_id);
  });

  it('should be able to throw if find transaction by alias id throws', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    jest.spyOn(fakeTransactionRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      checkIfIsCustomerTransactionMiddleware.handle({
        params: {
          customer_alias_id: createdCustomer.alias_id,
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if does not exists transaction', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    await expect(
      checkIfIsCustomerTransactionMiddleware.handle({
        params: {
          customer_alias_id: createdCustomer.alias_id,
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to throw if not customer transaction', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ customer_id: 'any_customer_id' }),
    );

    await expect(
      checkIfIsCustomerTransactionMiddleware.handle({
        params: {
          customer_alias_id: createdCustomer.alias_id,
          transaction_alias_id: createdTransaction.alias_id,
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to check if is customer transaction', async () => {
    const createdCustomer = await fakeCustomerRepository.create(mockCustomerModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ customer_id: createdCustomer.id }),
    );

    const httpResponse = await checkIfIsCustomerTransactionMiddleware.handle({
      params: {
        customer_alias_id: createdCustomer.alias_id,
        transaction_alias_id: createdTransaction.alias_id,
      },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({ status: 'success' });
  });
});
