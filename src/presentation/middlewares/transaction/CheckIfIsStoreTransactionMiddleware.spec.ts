import AppError from '@utils/errors/AppError';

import FakeStoreRepository from '@infra/database/repositories/store/fakes/FakeStoreRepository';
import FakeTransactionRepository from '@infra/database/repositories/transaction/fakes/FakeTransactionRepository';

import mockStoreModel from '@tests/domain/mocks/MockStoreModel';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import CheckIfIsStoreTransactionMiddleware from '@presentation/middlewares/transaction/CheckIfIsStoreTransactionMiddleware';

let checkIfIsStoreTransactionMiddleware: CheckIfIsStoreTransactionMiddleware;

let fakeStoreRepository: FakeStoreRepository;
let fakeTransactionRepository: FakeTransactionRepository;

describe('CheckIfIsStoreTransactionMiddleware', () => {
  beforeEach(() => {
    fakeStoreRepository = new FakeStoreRepository();
    fakeTransactionRepository = new FakeTransactionRepository();

    checkIfIsStoreTransactionMiddleware = new CheckIfIsStoreTransactionMiddleware(
      fakeStoreRepository,
      fakeTransactionRepository,
    );
  });

  it('should be able to call find by store alias id with correct values', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ store_id: createdStore.id }),
    );

    const findByAliasIdSpy = jest.spyOn(fakeStoreRepository, 'findByAliasId');

    await checkIfIsStoreTransactionMiddleware.handle({
      params: {
        store_alias_id: createdStore.alias_id,
        transaction_alias_id: createdTransaction.alias_id,
      },
    });

    expect(findByAliasIdSpy).toHaveBeenCalledWith(createdStore.alias_id);
  });

  it('should be able to throw if find store by alias id throws', async () => {
    jest.spyOn(fakeStoreRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      checkIfIsStoreTransactionMiddleware.handle({
        params: {
          store_alias_id: 'any_alias_id',
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if does not exists store', async () => {
    await expect(
      checkIfIsStoreTransactionMiddleware.handle({
        params: {
          store_alias_id: 'any_alias_id',
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call find transaction by alias id with correct values', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ store_id: createdStore.id }),
    );

    const findByAliasIdSpy = jest.spyOn(fakeTransactionRepository, 'findByAliasId');

    await checkIfIsStoreTransactionMiddleware.handle({
      params: {
        store_alias_id: createdStore.alias_id,
        transaction_alias_id: createdTransaction.alias_id,
      },
    });

    expect(findByAliasIdSpy).toHaveBeenCalledWith(createdTransaction.alias_id);
  });

  it('should be able to throw if find transaction by alias id throws', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    jest.spyOn(fakeTransactionRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      checkIfIsStoreTransactionMiddleware.handle({
        params: {
          store_alias_id: createdStore.alias_id,
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if does not exists transaction', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    await expect(
      checkIfIsStoreTransactionMiddleware.handle({
        params: {
          store_alias_id: createdStore.alias_id,
          transaction_alias_id: 'any_alias_id',
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to throw if not store transaction', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ store_id: 'any_store_id' }),
    );

    await expect(
      checkIfIsStoreTransactionMiddleware.handle({
        params: {
          store_alias_id: createdStore.alias_id,
          transaction_alias_id: createdTransaction.alias_id,
        },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to check if is store transaction', async () => {
    const createdStore = await fakeStoreRepository.create(mockStoreModel.mock());

    const createdTransaction = await fakeTransactionRepository.create(
      mockTransactionModel.mock({ store_id: createdStore.id }),
    );

    const httpResponse = await checkIfIsStoreTransactionMiddleware.handle({
      params: {
        store_alias_id: createdStore.alias_id,
        transaction_alias_id: createdTransaction.alias_id,
      },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({ status: 'success' });
  });
});
