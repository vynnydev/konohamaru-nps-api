import AppError from '@utils/errors/AppError';

import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';
import IEvaluationRepository from '@data/protocols/database/repositories/evaluation/IEvaluationRepository';

import IMiddleware from '@presentation/protocols/IMiddleware';
import { IHttpRequest, IHttpResponse } from '@presentation/protocols/IHttp';

export default class CheckIfExistsTransactionEvaluationMiddleware implements IMiddleware {
  constructor(
    private readonly transactionRepository: ITransactionRepository,
    private readonly evaluationRepository: IEvaluationRepository,
  ) {}

  public async handle({ params }: IHttpRequest): Promise<IHttpResponse> {
    const { transaction_alias_id } = params;

    const foundTransaction = await this.transactionRepository.findByAliasId(transaction_alias_id);

    if (!foundTransaction)
      throw new AppError({ message: 'Transaction does not exists', status_code: 400 });

    const foundEvaluation = await this.evaluationRepository.findByTransactionId(
      foundTransaction.id,
    );

    if (foundEvaluation)
      throw new AppError({ message: 'Use another route to update', status_code: 400 });

    return { status_code: 200, body: { status: 'success' } };
  }
}
