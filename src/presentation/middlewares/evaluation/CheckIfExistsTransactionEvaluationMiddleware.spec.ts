import AppError from '@utils/errors/AppError';

import FakeTransactionRepository from '@infra/database/repositories/transaction/fakes/FakeTransactionRepository';
import FakeEvaluationRepository from '@infra/database/repositories/evaluation/fakes/FakeEvaluationRepository';

import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';
import mockEvaluationModel from '@tests/domain/mocks/MockEvaluationModel';

import CheckIfExistsTransactionEvaluationMiddleware from '@presentation/middlewares/evaluation/CheckIfExistsTransactionEvaluationMiddleware';

let checkIfExistsTransactionEvaluationMiddleware: CheckIfExistsTransactionEvaluationMiddleware;

let fakeEvaluationRepository: FakeEvaluationRepository;
let fakeTransactionRepository: FakeTransactionRepository;

describe('CheckIfExistsTransactionEvaluationMiddleware', () => {
  beforeEach(() => {
    fakeEvaluationRepository = new FakeEvaluationRepository();
    fakeTransactionRepository = new FakeTransactionRepository();

    checkIfExistsTransactionEvaluationMiddleware = new CheckIfExistsTransactionEvaluationMiddleware(
      fakeTransactionRepository,
      fakeEvaluationRepository,
    );
  });

  it('should be able to call find by alias id with correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const foundAliasIdSpy = jest.spyOn(fakeTransactionRepository, 'findByAliasId');

    await checkIfExistsTransactionEvaluationMiddleware.handle({
      params: { transaction_alias_id: createdTransaction.alias_id },
    });

    await expect(foundAliasIdSpy).toHaveBeenCalledWith(createdTransaction.alias_id);
  });

  it('should be able to throw if find by alias id throws', async () => {
    jest.spyOn(fakeTransactionRepository, 'findByAliasId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      checkIfExistsTransactionEvaluationMiddleware.handle({
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if transaction does not exists', async () => {
    await expect(
      checkIfExistsTransactionEvaluationMiddleware.handle({
        params: { transaction_alias_id: 'any_alias_id' },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to call find by transaction id with correct values', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const findByTransactionIdSpy = jest.spyOn(fakeEvaluationRepository, 'findByTransactionId');

    await checkIfExistsTransactionEvaluationMiddleware.handle({
      params: { transaction_alias_id: createdTransaction.alias_id },
    });

    expect(findByTransactionIdSpy).toHaveBeenCalledWith(createdTransaction.id);
  });

  it('should be able to throw if find by transaction alias id throws', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    jest.spyOn(fakeEvaluationRepository, 'findByTransactionId').mockImplementationOnce(() => {
      throw new Error();
    });

    await expect(
      checkIfExistsTransactionEvaluationMiddleware.handle({
        params: { transaction_alias_id: createdTransaction.alias_id },
      }),
    ).rejects.toBeInstanceOf(Error);
  });

  it('should be able to throw if transaction evaluation already exists', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    await fakeEvaluationRepository.create(
      mockEvaluationModel.mock({
        transaction_id: createdTransaction.id,
      }),
    );

    await expect(
      checkIfExistsTransactionEvaluationMiddleware.handle({
        params: { transaction_alias_id: createdTransaction.alias_id },
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should be able to check if already exists transaction evaluation', async () => {
    const createdTransaction = await fakeTransactionRepository.create(mockTransactionModel.mock());

    const httpResponse = await checkIfExistsTransactionEvaluationMiddleware.handle({
      params: { transaction_alias_id: createdTransaction.alias_id },
    });

    expect(httpResponse.status_code).toBe(200);
    expect(httpResponse.body).toEqual({ status: 'success' });
  });
});
