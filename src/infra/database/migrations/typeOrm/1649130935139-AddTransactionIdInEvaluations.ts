import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey, TableIndex } from 'typeorm';

export class AddTransactionIdInEvaluations1649130935139 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'evaluations',
      new TableColumn({
        name: 'transaction_id',
        type: 'uuid',
        isNullable: false,
      }),
    );

    await queryRunner.createForeignKey(
      'evaluations',
      new TableForeignKey({
        name: 'FK_TRANSACTION_EVALUATION',
        columnNames: ['transaction_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'transactions',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }),
    );

    await queryRunner.createIndex(
      'evaluations',
      new TableIndex({
        name: 'IDX_EVALUATION_TRANSACTION_ID',
        columnNames: ['transaction_id'],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('evaluations', 'FK_TRANSACTION_EVALUATION');
    await queryRunner.dropIndex('evaluations', 'IDX_EVALUATION_TRANSACTION_ID');
    await queryRunner.dropColumn('evaluations', 'transaction_id');
  }
}
