import Store from '@domain/models/Store';
import Customer from '@domain/models/Customer';
import Employee from '@domain/models/Employee';
import Transaction from '@domain/models/Transaction';

import mockStoreModel from '@tests/domain/mocks/MockStoreModel';
import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';
import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';
import mockTransactionModel from '@tests/domain/mocks/MockTransactionModel';

import TestDatabaseConnectionManager from '@infra/database/helpers/typeOrm/TestDatabaseConnectionManager';

import TypeOrmStoreRepository from '@infra/database/repositories/store/typeOrm/TypeOrmStoreRepository';
import TypeOrmCustomerRepository from '@infra/database/repositories/customer/typeOrm/TypeOrmCustomerRepository';
import TypeOrmEmployeeRepository from '@infra/database/repositories/employee/typeOrm/TypeOrmEmployeeRepository';
import TypeOrmTransactionRepository from '@infra/database/repositories/transaction/typeOrm/TypeOrmTransactionRepository';
import TypeOrmEvaluationRepository from '@infra/database/repositories/evaluation/typeOrm/TypeOrmEvaluationRepository';

let testDatabaseConnectionManager: TestDatabaseConnectionManager;

let store: Store;
let customer: Customer;
let employee: Employee;
let transaction: Transaction;

let typeOrmStoreRepository: TypeOrmStoreRepository;
let typeOrmCustomerRepository: TypeOrmCustomerRepository;
let typeOrmEmployeeRepository: TypeOrmEmployeeRepository;
let typeOrmTransactionRepository: TypeOrmTransactionRepository;
let typeOrmEvaluationRepository: TypeOrmEvaluationRepository;

describe('TypeOrmEvaluationRepository', () => {
  beforeAll(async () => {
    testDatabaseConnectionManager = new TestDatabaseConnectionManager();

    typeOrmStoreRepository = new TypeOrmStoreRepository();
    typeOrmEmployeeRepository = new TypeOrmEmployeeRepository();
    typeOrmCustomerRepository = new TypeOrmCustomerRepository();
    typeOrmTransactionRepository = new TypeOrmTransactionRepository();

    typeOrmEvaluationRepository = new TypeOrmEvaluationRepository();

    await testDatabaseConnectionManager.getConnection();
  });

  afterAll(async () => {
    await testDatabaseConnectionManager.clear();
    await testDatabaseConnectionManager.close();
  });

  beforeEach(async () => {
    await testDatabaseConnectionManager.clear();

    store = await typeOrmStoreRepository.create(mockStoreModel.mock());

    customer = await typeOrmCustomerRepository.create(mockCustomerModel.mock());

    employee = await typeOrmEmployeeRepository.create(mockEmployeeModel.mock());

    transaction = await typeOrmTransactionRepository.create(
      mockTransactionModel.mock({
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
      }),
    );
  });

  describe('create()', () => {
    it('should be able to create evaluation', async () => {
      const createdEvaluation = await typeOrmEvaluationRepository.create({
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
      });

      expect(createdEvaluation).toEqual({
        id: createdEvaluation.id,
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
        created_at: createdEvaluation.created_at,
        updated_at: createdEvaluation.updated_at,
      });
    });
  });

  describe('findByAliasId()', () => {
    it('should be able to find evaluation', async () => {
      const createdEvaluation = await typeOrmEvaluationRepository.create({
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
      });

      const foundEvaluation = await typeOrmEvaluationRepository.findByAliasId('any_alias_id');

      expect(foundEvaluation).toEqual({
        id: createdEvaluation.id,
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
        created_at: createdEvaluation.created_at,
        updated_at: createdEvaluation.updated_at,
      });
    });

    it('should be able to return undefined if evaluation does not exists', async () => {
      await typeOrmEvaluationRepository.create({
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
      });

      const foundEvaluation = await typeOrmEvaluationRepository.findByAliasId(
        'f0abb9a7-0fb7-43f2-b228-6463cdac2aad',
      );

      expect(foundEvaluation).toBeUndefined();
    });
  });

  describe('findByTransactionId()', () => {
    it('should be able to find evaluation', async () => {
      const createdEvaluation = await typeOrmEvaluationRepository.create({
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
      });

      const foundEvaluation = await typeOrmEvaluationRepository.findByTransactionId(transaction.id);

      expect(foundEvaluation).toEqual({
        id: createdEvaluation.id,
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
        created_at: createdEvaluation.created_at,
        updated_at: createdEvaluation.updated_at,
      });
    });

    it('should be able to return undefined if evaluation does not exists', async () => {
      await typeOrmEvaluationRepository.create({
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
      });

      const foundEvaluation = await typeOrmEvaluationRepository.findByTransactionId(
        'f0abb9a7-0fb7-43f2-b228-6463cdac2aad',
      );

      expect(foundEvaluation).toBeUndefined();
    });
  });

  describe('findByTransaction()', () => {
    it('should be able to find evaluations', async () => {
      const createdEvaluation = await typeOrmEvaluationRepository.create({
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
      });

      const foundEvaluations = await typeOrmEvaluationRepository.findByTransaction({
        limit: 10,
        offset: 0,
        transaction_id: transaction.id,
      });

      expect(foundEvaluations).toEqual([
        {
          id: createdEvaluation.id,
          alias_id: 'any_alias_id',
          description: 'any_description',
          transaction_value: 10,
          transaction_id: transaction.id,
          transaction: {
            id: transaction.id,
            alias_id: transaction.alias_id,
            provider_date: transaction.provider_date,
            store_evaluation: transaction.store_evaluation,
            customer_id: transaction.customer_id,
            store_id: transaction.store_id,
            employee_id: transaction.employee_id,
            created_at: transaction.created_at,
            updated_at: transaction.updated_at,
          },
          created_at: createdEvaluation.created_at,
          updated_at: createdEvaluation.updated_at,
        },
      ]);
    });

    it('should be able to return empty array if does not exists evaluations', async () => {
      const foundTransactions = await typeOrmEvaluationRepository.findByTransaction({
        limit: 10,
        offset: 0,
        transaction_id: transaction.id,
      });

      expect(foundTransactions).toEqual([]);
    });

    it('should be able to return empty array if evaluation does not exists', async () => {
      const foundTransactions = await typeOrmEvaluationRepository.findByTransaction({
        limit: 10,
        offset: 0,
        transaction_id: transaction.id,
      });

      expect(foundTransactions).toEqual([]);
    });
  });

  describe('countByTransactionId()', () => {
    it('should be able to count evaluations', async () => {
      await typeOrmEvaluationRepository.create({
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
      });

      const count = await typeOrmEvaluationRepository.countByTransactionId(transaction.id);

      expect(count).toBe(1);
    });

    it('should be able to return 0 if does not exists evaluations', async () => {
      await typeOrmEvaluationRepository.create({
        alias_id: 'any_alias_id',
        description: 'any_description',
        transaction_value: 10,
        transaction_id: transaction.id,
      });

      const count = await typeOrmEvaluationRepository.countByTransactionId(
        'f0abb9a7-0fb7-43f2-b228-6463cdac2aad',
      );

      expect(count).toBe(0);
    });
  });
});
