import { getRepository } from 'typeorm';

import Evaluation from '@domain/models/Evaluation';
import EvaluationEntity from '@infra/database/entities/typeOrm/Evaluation';

import IEvaluationRepository from '@data/protocols/database/repositories/evaluation/IEvaluationRepository';

import ICreateEvaluationDTO from '@data/protocols/database/repositories/evaluation/dtos/ICreateEvaluationDTO';
import IFindByTransactionDTO from '@data/protocols/database/repositories/evaluation/dtos/IFindByTransactionDTO';

export default class TypeOrmEvaluationRepository implements IEvaluationRepository {
  public async create({
    alias_id,
    description,
    transaction_value,
    transaction_id,
  }: ICreateEvaluationDTO): Promise<Evaluation> {
    const createdEvaluation = getRepository(EvaluationEntity).create({
      alias_id,
      description,
      transaction_value,
      transaction_id,
    });

    await getRepository(EvaluationEntity).save(createdEvaluation);

    return createdEvaluation;
  }

  public async findByAliasId(alias_id: string): Promise<Evaluation | undefined> {
    const foundEvaluation = await getRepository(EvaluationEntity)
      .createQueryBuilder()
      .where('alias_id = :alias_id', { alias_id })
      .getOne();

    return foundEvaluation;
  }

  public async findByTransactionId(transaction_id: string): Promise<Evaluation | undefined> {
    const foundEvaluations = await getRepository(EvaluationEntity)
      .createQueryBuilder()
      .where('transaction_id = :transaction_id', { transaction_id })
      .getOne();

    return foundEvaluations;
  }

  public async findByTransaction({
    transaction_id,
    limit,
    offset,
  }: IFindByTransactionDTO): Promise<Evaluation[]> {
    const foundEvaluations = await getRepository(EvaluationEntity)
      .createQueryBuilder('evaluation')
      .leftJoinAndSelect('evaluation.transaction', 'transaction')
      .where('evaluation.transaction_id = :transaction_id', { transaction_id })
      .limit(limit)
      .offset(offset)
      .getMany();

    return foundEvaluations;
  }

  public async countByTransactionId(transaction_id: string): Promise<number> {
    const foundEvaluations = await getRepository(EvaluationEntity)
      .createQueryBuilder()
      .where('transaction_id = :transaction_id', { transaction_id })
      .getMany();

    const count = foundEvaluations.length;

    return count;
  }
}
