import Evaluation from '@domain/models/Evaluation';

import IEvaluationRepository from '@data/protocols/database/repositories/evaluation/IEvaluationRepository';

import ICreateEvaluationDTO from '@data/protocols/database/repositories/evaluation/dtos/ICreateEvaluationDTO';
import IFindByTransactionDTO from '@data/protocols/database/repositories/evaluation/dtos/IFindByTransactionDTO';

export default class FakeTypeOrmEvaluationRepository implements IEvaluationRepository {
  private readonly evaluations: Evaluation[] = [];

  public async create({
    alias_id,
    description,
    transaction_value,
    transaction_id,
  }: ICreateEvaluationDTO): Promise<Evaluation> {
    const createEvaluation = {
      id: Math.random().toString(),
      alias_id,
      description,
      transaction_value,
      transaction_id,
      created_at: new Date(),
      updated_at: new Date(),
    };

    this.evaluations.push(createEvaluation);

    return createEvaluation;
  }

  public async findByAliasId(alias_id: string): Promise<Evaluation | undefined> {
    const foundEvaluation = this.evaluations.find(evaluation => evaluation.alias_id === alias_id);

    return foundEvaluation;
  }

  public async findByTransactionId(transaction_id: string): Promise<Evaluation | undefined> {
    const foundEvaluation = this.evaluations.find(
      evaluation => evaluation.transaction_id === transaction_id,
    );

    return foundEvaluation;
  }

  public async findByTransaction({
    transaction_id,
    limit,
    offset,
  }: IFindByTransactionDTO): Promise<Evaluation[]> {
    const foundEvaluations = this.evaluations.filter(
      evaluation => evaluation.transaction_id === transaction_id,
    );

    return foundEvaluations;
  }

  public async countByTransactionId(transaction_id: string): Promise<number> {
    const foundCheckoutFees = this.evaluations.filter(
      evaluation => evaluation.transaction_id === transaction_id,
    );

    const count = foundCheckoutFees.length;

    return count;
  }
}
