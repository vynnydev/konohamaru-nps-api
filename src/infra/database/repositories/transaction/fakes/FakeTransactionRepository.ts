import Transaction from '@domain/models/Transaction';

import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';

import ICreateTransactionDTO from '@data/protocols/database/repositories/transaction/dtos/ICreateTransactionDTO';
import IUpdateTransactionDTO from '@data/protocols/database/repositories/transaction/dtos/IUpdateTransactionDTO';

export default class FakeTypeOrmTransactionRepository implements ITransactionRepository {
  private readonly Transactions: Transaction[] = [];

  public async create({
    alias_id,
    provider_date,
    store_evaluation,
    store_id,
    customer_id,
    employee_id,
  }: ICreateTransactionDTO): Promise<Transaction> {
    const createTransaction = {
      id: Math.random().toString(),
      alias_id,
      provider_date,
      store_evaluation,
      store_id,
      customer_id,
      employee_id,
      created_at: new Date(),
      updated_at: new Date(),
    };

    this.Transactions.push(createTransaction);

    return createTransaction;
  }

  public async findByAliasId(alias_id: string): Promise<Transaction | undefined> {
    const foundTransaction = this.Transactions.find(
      transaction => transaction.alias_id === alias_id,
    );

    return foundTransaction;
  }

  public async update({ id, data }: IUpdateTransactionDTO): Promise<Transaction | undefined> {
    const foundIndex = this.Transactions.findIndex(transaction => transaction.id === id);

    if (foundIndex < 0) return undefined;

    const foundTransaction = this.Transactions[foundIndex];

    Object.assign(foundTransaction, data);

    this.Transactions[foundIndex] = foundTransaction;

    return foundTransaction;
  }

  public async deleteById(id: string): Promise<void> {
    const foundIndex = this.Transactions.findIndex(transaction => transaction.id === id);

    if (foundIndex >= 0) this.Transactions.splice(foundIndex, 1);
  }
}
