import { getRepository } from 'typeorm';

import Transaction from '@domain/models/Transaction';
import TransactionEntity from '@infra/database/entities/typeOrm/Transaction';

import ITransactionRepository from '@data/protocols/database/repositories/transaction/ITransactionRepository';

import ICreateTransactionDTO from '@data/protocols/database/repositories/transaction/dtos/ICreateTransactionDTO';
import IUpdateTransactionDTO from '@data/protocols/database/repositories/transaction/dtos/IUpdateTransactionDTO';

export default class TypeOrmTransactionRepository implements ITransactionRepository {
  public async create({
    alias_id,
    provider_date,
    store_evaluation,
    store_id,
    customer_id,
    employee_id,
  }: ICreateTransactionDTO): Promise<Transaction> {
    const createdTransaction = getRepository(TransactionEntity).create({
      alias_id,
      provider_date,
      store_evaluation,
      store_id,
      customer_id,
      employee_id,
    });

    await getRepository(TransactionEntity).save(createdTransaction);

    return createdTransaction;
  }

  public async findByAliasId(alias_id: string): Promise<Transaction | undefined> {
    const foundTransaction = await getRepository(TransactionEntity)
      .createQueryBuilder()
      .where('alias_id = :alias_id', { alias_id })
      .getOne();

    return foundTransaction;
  }

  public async update({ id, data }: IUpdateTransactionDTO): Promise<Transaction | undefined> {
    await getRepository(TransactionEntity)
      .createQueryBuilder()
      .update()
      .set({ ...data })
      .where('id = :id', { id })
      .execute();

    const foundTransaction = await getRepository(TransactionEntity)
      .createQueryBuilder()
      .where('id = :id', { id })
      .getOne();

    return foundTransaction;
  }
}
