import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import Store from '@domain/models/Store';
import Customer from '@domain/models/Customer';
import Employee from '@domain/models/Employee';

import mockStoreModel from '@tests/domain/mocks/MockStoreModel';
import mockCustomerModel from '@tests/domain/mocks/MockCustomerModel';
import mockEmployeeModel from '@tests/domain/mocks/MockEmployeeModel';

import TestDatabaseConnectionManager from '@infra/database/helpers/typeOrm/TestDatabaseConnectionManager';

import TypeOrmStoreRepository from '@infra/database/repositories/store/typeOrm/TypeOrmStoreRepository';
import TypeOrmCustomerRepository from '@infra/database/repositories/customer/typeOrm/TypeOrmCustomerRepository';
import TypeOrmEmployeeRepository from '@infra/database/repositories/employee/typeOrm/TypeOrmEmployeeRepository';
import TypeOrmTransactionRepository from '@infra/database/repositories/transaction/typeOrm/TypeOrmTransactionRepository';

let testDatabaseConnectionManager: TestDatabaseConnectionManager;

let store: Store;
let customer: Customer;
let employee: Employee;

let typeOrmStoreRepository: TypeOrmStoreRepository;
let typeOrmEmployeeRepository: TypeOrmEmployeeRepository;
let typeOrmCustomerRepository: TypeOrmCustomerRepository;
let typeOrmTransactionRepository: TypeOrmTransactionRepository;

describe('TypeOrmTransactionRepository', () => {
  beforeAll(async () => {
    testDatabaseConnectionManager = new TestDatabaseConnectionManager();

    typeOrmStoreRepository = new TypeOrmStoreRepository();
    typeOrmEmployeeRepository = new TypeOrmEmployeeRepository();
    typeOrmCustomerRepository = new TypeOrmCustomerRepository();

    typeOrmTransactionRepository = new TypeOrmTransactionRepository();

    await testDatabaseConnectionManager.getConnection();
  });

  afterAll(async () => {
    await testDatabaseConnectionManager.clear();
    await testDatabaseConnectionManager.close();
  });

  beforeEach(async () => {
    await testDatabaseConnectionManager.clear();

    store = await typeOrmStoreRepository.create(mockStoreModel.mock());

    employee = await typeOrmEmployeeRepository.create(mockEmployeeModel.mock());

    customer = await typeOrmCustomerRepository.create(mockCustomerModel.mock());
  });

  describe('create()', () => {
    it('should be able to create nps experience', async () => {
      const createdTransaction = await typeOrmTransactionRepository.create({
        alias_id: 'any_alias_id',
        provider_date: new Date(2022, 3, 15),
        store_evaluation: EStoreEvaluation.REGULAR,
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
      });

      expect(createdTransaction).toEqual({
        id: createdTransaction.id,
        alias_id: 'any_alias_id',
        provider_date: new Date(2022, 3, 15),
        store_evaluation: EStoreEvaluation.REGULAR,
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
        created_at: createdTransaction.created_at,
        updated_at: createdTransaction.updated_at,
      });
    });
  });

  describe('findByAliasId()', () => {
    it('should be able to find nps experience', async () => {
      const createdTransaction = await typeOrmTransactionRepository.create({
        alias_id: 'any_alias_id',
        provider_date: new Date(2022, 3, 15),
        store_evaluation: EStoreEvaluation.REGULAR,
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
      });

      const foundTransaction = await typeOrmTransactionRepository.findByAliasId('any_alias_id');

      expect(foundTransaction).toEqual({
        id: createdTransaction.id,
        alias_id: 'any_alias_id',
        provider_date: new Date(2022, 3, 15),
        store_evaluation: EStoreEvaluation.REGULAR,
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
        created_at: createdTransaction.created_at,
        updated_at: createdTransaction.updated_at,
      });
    });

    it('should be able to return undefined if customer does not exists', async () => {
      await typeOrmTransactionRepository.create({
        alias_id: 'any_alias_id',
        provider_date: new Date(2022, 3, 15),
        store_evaluation: EStoreEvaluation.REGULAR,
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
      });

      const foundTransaction = await typeOrmTransactionRepository.findByAliasId(
        'non_existing_alias_id',
      );

      expect(foundTransaction).toBeUndefined();
    });
  });

  describe('update()', () => {
    it('should be able to update nps experience', async () => {
      const createdTransaction = await typeOrmTransactionRepository.create({
        alias_id: 'any_alias_id',
        provider_date: new Date(2022, 3, 15),
        store_evaluation: EStoreEvaluation.REGULAR,
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
      });

      const updatedTransaction = await typeOrmTransactionRepository.update({
        id: createdTransaction.id,
        data: { store_evaluation: EStoreEvaluation.OTIMO },
      });

      expect(updatedTransaction).toEqual({
        id: createdTransaction.id,
        alias_id: 'any_alias_id',
        provider_date: new Date(2022, 3, 15),
        store_evaluation: EStoreEvaluation.OTIMO,
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
        created_at: createdTransaction.created_at,
        updated_at: expect.any(Date),
      });
    });

    it('should be able to return undefined if nps experience does not exists', async () => {
      const createdTransaction = await typeOrmTransactionRepository.create({
        alias_id: 'any_alias_id',
        provider_date: new Date(2022, 3, 15),
        store_evaluation: EStoreEvaluation.REGULAR,
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
      });

      const updatedTransaction = await typeOrmTransactionRepository.update({
        id: 'f0abb9a7-0fb7-43f2-b228-6463cdac2aad',
        data: { store_evaluation: EStoreEvaluation.OTIMO },
      });

      const foundTransaction = await typeOrmTransactionRepository.findByAliasId(
        createdTransaction.alias_id,
      );

      expect(updatedTransaction).toBeUndefined();
      expect(foundTransaction).toEqual({
        id: createdTransaction.id,
        alias_id: 'any_alias_id',
        provider_date: new Date(2022, 3, 15),
        store_evaluation: EStoreEvaluation.REGULAR,
        store_id: store.id,
        customer_id: customer.id,
        employee_id: employee.id,
        created_at: createdTransaction.created_at,
        updated_at: createdTransaction.updated_at,
      });
    });
  });
});
