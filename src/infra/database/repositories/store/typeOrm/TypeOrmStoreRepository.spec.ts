import TestDatabaseConnectionManager from '@infra/database/helpers/typeOrm/TestDatabaseConnectionManager';

import TypeOrmStoreRepository from '@infra/database/repositories/store/typeOrm/TypeOrmStoreRepository';

let testDatabaseConnectionManager: TestDatabaseConnectionManager;

let typeOrmStoreRepository: TypeOrmStoreRepository;

describe('TypeOrmStoreRepository', () => {
  beforeAll(async () => {
    testDatabaseConnectionManager = new TestDatabaseConnectionManager();

    typeOrmStoreRepository = new TypeOrmStoreRepository();

    await testDatabaseConnectionManager.getConnection();
  });

  afterAll(async () => {
    await testDatabaseConnectionManager.clear();
    await testDatabaseConnectionManager.close();
  });

  beforeEach(async () => {
    await testDatabaseConnectionManager.clear();
  });

  describe('create()', () => {
    it('should be able to create store', async () => {
      const createdStore = await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      expect(createdStore).toEqual({
        id: createdStore.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        created_at: createdStore.created_at,
        updated_at: createdStore.updated_at,
      });
    });
  });

  describe('findById()', () => {
    it('should be able to find store', async () => {
      const createdStore = await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const foundStore = await typeOrmStoreRepository.findById(createdStore.id);

      expect(foundStore).toEqual({
        id: createdStore.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        created_at: createdStore.created_at,
        updated_at: createdStore.updated_at,
      });
    });

    it('should be able to return undefined if store does not exists', async () => {
      await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const foundStore = await typeOrmStoreRepository.findById(
        'f0abb9a7-0fb7-43f2-b228-6463cdac2aad',
      );

      expect(foundStore).toBeUndefined();
    });
  });

  describe('findAll()', () => {
    it('should be able to find stores', async () => {
      const createdStore = await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const foundStores = await typeOrmStoreRepository.findAll();

      expect(foundStores).toEqual([
        {
          id: createdStore.id,
          alias_id: 'any_alias_id',
          name: 'any_name',
          created_at: createdStore.created_at,
          updated_at: createdStore.updated_at,
        },
      ]);
    });

    it('should be able to return empty array if stores does not exists', async () => {
      const foundStores = await typeOrmStoreRepository.findAll();

      expect(foundStores).toEqual([]);
    });
  });

  describe('findByAliasId()', () => {
    it('should be able to find store', async () => {
      const createdStore = await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const foundStore = await typeOrmStoreRepository.findByAliasId('any_alias_id');

      expect(foundStore).toEqual({
        id: createdStore.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        created_at: createdStore.created_at,
        updated_at: createdStore.updated_at,
      });
    });

    it('should be able to return undefined if store does not exists', async () => {
      await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const foundStore = await typeOrmStoreRepository.findByAliasId('non_existing_alias_id');

      expect(foundStore).toBeUndefined();
    });
  });

  describe('findByName()', () => {
    it('should be able to find store', async () => {
      const createdStore = await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const foundStore = await typeOrmStoreRepository.findByName('any_name');

      expect(foundStore).toEqual({
        id: createdStore.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        created_at: createdStore.created_at,
        updated_at: createdStore.updated_at,
      });
    });

    it('should be able to return undefined if store does not exists', async () => {
      await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const foundStore = await typeOrmStoreRepository.findByName('non_existing_name');

      expect(foundStore).toBeUndefined();
    });
  });

  describe('update()', () => {
    it('should be able to update store', async () => {
      const createdStore = await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const updatedStore = await typeOrmStoreRepository.update({
        id: createdStore.id,
        data: { name: 'updated_name' },
      });

      expect(updatedStore).toEqual({
        id: createdStore.id,
        alias_id: 'any_alias_id',
        name: 'updated_name',
        created_at: createdStore.created_at,
        updated_at: expect.any(Date),
      });
    });

    it('should be able to return undefined if store does not exists', async () => {
      const createdStore = await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const updatedStore = await typeOrmStoreRepository.update({
        id: 'f0abb9a7-0fb7-43f2-b228-6463cdac2aad',
        data: { name: 'updated_name' },
      });

      const foundStore = await typeOrmStoreRepository.findById(createdStore.id);

      expect(updatedStore).toBeUndefined();
      expect(foundStore).toEqual({
        id: createdStore.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        created_at: createdStore.created_at,
        updated_at: createdStore.updated_at,
      });
    });
  });

  describe('deleteById()', () => {
    it('should be able to delete store', async () => {
      const createdStore = await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const error = await typeOrmStoreRepository.deleteById(createdStore.id);
      const foundStore = await typeOrmStoreRepository.findById(createdStore.id);

      expect(error).toBeFalsy();
      expect(foundStore).toBeUndefined();
    });

    it('should be able to do nothing if store does not exists', async () => {
      const createdStore = await typeOrmStoreRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
      });

      const error = await typeOrmStoreRepository.deleteById('f0abb9a7-0fb7-43f2-b228-6463cdac2aad');
      const foundStore = await typeOrmStoreRepository.findById(createdStore.id);

      expect(error).toBeFalsy();
      expect(foundStore).toEqual({
        id: createdStore.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        created_at: createdStore.created_at,
        updated_at: createdStore.updated_at,
      });
    });
  });
});
