import { getRepository } from 'typeorm';

import Store from '@domain/models/Store';
import StoreEntity from '@infra/database/entities/typeOrm/Store';

import IStoreRepository from '@data/protocols/database/repositories/store/IStoreRepository';

import ICreateStoreDTO from '@data/protocols/database/repositories/store/dtos/ICreateStoreDTO';
import IUpdateStoreDTO from '@data/protocols/database/repositories/store/dtos/IUpdateStoreDTO';

export default class TypeOrmStoreRepository implements IStoreRepository {
  public async create({ alias_id, name }: ICreateStoreDTO): Promise<Store> {
    const createdStore = getRepository(StoreEntity).create({
      alias_id,
      name,
    });

    await getRepository(StoreEntity).save(createdStore);

    return createdStore;
  }

  public async findByAliasId(alias_id: string): Promise<Store | undefined> {
    const foundStore = await getRepository(StoreEntity)
      .createQueryBuilder()
      .where('alias_id = :alias_id', { alias_id })
      .getOne();

    return foundStore;
  }

  public async findByName(name: string): Promise<Store | undefined> {
    const foundStore = await getRepository(StoreEntity)
      .createQueryBuilder()
      .where('name = :name', { name })
      .getOne();

    return foundStore;
  }

  public async findById(id: string): Promise<Store | undefined> {
    const foundStore = await getRepository(StoreEntity)
      .createQueryBuilder()
      .where('id = :id', { id })
      .getOne();

    return foundStore;
  }

  public async findAll(): Promise<Store[]> {
    const foundStores = await getRepository(StoreEntity).createQueryBuilder().getMany();

    return foundStores;
  }

  public async update({ id, data }: IUpdateStoreDTO): Promise<Store | undefined> {
    await getRepository(StoreEntity)
      .createQueryBuilder()
      .update()
      .set({ ...data })
      .where('id = :id', { id })
      .execute();

    const foundStore = await getRepository(StoreEntity)
      .createQueryBuilder()
      .where('id = :id', { id })
      .getOne();

    return foundStore;
  }

  public async deleteById(id: string): Promise<void> {
    await getRepository(StoreEntity)
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id })
      .execute();
  }
}
