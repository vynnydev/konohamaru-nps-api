import Store from '@domain/models/Store';

import IStoreRepository from '@data/protocols/database/repositories/store/IStoreRepository';

import ICreateStoreDTO from '@data/protocols/database/repositories/store/dtos/ICreateStoreDTO';
import IUpdateStoreDTO from '@data/protocols/database/repositories/store/dtos/IUpdateStoreDTO';

export default class FakeTypeOrmStoreRepository implements IStoreRepository {
  private readonly stores: Store[] = [];

  public async create({ alias_id, name }: ICreateStoreDTO): Promise<Store> {
    const createStore = {
      id: Math.random().toString(),
      alias_id,
      name,
      created_at: new Date(),
      updated_at: new Date(),
    };

    this.stores.push(createStore);

    return createStore;
  }

  public async findAll(): Promise<Store[]> {
    return this.stores;
  }

  public async findByName(name: string): Promise<Store | undefined> {
    const foundStore = this.stores.find(store => store.name === name);

    return foundStore;
  }

  public async findById(id: string): Promise<Store | undefined> {
    const foundStore = this.stores.find(store => store.id === id);

    return foundStore;
  }

  public async findByAliasId(alias_id: string): Promise<Store | undefined> {
    const foundStore = this.stores.find(store => store.alias_id === alias_id);

    return foundStore;
  }

  public async update({ id, data }: IUpdateStoreDTO): Promise<Store | undefined> {
    const foundIndex = this.stores.findIndex(store => store.id === id);

    if (foundIndex < 0) return undefined;

    const foundStore = this.stores[foundIndex];

    Object.assign(foundStore, data);

    this.stores[foundIndex] = foundStore;

    return foundStore;
  }

  public async deleteById(id: string): Promise<void> {
    const foundIndex = this.stores.findIndex(store => store.id === id);

    if (foundIndex >= 0) this.stores.splice(foundIndex, 1);
  }
}
