import TestDatabaseConnectionManager from '@infra/database/helpers/typeOrm/TestDatabaseConnectionManager';

import TypeOrmEmployeeRepository from '@infra/database/repositories/employee/typeOrm/TypeOrmEmployeeRepository';

let testDatabaseConnectionManager: TestDatabaseConnectionManager;

let typeOrmEmployeeRepository: TypeOrmEmployeeRepository;

describe('TypeOrmEmployeeRepository', () => {
  beforeAll(async () => {
    testDatabaseConnectionManager = new TestDatabaseConnectionManager();

    typeOrmEmployeeRepository = new TypeOrmEmployeeRepository();

    await testDatabaseConnectionManager.getConnection();
  });

  afterAll(async () => {
    await testDatabaseConnectionManager.clear();
    await testDatabaseConnectionManager.close();
  });

  beforeEach(async () => {
    await testDatabaseConnectionManager.clear();
  });

  describe('create()', () => {
    it('should be able to create employee', async () => {
      const createdEmployee = await typeOrmEmployeeRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        is_active: true,
      });

      expect(createdEmployee).toEqual({
        id: createdEmployee.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        is_active: true,
        created_at: createdEmployee.created_at,
        updated_at: createdEmployee.updated_at,
      });
    });
  });

  describe('findEmployees()', () => {
    it('should be able to find employees', async () => {
      const createdEmployee = await typeOrmEmployeeRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        is_active: true,
      });

      const foundEmployees = await typeOrmEmployeeRepository.findEmployees();

      expect(foundEmployees).toEqual([
        {
          id: createdEmployee.id,
          alias_id: 'any_alias_id',
          name: 'any_name',
          is_active: true,
          created_at: createdEmployee.created_at,
          updated_at: createdEmployee.updated_at,
        },
      ]);
    });

    it('should be able to return empty array if employees does not exists', async () => {
      const foundEmployees = await typeOrmEmployeeRepository.findEmployees();

      expect(foundEmployees).toEqual([]);
    });
  });

  describe('findByAliasId()', () => {
    it('should be able to find employee', async () => {
      const createdEmployee = await typeOrmEmployeeRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        is_active: true,
      });

      const foundEmployee = await typeOrmEmployeeRepository.findByAliasId('any_alias_id');

      expect(foundEmployee).toEqual({
        id: createdEmployee.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        is_active: true,
        created_at: createdEmployee.created_at,
        updated_at: createdEmployee.updated_at,
      });
    });

    it('should be able to return undefined if employee does not exists', async () => {
      await typeOrmEmployeeRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        is_active: true,
      });

      const foundEmployee = await typeOrmEmployeeRepository.findByAliasId('non_existing_alias_id');

      expect(foundEmployee).toBeUndefined();
    });
  });

  describe('update()', () => {
    it('should be able to update employee', async () => {
      const createdEmployee = await typeOrmEmployeeRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        is_active: true,
      });

      const updatedEmployee = await typeOrmEmployeeRepository.update({
        id: createdEmployee.id,
        data: { name: 'updated_name' },
      });

      expect(updatedEmployee).toEqual({
        id: createdEmployee.id,
        alias_id: 'any_alias_id',
        name: 'updated_name',
        is_active: true,
        created_at: createdEmployee.created_at,
        updated_at: expect.any(Date),
      });
    });

    it('should be able to return undefined if employee does not exists', async () => {
      const createdEmployee = await typeOrmEmployeeRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        is_active: true,
      });

      const updatedEmployee = await typeOrmEmployeeRepository.update({
        id: 'f0abb9a7-0fb7-43f2-b228-6463cdac2aad',
        data: { name: 'updated_name' },
      });

      const foundEmployee = await typeOrmEmployeeRepository.findByAliasId('any_alias_id');

      expect(updatedEmployee).toBeUndefined();
      expect(foundEmployee).toEqual({
        id: createdEmployee.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        is_active: true,
        created_at: createdEmployee.created_at,
        updated_at: createdEmployee.updated_at,
      });
    });
  });
});
