import { getRepository } from 'typeorm';

import Employee from '@domain/models/Employee';
import EmployeeEntity from '@infra/database/entities/typeOrm/Employee';

import IEmployeeRepository from '@data/protocols/database/repositories/employee/IEmployeeRepository';

import ICreateEmployeeDTO from '@data/protocols/database/repositories/employee/dtos/ICreateEmployeeDTO';
import IUpdateEmployeeDTO from '@data/protocols/database/repositories/employee/dtos/IUpdateEmployeeDTO';

export default class TypeOrmEmployeeRepository implements IEmployeeRepository {
  public async create({ alias_id, name, is_active }: ICreateEmployeeDTO): Promise<Employee> {
    const createdEmployee = getRepository(EmployeeEntity).create({
      alias_id,
      name,
      is_active,
    });

    await getRepository(EmployeeEntity).save(createdEmployee);

    return createdEmployee;
  }

  public async findByAliasId(alias_id: string): Promise<Employee | undefined> {
    const foundEmployee = await getRepository(EmployeeEntity)
      .createQueryBuilder()
      .where('alias_id = :alias_id', { alias_id })
      .getOne();

    return foundEmployee;
  }

  public async findEmployees(): Promise<Employee[]> {
    const foundEmployees = await getRepository(EmployeeEntity).createQueryBuilder().getMany();

    return foundEmployees;
  }

  public async update({ id, data }: IUpdateEmployeeDTO): Promise<Employee | undefined> {
    await getRepository(EmployeeEntity)
      .createQueryBuilder()
      .update()
      .set({ ...data })
      .where('id = :id', { id })
      .execute();

    const foundEmployee = await getRepository(EmployeeEntity)
      .createQueryBuilder()
      .where('id = :id', { id })
      .getOne();

    return foundEmployee;
  }
}
