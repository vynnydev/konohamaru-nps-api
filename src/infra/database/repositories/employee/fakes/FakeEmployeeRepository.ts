import Employee from '@domain/models/Employee';

import IEmployeeRepository from '@data/protocols/database/repositories/employee/IEmployeeRepository';

import ICreateEmployeeDTO from '@data/protocols/database/repositories/employee/dtos/ICreateEmployeeDTO';
import IUpdateEmployeeDTO from '@data/protocols/database/repositories/employee/dtos/IUpdateEmployeeDTO';

export default class FakeTypeOrmEmployeeRepository implements IEmployeeRepository {
  private readonly employees: Employee[] = [];

  public async create({ alias_id, name, is_active }: ICreateEmployeeDTO): Promise<Employee> {
    const createEmployee = {
      id: Math.random().toString(),
      alias_id,
      name,
      is_active,
      created_at: new Date(),
      updated_at: new Date(),
    };

    this.employees.push(createEmployee);

    return createEmployee;
  }

  public async findEmployees(): Promise<Employee[]> {
    return this.employees;
  }

  public async findByAliasId(alias_id: string): Promise<Employee | undefined> {
    const foundEmployee = this.employees.find(employee => employee.alias_id === alias_id);

    return foundEmployee;
  }

  public async update({ id, data }: IUpdateEmployeeDTO): Promise<Employee | undefined> {
    const foundIndex = this.employees.findIndex(employee => employee.id === id);

    if (foundIndex < 0) return undefined;

    const foundEmployee = this.employees[foundIndex];

    Object.assign(foundEmployee, data);

    this.employees[foundIndex] = foundEmployee;

    return foundEmployee;
  }
}
