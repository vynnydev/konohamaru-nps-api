import Customer from '@domain/models/Customer';

import ICustomerRepository from '@data/protocols/database/repositories/customer/ICustomerRepository';

import ICreateCustomerDTO from '@data/protocols/database/repositories/customer/dtos/ICreateCustomerDTO';
import IUpdateCustomerDTO from '@data/protocols/database/repositories/customer/dtos/IUpdateCustomerDTO';

export default class FakeTypeOrmCustomerRepository implements ICustomerRepository {
  private readonly customers: Customer[] = [];

  public async create({
    alias_id,
    name,
    cpf,
    email,
    phone_number,
  }: ICreateCustomerDTO): Promise<Customer> {
    const createCustomer = {
      id: Math.random().toString(),
      alias_id,
      name,
      cpf,
      email,
      phone_number,
      created_at: new Date(),
      updated_at: new Date(),
    };

    this.customers.push(createCustomer);

    return createCustomer;
  }

  public async findCustomers(): Promise<Customer[]> {
    return this.customers;
  }

  public async findByAliasId(alias_id: string): Promise<Customer | undefined> {
    const foundCustomer = this.customers.find(customer => customer.alias_id === alias_id);

    return foundCustomer;
  }

  public async update({ id, data }: IUpdateCustomerDTO): Promise<Customer | undefined> {
    const foundIndex = this.customers.findIndex(customer => customer.id === id);

    if (foundIndex < 0) return undefined;

    const foundCustomer = this.customers[foundIndex];

    Object.assign(foundCustomer, data);

    this.customers[foundIndex] = foundCustomer;

    return foundCustomer;
  }

  public async deleteById(id: string): Promise<void> {
    const foundIndex = this.customers.findIndex(customer => customer.id === id);

    if (foundIndex >= 0) this.customers.splice(foundIndex, 1);
  }
}
