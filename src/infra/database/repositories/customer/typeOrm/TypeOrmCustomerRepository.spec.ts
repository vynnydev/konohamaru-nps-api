import TestDatabaseConnectionManager from '@infra/database/helpers/typeOrm/TestDatabaseConnectionManager';

import TypeOrmCustomerRepository from '@infra/database/repositories/customer/typeOrm/TypeOrmCustomerRepository';

let testDatabaseConnectionManager: TestDatabaseConnectionManager;

let typeOrmCustomerRepository: TypeOrmCustomerRepository;

describe('TypeOrmCustomerRepository', () => {
  beforeAll(async () => {
    testDatabaseConnectionManager = new TestDatabaseConnectionManager();

    typeOrmCustomerRepository = new TypeOrmCustomerRepository();

    await testDatabaseConnectionManager.getConnection();
  });

  afterAll(async () => {
    await testDatabaseConnectionManager.clear();
    await testDatabaseConnectionManager.close();
  });

  beforeEach(async () => {
    await testDatabaseConnectionManager.clear();
  });

  describe('create()', () => {
    it('should be able to create customer', async () => {
      const createdCustomer = await typeOrmCustomerRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
      });

      expect(createdCustomer).toEqual({
        id: createdCustomer.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
        created_at: createdCustomer.created_at,
        updated_at: createdCustomer.updated_at,
      });
    });
  });

  describe('findCustomers()', () => {
    it('should be able to find customers', async () => {
      const createdCustomer = await typeOrmCustomerRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
      });

      const foundCustomers = await typeOrmCustomerRepository.findCustomers();

      expect(foundCustomers).toEqual([
        {
          id: createdCustomer.id,
          alias_id: 'any_alias_id',
          name: 'any_name',
          cpf: 'any_cpf',
          email: 'any_email',
          phone_number: 'any_phone_number',
          created_at: createdCustomer.created_at,
          updated_at: createdCustomer.updated_at,
        },
      ]);
    });

    it('should be able to return empty array if customers does not exists', async () => {
      const foundCustomers = await typeOrmCustomerRepository.findCustomers();

      expect(foundCustomers).toEqual([]);
    });
  });

  describe('findByAliasId()', () => {
    it('should be able to find customer', async () => {
      const createdCustomer = await typeOrmCustomerRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
      });

      const foundCustomer = await typeOrmCustomerRepository.findByAliasId('any_alias_id');

      expect(foundCustomer).toEqual({
        id: createdCustomer.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
        created_at: createdCustomer.created_at,
        updated_at: createdCustomer.updated_at,
      });
    });

    it('should be able to return undefined if customer does not exists', async () => {
      await typeOrmCustomerRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
      });

      const foundCustomer = await typeOrmCustomerRepository.findByAliasId('non_existing_alias_id');

      expect(foundCustomer).toBeUndefined();
    });
  });

  describe('update()', () => {
    it('should be able to update customer', async () => {
      const createdCustomer = await typeOrmCustomerRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
      });

      const updatedCustomer = await typeOrmCustomerRepository.update({
        id: createdCustomer.id,
        data: { name: 'updated_name' },
      });

      expect(updatedCustomer).toEqual({
        id: createdCustomer.id,
        alias_id: 'any_alias_id',
        name: 'updated_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
        created_at: createdCustomer.created_at,
        updated_at: expect.any(Date),
      });
    });

    it('should be able to return undefined if customer does not exists', async () => {
      const createdCustomer = await typeOrmCustomerRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
      });

      const updatedCustomer = await typeOrmCustomerRepository.update({
        id: 'f0abb9a7-0fb7-43f2-b228-6463cdac2aad',
        data: { name: 'updated_name' },
      });

      const foundCustomer = await typeOrmCustomerRepository.findByAliasId(createdCustomer.alias_id);

      expect(updatedCustomer).toBeUndefined();
      expect(foundCustomer).toEqual({
        id: createdCustomer.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
        created_at: createdCustomer.created_at,
        updated_at: createdCustomer.updated_at,
      });
    });
  });

  describe('deleteById()', () => {
    it('should be able to delete customer', async () => {
      const createdCustomer = await typeOrmCustomerRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
      });

      const error = await typeOrmCustomerRepository.deleteById(createdCustomer.id);
      const foundCustomer = await typeOrmCustomerRepository.findByAliasId(createdCustomer.alias_id);

      expect(error).toBeFalsy();
      expect(foundCustomer).toBeUndefined();
    });

    it('should be able to do nothing if customer does not exists', async () => {
      const createdCustomer = await typeOrmCustomerRepository.create({
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
      });

      const error = await typeOrmCustomerRepository.deleteById(
        'f0abb9a7-0fb7-43f2-b228-6463cdac2aad',
      );
      const foundCustomer = await typeOrmCustomerRepository.findByAliasId(createdCustomer.alias_id);

      expect(error).toBeFalsy();
      expect(foundCustomer).toEqual({
        id: createdCustomer.id,
        alias_id: 'any_alias_id',
        name: 'any_name',
        cpf: 'any_cpf',
        email: 'any_email',
        phone_number: 'any_phone_number',
        created_at: createdCustomer.created_at,
        updated_at: createdCustomer.updated_at,
      });
    });
  });
});
