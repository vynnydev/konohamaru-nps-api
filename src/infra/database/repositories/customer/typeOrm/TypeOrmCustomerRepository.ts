import { getRepository } from 'typeorm';

import Customer from '@domain/models/Customer';
import CustomerEntity from '@infra/database/entities/typeOrm/Customer';

import ICustomerRepository from '@data/protocols/database/repositories/customer/ICustomerRepository';

import ICreateCustomerDTO from '@data/protocols/database/repositories/customer/dtos/ICreateCustomerDTO';
import IUpdateCustomerDTO from '@data/protocols/database/repositories/customer/dtos/IUpdateCustomerDTO';

export default class TypeOrmCustomerRepository implements ICustomerRepository {
  public async create({
    alias_id,
    cpf,
    email,
    name,
    phone_number,
  }: ICreateCustomerDTO): Promise<Customer> {
    const createdCustomer = getRepository(CustomerEntity).create({
      alias_id,
      cpf,
      email,
      name,
      phone_number,
    });

    await getRepository(CustomerEntity).save(createdCustomer);

    return createdCustomer;
  }

  public async findByAliasId(alias_id: string): Promise<Customer | undefined> {
    const foundCustomer = await getRepository(CustomerEntity)
      .createQueryBuilder()
      .where('alias_id = :alias_id', { alias_id })
      .getOne();

    return foundCustomer;
  }

  public async findCustomers(): Promise<Customer[]> {
    const foundCustomers = await getRepository(CustomerEntity).createQueryBuilder().getMany();

    return foundCustomers;
  }

  public async update({ id, data }: IUpdateCustomerDTO): Promise<Customer | undefined> {
    await getRepository(CustomerEntity)
      .createQueryBuilder()
      .update()
      .set({ ...data })
      .where('id = :id', { id })
      .execute();

    const foundCustomer = await getRepository(CustomerEntity)
      .createQueryBuilder()
      .where('id = :id', { id })
      .getOne();

    return foundCustomer;
  }

  public async deleteById(id: string): Promise<void> {
    await getRepository(CustomerEntity)
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id })
      .execute();
  }
}
