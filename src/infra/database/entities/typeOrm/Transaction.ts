import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import Transaction from '@domain/models/Transaction';

import StoreEntity from '@infra/database/entities/typeOrm/Store';
import CustomerEntity from '@infra/database/entities/typeOrm/Customer';
import EmployeeEntity from '@infra/database/entities/typeOrm/Employee';

@Entity('transactions')
export default class TransactionEntity extends Transaction {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', unique: true })
  alias_id: string;

  @Column({ type: 'timestamp with time zone' })
  provider_date: Date;

  @Column({ type: 'enum', enum: EStoreEvaluation })
  store_evaluation: EStoreEvaluation;

  @ManyToOne(() => StoreEntity)
  @JoinColumn({ name: 'store_id' })
  store: StoreEntity;

  @Column({ type: 'varchar' })
  store_id: string;

  @ManyToOne(() => EmployeeEntity)
  @JoinColumn({ name: 'employee_id' })
  employee: EmployeeEntity;

  @Column({ type: 'varchar' })
  employee_id: string;

  @ManyToOne(() => CustomerEntity)
  @JoinColumn({ name: 'customer_id' })
  customer: CustomerEntity;

  @Column({ type: 'varchar' })
  customer_id: string;

  @CreateDateColumn({ type: 'timestamp with time zone' })
  created_at: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone' })
  updated_at: Date;
}
