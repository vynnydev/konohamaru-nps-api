import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  OneToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

import Evaluation from '@domain/models/Evaluation';
import Transaction from '@infra/database/entities/typeOrm/Transaction';

@Entity('evaluations')
export default class EvaluationEntity extends Evaluation {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', unique: true })
  alias_id: string;

  @Column({ type: 'varchar' })
  description: string;

  @Column({ type: 'int' })
  transaction_value: number;

  @OneToOne(() => Transaction)
  @JoinColumn({ name: 'transaction_id' })
  transaction: Transaction;

  @Column({ type: 'varchar' })
  transaction_id: string;

  @CreateDateColumn({ type: 'timestamp with time zone' })
  created_at: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone' })
  updated_at: Date;
}
