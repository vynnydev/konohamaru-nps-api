import 'dotenv/config';

import Customer from '@infra/database/entities/typeOrm/Customer';
import Employee from '@infra/database/entities/typeOrm/Employee';
import Evaluation from '@infra/database/entities/typeOrm/Evaluation';
import Transaction from '@infra/database/entities/typeOrm/Transaction';
import Store from '@infra/database/entities/typeOrm/Store';

const entities = [Customer, Employee, Evaluation, Transaction, Store];
const config = {
  default: {
    type: 'postgres',
    host: process.env.PG_HOST as string,
    port: Number(process.env.PG_PORT),
    username: process.env.PG_USER as string,
    password: process.env.PG_PASSWORD as string,
    database: 'main_database',
    migrationsTableName: 'migrations',
    dropSchema: false,
    migrationsRun: false,
    synchronize: true,
    NODE_ENV: 'default',
    entities,
  },
  test: {
    type: 'postgres',
    host: process.env.PG_TEST_HOST as string,
    port: Number(process.env.PG_TEST_PORT),
    username: process.env.PG_TEST_USER as string,
    password: process.env.PG_TEST_PASSWORD as string,
    database: 'test_database',
    dropSchema: true,
    migrationsRun: true,
    NODE_ENV: 'test',
    entities,
  },
};

const environment = process.env.NODE_ENV?.trim() === 'test' ? 'test' : 'default';

const typeOrmConfig = config[environment];

export default { ...typeOrmConfig };
