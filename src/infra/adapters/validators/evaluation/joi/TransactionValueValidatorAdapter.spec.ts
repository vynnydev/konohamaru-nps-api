import TransactionValueValidatorAdapter from '@infra/adapters/validators/evaluation/joi/TransactionValueValidatorAdapter';

let transactionValueValidatorAdapter: TransactionValueValidatorAdapter;

describe('TransactionValueValidatorAdapter', () => {
  beforeEach(() => {
    transactionValueValidatorAdapter = new TransactionValueValidatorAdapter();
  });

  it('should be able to return false if has validation error', () => {
    const invalidTransactionValue = -1;

    const isValid = transactionValueValidatorAdapter.isValid(invalidTransactionValue);

    expect(isValid).toBeFalsy();
  });

  it('should be able to return true if do not has validation error', () => {
    const validTransactionValue = 10;

    const isValid = transactionValueValidatorAdapter.isValid(validTransactionValue);

    expect(isValid).toBeTruthy();
  });
});
