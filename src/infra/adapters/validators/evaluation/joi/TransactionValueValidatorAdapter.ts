import Joi from 'joi';

import ITransactionValueValidator from '@utils/validation/protocols/evaluation/ITransactionValueValidator';

export default class TransactionValueValidatorAdapter implements ITransactionValueValidator {
  public isValid(transaction_value: any): boolean {
    const schema = Joi.object().keys({
      transaction_value: Joi.number().positive().allow(0).required(),
    });

    const { error } = schema.validate({ transaction_value });

    if (error) return false;

    return true;
  }
}
