import ITransactionValueValidator from '@utils/validation/protocols/evaluation/ITransactionValueValidator';

export default class FakeTransactionValueValidatorAdapter implements ITransactionValueValidator {
  public isValid(transaction_value: any): boolean {
    return true;
  }
}
