import Joi from 'joi';

import IPageValidator from '@utils/validation/protocols/common/IPageValidator';

export default class PageValidatorAdapter implements IPageValidator {
  public isValid(page: any): boolean {
    const schema = Joi.object().keys({
      page: Joi.number().positive().required(),
    });

    const { error } = schema.validate({ page });

    if (error) return false;

    return true;
  }
}
