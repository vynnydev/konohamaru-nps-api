import DescriptionValidatorAdapter from '@infra/adapters/validators/common/joi/DescriptionValidatorAdapter';

let descriptionValidatorAdapter: DescriptionValidatorAdapter;

describe('DescriptionValidatorAdapter', () => {
  beforeEach(() => {
    descriptionValidatorAdapter = new DescriptionValidatorAdapter();
  });

  it('should be able to return false if has validation error', () => {
    const invalidDescription = '';

    const isValid = descriptionValidatorAdapter.isValid(invalidDescription);

    expect(isValid).toBeFalsy();
  });

  it('should be able to return true if has no validation error', () => {
    const validDescription = 'valid_description';

    const isValid = descriptionValidatorAdapter.isValid(validDescription);

    expect(isValid).toBeTruthy();
  });
});
