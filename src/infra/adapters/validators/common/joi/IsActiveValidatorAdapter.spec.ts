import IsActiveValidatorAdapter from '@infra/adapters/validators/common/joi/IsActiveValidatorAdapter';

let isActiveValidatorAdapter: IsActiveValidatorAdapter;

describe('IsActiveValidatorAdapter', () => {
  beforeEach(() => {
    isActiveValidatorAdapter = new IsActiveValidatorAdapter();
  });

  it('should be able to return false if has validation error', () => {
    const invalidIsActive = '';

    const isValid = isActiveValidatorAdapter.isValid(invalidIsActive);

    expect(isValid).toBeFalsy();
  });

  it('should be able to return true if do not has validation error', () => {
    const validIsActive = true;

    const isValid = isActiveValidatorAdapter.isValid(validIsActive);

    expect(isValid).toBeTruthy();
  });
});
