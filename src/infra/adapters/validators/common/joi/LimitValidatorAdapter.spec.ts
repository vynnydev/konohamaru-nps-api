import LimitValidatorAdapter from '@infra/adapters/validators/common/joi/LimitValidatorAdapter';

let limitValidatorAdapter: LimitValidatorAdapter;

describe('AccessTokenValidatorAdapter', () => {
  beforeEach(() => {
    limitValidatorAdapter = new LimitValidatorAdapter();
  });

  it('should be able to return false if has validation error', () => {
    const invalidLimit = '';

    const isValid = limitValidatorAdapter.isValid(invalidLimit);

    expect(isValid).toBeFalsy();
  });

  it('should be able to return true if has no validation error', () => {
    const validLimit = 'valid_limit';

    const isValid = limitValidatorAdapter.isValid(validLimit);

    expect(isValid).toBeTruthy();
  });
});
