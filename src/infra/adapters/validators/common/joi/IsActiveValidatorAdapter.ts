import Joi from 'joi';

import IIsActiveValidator from '@utils/validation/protocols/common/IIsActiveValidator';

export default class IsActiveValidatorAdapter implements IIsActiveValidator {
  public isValid(is_active: any): boolean {
    const schema = Joi.object().keys({
      is_active: Joi.boolean().required(),
    });

    const { error } = schema.validate({ is_active });

    if (error) return false;

    return true;
  }
}
