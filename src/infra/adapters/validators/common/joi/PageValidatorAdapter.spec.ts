import PageValidatorAdapter from '@infra/adapters/validators/common/joi/PageValidatorAdapter';

let pageValidatorAdapter: PageValidatorAdapter;

describe('PageValidatorAdapter', () => {
  beforeEach(() => {
    pageValidatorAdapter = new PageValidatorAdapter();
  });

  it('should be able to return false if has validation error', () => {
    const invalidPage = -1;

    const isValid = pageValidatorAdapter.isValid(invalidPage);

    expect(isValid).toBeFalsy();
  });

  it('should be able to return true if do not has validation error', () => {
    const validPage = 10;

    const isValid = pageValidatorAdapter.isValid(validPage);

    expect(isValid).toBeTruthy();
  });
});
