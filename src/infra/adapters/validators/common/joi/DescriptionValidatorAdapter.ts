import Joi from 'joi';

import IDescriptionValidator from '@utils/validation/protocols/common/IDescriptionValidator';

export default class DescriptionValidatorAdapter implements IDescriptionValidator {
  public isValid(description: any): boolean {
    const schema = Joi.object().keys({
      description: Joi.string().min(2).max(1000).required(),
    });

    const { error } = schema.validate({ description });

    if (error) return false;

    return true;
  }
}
