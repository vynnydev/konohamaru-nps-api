import Joi from 'joi';

import ILimitValidator from '@utils/validation/protocols/common/ILimitValidator';

export default class LimitValidatorAdapter implements ILimitValidator {
  public isValid(limit: any): boolean {
    const schema = Joi.object().keys({
      limit: Joi.string().min(2).max(100).required(),
    });

    const { error } = schema.validate({ limit });

    if (error) return false;

    return true;
  }
}
