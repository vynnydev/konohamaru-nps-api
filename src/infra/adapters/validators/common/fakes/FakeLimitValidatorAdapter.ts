import ILimitValidator from '@utils/validation/protocols/common/ILimitValidator';

export default class FakeLimitValidatorAdapter implements ILimitValidator {
  public isValid(limit: any): boolean {
    return true;
  }
}
