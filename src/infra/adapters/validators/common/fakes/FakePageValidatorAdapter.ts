import IPageValidator from '@utils/validation/protocols/common/IPageValidator';

export default class FakePageValidatorAdapter implements IPageValidator {
  public isValid(page: any): boolean {
    return true;
  }
}
