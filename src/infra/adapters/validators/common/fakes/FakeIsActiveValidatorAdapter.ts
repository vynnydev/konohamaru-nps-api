import IIsActiveValidator from '@utils/validation/protocols/common/IIsActiveValidator';

export default class FakeIsActiveValidatorAdapter implements IIsActiveValidator {
  public isValid(is_active: string): boolean {
    return true;
  }
}
