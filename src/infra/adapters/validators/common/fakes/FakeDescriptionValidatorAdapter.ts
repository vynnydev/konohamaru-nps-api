import IDescriptionValidator from '@utils/validation/protocols/common/IDescriptionValidator';

export default class FakeDescriptionValidatorAdapter implements IDescriptionValidator {
  public isValid(description: any): boolean {
    return true;
  }
}
