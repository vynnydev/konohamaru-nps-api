import ProviderDateValidatorAdapter from '@infra/adapters/validators/transaction/joi/ProviderDateValidatorAdapter';

let providerDateValidatorAdapter: ProviderDateValidatorAdapter;

describe('ProviderDateValidatorAdapter', () => {
  beforeEach(() => {
    providerDateValidatorAdapter = new ProviderDateValidatorAdapter();
  });

  it('should be able to return false if has validation error', () => {
    const invalidProviderDate = '2021-03-19T03:00:00Z';

    const isValid = providerDateValidatorAdapter.isValid(invalidProviderDate);

    expect(isValid).toBeFalsy();
  });

  it('should be able to return true if do not has validation error', () => {
    const validProviderDate = '2021-03-19';

    const isValid = providerDateValidatorAdapter.isValid(validProviderDate);

    expect(isValid).toBeTruthy();
  });
});
