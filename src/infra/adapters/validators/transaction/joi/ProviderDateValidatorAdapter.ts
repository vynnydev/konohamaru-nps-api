import Joi from 'joi';
import JoiDate from '@joi/date';

import IProviderDateValidator from '@utils/validation/protocols/transaction/IProviderDateValidator';

const extendedJoi = Joi.extend(JoiDate);

export default class ProviderDateValidatorAdapter implements IProviderDateValidator {
  public isValid(provider_date: any): boolean {
    const schema = extendedJoi.object().keys({
      provider_date: extendedJoi.date().format('YYYY-MM-DD').required(),
    });

    const { error } = schema.validate({ provider_date });

    if (error) return false;

    return true;
  }
}
