import Joi from 'joi';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import IStoreEvaluationValidator from '@utils/validation/protocols/transaction/IStoreEvaluationValidator';

export default class StoreEvaluationValidatorAdapter implements IStoreEvaluationValidator {
  public isValid(store_evaluation: any): boolean {
    const schema = Joi.object().keys({
      store_evaluation: Joi.string()
        .valid(
          EStoreEvaluation.RUIM,
          EStoreEvaluation.REGULAR,
          EStoreEvaluation.BOM,
          EStoreEvaluation.OTIMO,
        )
        .required(),
    });

    const { error } = schema.validate({ store_evaluation });

    if (error) return false;

    return true;
  }
}
