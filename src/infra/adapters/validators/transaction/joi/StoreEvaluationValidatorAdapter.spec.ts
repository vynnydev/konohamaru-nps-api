import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import StoreEvaluationValidatorAdapter from '@infra/adapters/validators/transaction/joi/StoreEvaluationValidatorAdapter';

let transactionValueValidatorAdapter: StoreEvaluationValidatorAdapter;

describe('StoreEvaluationValidatorAdapter', () => {
  beforeEach(() => {
    transactionValueValidatorAdapter = new StoreEvaluationValidatorAdapter();
  });

  it('should be able to return false if has validation error', () => {
    const invalidStoreEvaluation = -1;

    const isValid = transactionValueValidatorAdapter.isValid(invalidStoreEvaluation);

    expect(isValid).toBeFalsy();
  });

  it('should be able to return true if do not has validation error', () => {
    const validStoreEvaluation = EStoreEvaluation.OTIMO;

    const isValid = transactionValueValidatorAdapter.isValid(validStoreEvaluation);

    expect(isValid).toBeTruthy();
  });
});
