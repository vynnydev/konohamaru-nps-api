import IProviderDateValidator from '@utils/validation/protocols/transaction/IProviderDateValidator';

export default class FakeProviderDateValidatorAdapter implements IProviderDateValidator {
  public isValid(provider_date: any): boolean {
    return true;
  }
}
