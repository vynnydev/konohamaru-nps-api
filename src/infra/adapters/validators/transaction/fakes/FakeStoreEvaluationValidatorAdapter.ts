import IStoreEvaluationValidator from '@utils/validation/protocols/transaction/IStoreEvaluationValidator';

export default class FakeStoreEvaluationValidatorAdapter implements IStoreEvaluationValidator {
  public isValid(store_evaluation: any): boolean {
    return true;
  }
}
