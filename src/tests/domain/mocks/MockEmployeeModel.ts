import { faker } from '@faker-js/faker';

import Employee from '@domain/models/Employee';

import IMockEmployeeModelDTO from '@tests/domain/mocks/dtos/MockEmployeeModelDTO';

const mock = (data?: IMockEmployeeModelDTO): Employee => {
  const employeeMock = {
    id: faker.datatype.uuid(),
    alias_id: `employee-${faker.datatype.uuid()}`,
    name: faker.commerce.productName(),
    is_active: faker.datatype.boolean(),
    created_at: faker.date.recent(),
    updated_at: faker.date.recent(),
    ...data,
  };

  return employeeMock;
};

export default { mock };
