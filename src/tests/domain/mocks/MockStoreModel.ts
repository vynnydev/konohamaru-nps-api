import { faker } from '@faker-js/faker';

import Store from '@domain/models/Store';

import IMockStoreModelDTO from '@tests/domain/mocks/dtos/MockStoreModelDTO';

const mock = (data?: IMockStoreModelDTO): Store => {
  const storeMock = {
    id: faker.datatype.uuid(),
    alias_id: `store-${faker.datatype.uuid()}`,
    name: faker.commerce.productName(),
    created_at: faker.date.recent(),
    updated_at: faker.date.recent(),
    ...data,
  };

  return storeMock;
};

export default { mock };
