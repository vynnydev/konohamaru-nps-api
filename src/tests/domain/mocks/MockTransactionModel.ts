import { faker } from '@faker-js/faker';

import { EStoreEvaluation } from '@domain/enums/transaction/EStoreEvaluation';

import Transaction from '@domain/models/Transaction';

import IMockTransactionModelDTO from '@tests/domain/mocks/dtos/MockTransactionModelDTO';

const mock = (data?: IMockTransactionModelDTO): Transaction => {
  const transactionMock = {
    id: faker.datatype.uuid(),
    alias_id: `transaction-${faker.datatype.uuid()}`,
    provider_date: faker.date.recent(),
    store_evaluation: EStoreEvaluation.OTIMO,
    store_id: faker.datatype.uuid(),
    customer_id: faker.datatype.uuid(),
    employee_id: faker.datatype.uuid(),
    created_at: faker.date.recent(),
    updated_at: faker.date.recent(),
    ...data,
  };

  return transactionMock;
};

export default { mock };
