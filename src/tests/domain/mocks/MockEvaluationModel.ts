import { faker } from '@faker-js/faker';

import Evaluation from '@domain/models/Evaluation';

import IMockEvaluationModelDTO from '@tests/domain/mocks/dtos/MockEvaluationModelDTO';

const mock = (data?: IMockEvaluationModelDTO): Evaluation => {
  const evaluationMock = {
    id: faker.datatype.uuid(),
    alias_id: `evaluation-${faker.datatype.uuid()}`,
    description: faker.commerce.productAdjective(),
    transaction_value: faker.datatype.number({ max: 10 }),
    transaction_id: faker.datatype.uuid(),
    created_at: faker.date.recent(),
    updated_at: faker.date.recent(),
    ...data,
  };

  return evaluationMock;
};

export default { mock };
