import { faker } from '@faker-js/faker';

import Customer from '@domain/models/Customer';

import IMockCustomerModelDTO from '@tests/domain/mocks/dtos/MockCustomerModelDTO';

const mock = (data?: IMockCustomerModelDTO): Customer => {
  const customerMock = {
    id: faker.datatype.uuid(),
    alias_id: `customer-${faker.datatype.uuid()}`,
    name: faker.commerce.productName(),
    email: faker.internet.email(),
    phone_number: String(faker.datatype.number()),
    cpf: String(faker.datatype.number()),
    created_at: faker.date.recent(),
    updated_at: faker.date.recent(),
    ...data,
  };

  return customerMock;
};

export default { mock };
