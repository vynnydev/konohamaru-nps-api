export default interface IMockEvaluationModelDTO {
  id?: string;
  alias_id?: string;
  description?: string;
  transaction_value?: number;
  transaction_id?: string;
  created_at?: Date;
  updated_at?: Date;
}
