export default interface IMockStoreModelDTO {
  id?: string;
  alias_id?: string;
  name?: string;
  created_at?: Date;
  updated_at?: Date;
}
