export default interface IMockCustomerModelDTO {
  id?: string;
  alias_id?: string;
  name?: string;
  email?: string;
  phone_number?: string;
  cpf?: string;
  created_at?: Date;
  updated_at?: Date;
}
