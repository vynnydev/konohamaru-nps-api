import 'dotenv/config';
import 'reflect-metadata';

import swaggerUi from 'swagger-ui-express';
import { app } from '@main/config/app';
import swaggerDocument from '../swagger.json';

import databaseConnectionManagerFactory from './main/factories/helpers/typeOrm/DatabaseConnectionManagerFactory';
import testDatabaseConnectionManagerFactory from './main/factories/helpers/typeOrm/TestDatabaseConnectionManagerFactory';

const PORT = process.env.PORT || 8000;

const startApp = async () => {
  const databaseConnectionManager =
    databaseConnectionManagerFactory.makeDatabaseConnectionManager();

  const testDatabaseConnectionManager =
    testDatabaseConnectionManagerFactory.makeTestDatabaseConnectionManager();

  await databaseConnectionManager.getConnection();
  await testDatabaseConnectionManager.getConnection();

  app.use(
    '/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument, {
      swaggerOptions: {
        url: '/swagger.json',
      },
    }),
  );

  app.listen(PORT);
};

startApp();
