FROM node:alpine

WORKDIR /usr/app

COPY package*.json ./

RUN yarn install

COPY --from=build package-lock.json package-lock.json

EXPOSE 3000

CMD ["yarn", "run", "dev"]